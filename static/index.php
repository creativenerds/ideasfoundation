<?php include('includes/_header-meta.php');?>
<?php include('includes/_header.php');?>

<section id="home">
	<div class="main">
		<div class="banner">
			<div class="plane"></div>
		</div>
	</div>

	<div class="inner">
		<div class="sub-inset">
			<div class="welcome">Hello! Welcome to the</div>
			<div class="notepad-section">
				<ul>
					<li>We identify & nurture creatively gifted young people.</li>
					<li>We give the young people we identify the skills they <br /> need to work in the creative industries</li>
					<li>We identify & nurture creatively gifted young people.</li>
				</ul>
			</div>

			<div class="speech-cloud"></div>

			<div class="video-holder">
				<div class="video">
					<iframe width="475" height="290" src="//www.youtube.com/embed/ScMzIvxBSi4?rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>

			<div class="home-carousel">
				<div class="cloud-2"></div>
				<!-- CAROUSEl -->
				<div id="sub-carousel" class="owl-carousel">
					<?php for ($i=0; $i < 10; $i++) {?>
					<a href="http://www.creativenerds.org"><img src="img/home/creativenerds.png" alt=""></a>
					<?php } ?>
				</div>
				<!-- // END CAROUSEL -->
			</div>
		</div>
	</div>
</section>
<div class="end-section home"></div>
<section id="what-we-do" >
	<div class="inner" >
		<h2 id="what-we-do-heading">What We Do...?</h2>
		<div class="creative">
			<span class="icon"></span>
			<div class="content">
				<p>

					Lorem ipsum dolor sit amet, consectetur  
					adiping elit. Quisque id metus libero. Pellentesque habitant morbi tri tique senectus et netus et malesuada fames  ac turpis egestas.  Quisqu id metus libero. Pellentesque habitant morbtristique senectus et netus et malesuada fames  ac turpis egestas. Quisqu id. Lorem ipsum dolor sit amet, consectetur  adiping elit. Quisque id metus libero. Quisqu id metus libero. Pellentesque habitant.ing elit. Quisque id metus libero. Quisqu id metus libero. Pellentesque habitant.
				</p>
			</div>

			<a href="#" class="cta">View Site</a>
		</div>
		<div class="incubate">
			<span class="icon"></span>
			<div class="content">
				<p>

					Lorem ipsum dolor sit amet, consectetur  
					adiping elit. Quisque id metus libero. Pellentesque habitant morbi tri tique senectus et netus et malesuada fames  ac turpis egestas.  Quisqu id metus libero. Pellentesque habitant morbtristique senectus et netus et malesuada fames  ac turpis egestas. Quisqu id. Lorem ipsum dolor sit amet, consectetur  adiping elit. Quisque id metus libero. Quisqu id metus libero. Pellentesque habitant.ing elit. Quisque id metus libero. Quisqu id metus libero. Pellentesque habitant.
				</p>
			</div>
			<a href="#" class="cta">View Site</a>
		</div>

		<div class="ladder clearfix" >
			<div class="content">
				<p>
					Lorem ipsum dolor sit amet, consectetur  adiping elit. Quisque id metus libero. Pellentesque 
					habitant morbi tri tique senectus et netus et malesuada fames  ac turpis egestas.  Quisqu 
					id metus libero. Pellentesque habitant morbtristique senectus et netus et malesuada fames  
					ac turpis egestas. Quisqu id. Lorem ipsum dolor sit amet, consectetur  adiping elit. Quisque id 
					metus libero. Quisqu id metus libero. Pellentesque habitant.ing elit. Quisque id metus 
					libero. Quisqu id metus libero. Pellentesque habitant.
				</p>
				<a href="#" class="cta">View Site</a>
			</div>

		</div>

		<a href="#" class="nextpage who-we-work"><span></span></a>
	</div>
</section>

<section id="diversity">
	<div class="inner">
		<div class="diversity-element">
			<div class="content">
				<p>
					Lolor sit amet, consectetur  adiping elit. Quisque id metus libero. Pellentesque 
					habitant morbi tri tique senectus et netus et malesuada fames  ac turpis egestas.  Quisqu 
					id metus libero. Pellentesque habitant morbtristique senectus et netus et malesuada fames  
					ac turpis egestas. Quisqu id. Lorem ipsum dolor sit amet, consectetur  adiping elit. Quisque id 
					metus libero. Quisqu id metus libero. Pellentesque habitant.ing elit. Quisque id metus 
					libero. Quisqu id metus libero. Pellentesque habitant.
				</p>

				<a href="#" class="cta">View Site</a>
			</div>
		</div>
	</div>
</section>
<div class="end-section what"></div>

<section id="who-we-work" >
	<div class="inner">
		<h2>WHO WE WORK WITH...?</h2>
		<div id="focal" data-stellar-ratio="1.5" >
			<figure class="polaroid">
				<img src="http://placehold.it/370x370" alt="">
			</figure>

			<div class="tab-section">
				<div class="line-span"></div>
				<ul class="clearfix">
					<li><a class="first active" href="#schools">Schools</a></li>
					<li><a class="second" href="#brands">Brands</a></li>
					<li><a class="third" href="#agencies">Agencies</a></li>
				</ul>

				<div class="tab-content active" id="schools">
					<p>
						SCHOOLS - Lorem ipsum dolor sit amet, consectetur 
						adipiscing elit. Quisque id metus libero. Pellentesque 
						habitant morbi tristique senectus et netus et malesuada 
						fames ac turpis egestas.  Lorem ipsum dolor sit amet, 
						consectetur  adipiscing elit. Quisque id metus libero. 
						Pellentesque habitant morbi tristique senectus et netus
						fames ac turpis egestas.  senectus et netus et malesuada 
						fames ac turpis egestas.  Lorem ipsum dolor sit amet, consect
						adipiscing elit. Quisque id .
					</p>
				</div>
				<div class="tab-content" id="brands">
					<p>
						BRANDS - Lorem ipsum dolor sit amet, consectetur 
						adipiscing elit. Quisque id metus libero. Pellentesque 
						habitant morbi tristique senectus et netus et malesuada 
						fames ac turpis egestas.  Lorem ipsum dolor sit amet, 
						consectetur  adipiscing elit. Quisque id metus libero. 
						Pellentesque habitant morbi tristique senectus et netus
						fames ac turpis egestas.  senectus et netus et malesuada 
						fames ac turpis egestas.  Lorem ipsum dolor sit amet, consect
						adipiscing elit. Quisque id .
					</p>
				</div>
				<div class="tab-content" id="agencies">
					<p>
						AGENCIES - Lorem ipsum dolor sit amet, consectetur 
						adipiscing elit. Quisque id metus libero. Pellentesque 
						habitant morbi tristique senectus et netus et malesuada 
						fames ac turpis egestas.  Lorem ipsum dolor sit amet, 
						consectetur  adipiscing elit. Quisque id metus libero. 
						Pellentesque habitant morbi tristique senectus et netus
						fames ac turpis egestas.  senectus et netus et malesuada 
						fames ac turpis egestas.  Lorem ipsum dolor sit amet, consect
						adipiscing elit. Quisque id .
					</p>
				</div>
			</div>

			<div class="testimonial clearfix">
				<div class="line-break"></div>
				<figure>
					<img src="http://placehold.it/125x125" alt="">
				</figure>
				<div class="copy">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quae error nisi eaque nesciunt iure blanditiis delectus consectetur tempore vitae. Neque, eum odit sit in facere nam quas cum blanditiis.
				</div>
				<div class="author">
					<span>- Forename Surname, Job Position</span>
				</div>
			</div>
		</div>

		<!-- CAROUSEl -->
		<div id="logo-carousel" class="owl-carousel">
			<?php for ($i=0; $i < 10; $i++):?>
			<a href="#"><img src="img/home/logos.png" alt=""></a>
		<?php endfor; ?>

	</div>
	<!-- // END CAROUSEL -->

</div>
</section>
<div class="end-section who"></div>

<section id="us">
	<div class="inner">
		<h2>Who We Are...</h2>

		<div id="about-cats">
			<div class="tab-section">
				<div class="line-span"></div>
				<ul class="clearfix">
					<li><a class="first active" href="#trustees" data-scroller="trustees">trustees</a></li>
					<li><a class="second" href="#staff" data-scroller="staff">staff</a></li>
					<li><a class="third" href="#scholars-council" data-scroller="scholars-council">Scholars Council</a></li>
				</ul>

				<div class="tab-content active" id="trustees">
					<p>
						SCHOOLS - Lorem ipsum dolor sit amet, consectetur 
						adipiscing elit. Quisque id metus libero. Pellentesque 
						habitant morbi tristique senectus et netus et malesuada 
						fames ac turpis egestas.  Lorem ipsum dolor sit amet, 
						consectetur  adipiscing elit. Quisque id metus libero. 
						Pellentesque habitant morbi tristique senectus et netus
						fames ac turpis egestas.  senectus et netus et malesuada 
						fames ac turpis egestas.  Lorem ipsum dolor sit amet, consect
						adipiscing elit. Quisque id .
					</p>
				</div>
				<div class="tab-content" id="staff">
					<p>
						SCHOOLS - Lorem ipsum dolor sit amet, consectetur 
						adipiscing elit. Quisque id metus libero. Pellentesque 
						habitant morbi tristique senectus et netus et malesuada 
						fames ac turpis egestas.  Lorem ipsum dolor sit amet, 
						consectetur  adipiscing elit. Quisque id metus libero. 
						Pellentesque habitant morbi tristique senectus et netus
						fames ac turpis egestas.  senectus et netus et malesuada 
						fames ac turpis egestas.  Lorem ipsum dolor sit amet, consect
						adipiscing elit. Quisque id .
					</p>
				</div>
				<div class="tab-content" id="scholars-council">
					<p>
						SCHOOLS - Lorem ipsum dolor sit amet, consectetur 
						adipiscing elit. Quisque id metus libero. Pellentesque 
						habitant morbi tristique senectus et netus et malesuada 
						fames ac turpis egestas.  Lorem ipsum dolor sit amet, 
						consectetur  adipiscing elit. Quisque id metus libero. 
						Pellentesque habitant morbi tristique senectus et netus
						fames ac turpis egestas.  senectus et netus et malesuada 
						fames ac turpis egestas.  Lorem ipsum dolor sit amet, consect
						adipiscing elit. Quisque id .
					</p>
				</div>
			</div>
		</div>

		<div class="trustees-cont trustees">
			<h3>Trustees</h3>
			<div class="line-break"></div>
			<div class="jcarousel">
				<ul>
					<?php for ($i=0; $i < 6; $i++): ?>
					<li>
						<figure>
							<img src="http://placehold.it/77x77" alt="">
						</figure>
						<div class="sub-content">
							<strong>Joe Bloggs <?php echo $i; ?></strong>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing eldignissimos exercitationem eum officiis illum!
							</p>
						</div>
					</li>
				<?php endfor; ?>
			</ul>
		</div>

		<div class="paginated">
			<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
			<a href="#" class="jcarousel-control-next">&rsaquo;</a>
		</div>

	</div>


	<div class="trustees-cont staff">
		<h3>Staff</h3>
		<div class="line-break"></div>
		<div class="jcarousel">
			<ul>
				<?php for ($i=0; $i < 6; $i++): ?>
				<li>
					<figure>
						<img src="http://placehold.it/77x77" alt="">
					</figure>
					<div class="sub-content">
						<strong>Staff Member <?php echo $i; ?></strong>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing eldignissimos exercitationem eum officiis illum!
						</p>
					</div>
				</li>
			<?php endfor; ?>
		</ul>
	</div>

	<div class="paginated">
		<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
		<a href="#" class="jcarousel-control-next">&rsaquo;</a>
	</div>

</div>

<div class="trustees-cont scholars-council">
	<h3>Scholars Council</h3>
	<div class="line-break"></div>
	<div class="jcarousel">
		<ul>
			<?php for ($i=0; $i < 6; $i++): ?>
			<li>
				<figure>
					<img src="http://placehold.it/77x77" alt="">
				</figure>
				<div class="sub-content">
					<strong>Scholars Council - <?php echo $i; ?></strong>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing eldignissimos exercitationem eum officiis illum!
					</p>
				</div>
			</li>
		<?php endfor; ?>
	</ul>
</div>

<div class="paginated">
	<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
	<a href="#" class="jcarousel-control-next">&rsaquo;</a>
</div>

</div>
</div>
</section>
<div class="end-section blog"></div>

<section id="blog">
	<div class="inner">
		<h2>BLOG</h2>

		<div class="blog-articles clearfix" data-stellar-ratio="2" data-stellar-vertical-offset="150">
			<article class="one">
				<figure >
					<img src="http://placehold.it/155x155" alt="">
				</figure>
				<div class="blog-content">
					<h3>Lorem ipsum dolor</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, explicabo ipsa necessitatibus labore est error similique eaque. Aliquid, sit, nihil cum ratione delectus provident ea impedit aut officiis commodi quod.</p>
					<a href="#" class="read-more">...Read More</a>
				</div>
			</article>
			<article class="two" data-id="first-blog">
				<figure>
					<img src="http://placehold.it/155x155" alt="">
				</figure>
				<div class="blog-content">
					<h3>Lorem ipsum dolor</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, explicabo ipsa necessitatibus labore est error similique eaque. Aliquid, sit, nihil cum ratione delectus provident ea impedit aut officiis commodi quod.</p>
					<a href="#" class="read-more">...Read More</a>
				</div>
			</article>
			<article class="three">
				<figure>
					<img src="http://placehold.it/155x155" alt="">
				</figure>
				<div class="blog-content">
					<h3>Lorem ipsum dolor</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, explicabo ipsa necessitatibus labore est error similique eaque. Aliquid, sit, nihil cum ratione delectus provident ea impedit aut officiis commodi quod.</p>
					<a href="#" class="read-more">...Read More</a>
				</div>
			</article>
			<article class="four">
				<figure>
					<img src="http://placehold.it/155x155" alt="">
				</figure>
				<div class="blog-content">
					<h3>Lorem ipsum dolor</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum, explicabo ipsa necessitatibus labore est error similique eaque. Aliquid, sit, nihil cum ratione delectus provident ea impedit aut officiis commodi quod.</p>
					<a href="#" class="read-more">...Read More</a>
				</div>
			</article>
		</div>
	</div>
	<div class="blog-drops">
		<article data-id="first-blog">
			<figure class="polaroid">
				<img src="http://placehold.it/370x370" alt="">
			</figure>

			<figure class="polaroid secondary">
				<img src="http://placehold.it/370x370" alt="">
			</figure>

			<div class="tab-section">
				<div class="line-span"></div>
				<h3>Lorem ipsum dolar...</h3>

				<div class="tab-content">
					<p>
						SCHOOLS - Lorem ipsum dolor sit amet, consectetur 
						adipiscing elit. Quisque id metus libero. Pellentesque 
						habitant morbi tristique senectus et netus et malesuada 
						fames ac turpis egestas.  Lorem ipsum dolor sit amet, 
						consectetur  adipiscing elit. Quisque id metus libero. 
						Pellentesque habitant morbi tristique senectus et netus
						fames ac turpis egestas.  senectus et netus et malesuada 
						fames ac turpis egestas.  Lorem ipsum dolor sit amet, consect
						adipiscing elit. Quisque id .
					</p>

					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae, iure, sequi esse vitae nulla eum facere explicabo! Inventore, error totam at quam sed saepe enim! Dolore alias veniam nostrum odio.
					</p>

					<p>
						<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi, enim possimus ut quae minus non omnis asperiores iste assumenda pariatur modi expedita necessitatibus blanditiis perspiciatis nemo animi quod doloremque ipsa.</span>
						<span>Debitis, impedit, ullam, neque, dolorum eos fuga consectetur quisquam asperiores voluptatum aspernatur delectus quis cum odio aperiam voluptas commodi numquam voluptate voluptatibus officiis deserunt porro minima eaque iste nihil nemo!</span>
						<span>Ea, in error vitae neque modi animi dolores suscipit qui tenetur quisquam. Earum, repellat, voluptatum itaque neque officiis alias veritatis aspernatur eos obcaecati eius iusto vel! Corrupti laborum tempora laudantium.</span>
					</p>
				</div>
			</div>
			<div class="testimonial clearfix">
				<div class="line-break"></div>
				<a href="" class="close-blog">
					CLOSE
				</a>
			</div>
		</article>
	</div>

</section>
<div class="end-section footer"></div>

<?php include('includes/_footer.php');?>
<?php include('includes/_footer-meta.php');?>
