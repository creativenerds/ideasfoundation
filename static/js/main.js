$(document).ready(function(){


// Tabs setup
$('#focal .tab-section li a').click(function(event){
	event.preventDefault();
	var tabContent = $(this).attr('href');
	$('#focal .tab-section li a').removeClass('active');
	$(this).addClass('active');
	$(this).parent().parent().parent().find('.tab-content').hide().removeClass('active');
	$(tabContent).fadeIn('slow').addClass('active');
});


// REDO TGUS
// $('#about-cats .tab-section li a').click(function(event){
// 	event.preventDefault();
// 	var tabContent = $(this).attr('href');
// 	var scroller = $(this).data('scroller');
// 	$('#about-cats .tab-section li a').removeClass('active');	
// 	$(this).addClass('active');
// 	$(this).parent().parent().parent().find('.tab-content').hide().removeClass('active');
// 	$(tabContent).fadeIn('slow').addClass('active');
// });


setTimeout(function(){
	var calculateWidth = $('#home').width()/2;
	var furtherCal = calculateWidth - $('.banner').width();
	console.log(furtherCal);
	$('.plane').animate({
		right: '-250px'
	}, 1750);
	$('.banner').animate({
		left: furtherCal
	}, 5000, function(){
		$('.banner').css({
			left: furtherCal
		});
	});

}, 250, setTimeout(function(){
	$('.welcome').fadeIn();
	$('.plane').animate({
			right: '-220%'
		}, 5000, function(){
			$('.main').addClass('triggered');
		});

}, 5200));

$('#about-cats .tab-section li a').click(function(e){
	e.preventDefault();

	var tab_id = $(this).data('scroller');

	$('#about-cats .tab-section li a').removeClass('active');
	$('#about-cats .tab-content').hide();
	$('.trustees-cont').hide();

	$(this).addClass('active');
	$('#about-cats #'+tab_id).fadeIn();
	$('.trustees-cont.'+tab_id).fadeIn();
});

$.stellar({
	horizontalScrolling: false,
	verticalOffset: 0,
	hideDistantElements: false,
});

$("#sub-carousel").owlCarousel({
	items: 4,
	autoPlay: true,
	navigation: true,
	navigationText: [
	"<i class='left-arrow'></i>",
	"<i class='right-arrow'></i>"
	]
});

$("#logo-carousel").owlCarousel({
	items: 5,
	autoPlay: true,
	navigation: true,
	navigationText: [
	"<i class='left-arrow'></i>",
	"<i class='right-arrow'></i>"
	]
});

$(".nav a").click(function(e) {
	e.preventDefault();
	$(".nav a").removeClass('active');
	$(this).addClass('active');
	$('html, body').animate({
		scrollTop: $($.attr(this, 'href')).offset().top
	}, 2000);
});

$('.blog-content a').click(function(e){
	e.preventDefault();
	var thisPost = $(this).data('id');
	$('.blog-drops article').data('id', thisPost).fadeIn();
});

$('.close-blog').click(function(e){
	e.preventDefault();
	$('.blog-drops article').fadeOut();
});

$('.jcarousel').jcarousel({
	'vertical': true
});

$('.jcarousel-control-prev')
.on('active.jcarouselcontrol', function() {
	$(this).removeClass('inactive');
})
.on('inactive.jcarouselcontrol', function() {
	$(this).addClass('inactive');
})
.jcarouselControl({
	target: '-=1'
});

$('.jcarousel-control-next')
.on('active.jcarouselcontrol', function() {
	$(this).removeClass('inactive');
})
.on('inactive.jcarouselcontrol', function() {
	$(this).addClass('inactive');
})
.jcarouselControl({
	target: '+=1'
});

$('.jcarousel-pagination')
.on('active.jcarouselpagination', 'a', function() {
	$(this).addClass('active');
})
.on('inactive.jcarouselpagination', 'a', function() {
	$(this).removeClass('active');
})
.jcarouselPagination();


});
