/*
 Navicat MySQL Data Transfer

 Source Server         : Localhost
 Source Server Version : 50529
 Source Host           : localhost
 Source Database       : ideasfoundation

 Target Server Version : 50529
 File Encoding         : utf-8

 Date: 10/29/2013 20:03:38 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `exp_accessories`
-- ----------------------------
DROP TABLE IF EXISTS `exp_accessories`;
CREATE TABLE `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(255) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_accessories`
-- ----------------------------
BEGIN;
INSERT INTO `exp_accessories` VALUES ('1', 'Expressionengine_info_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0'), ('2', 'Structure_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '3.3.13');
COMMIT;

-- ----------------------------
--  Table structure for `exp_actions`
-- ----------------------------
DROP TABLE IF EXISTS `exp_actions`;
CREATE TABLE `exp_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  `csrf_exempt` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`action_id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_actions`
-- ----------------------------
BEGIN;
INSERT INTO `exp_actions` VALUES ('1', 'Comment', 'insert_new_comment', '0'), ('2', 'Comment_mcp', 'delete_comment_notification', '0'), ('3', 'Comment', 'comment_subscribe', '0'), ('4', 'Comment', 'edit_comment', '0'), ('5', 'Email', 'send_email', '0'), ('6', 'Channel', 'submit_entry', '0'), ('7', 'Channel', 'filemanager_endpoint', '0'), ('8', 'Channel', 'smiley_pop', '0'), ('9', 'Channel', 'combo_loader', '0'), ('10', 'Member', 'registration_form', '0'), ('11', 'Member', 'register_member', '0'), ('12', 'Member', 'activate_member', '0'), ('13', 'Member', 'member_login', '0'), ('14', 'Member', 'member_logout', '0'), ('15', 'Member', 'send_reset_token', '0'), ('16', 'Member', 'process_reset_password', '0'), ('17', 'Member', 'send_member_email', '0'), ('18', 'Member', 'update_un_pw', '0'), ('19', 'Member', 'member_search', '0'), ('20', 'Member', 'member_delete', '0'), ('21', 'Rte', 'get_js', '0'), ('22', 'Assets_mcp', 'upload_file', '0'), ('23', 'Assets_mcp', 'get_files_view_by_folders', '0'), ('24', 'Assets_mcp', 'get_props', '0'), ('25', 'Assets_mcp', 'save_props', '0'), ('26', 'Assets_mcp', 'get_ordered_files_view', '0'), ('27', 'Assets_mcp', 'get_session_id', '0'), ('28', 'Assets_mcp', 'start_index', '0'), ('29', 'Assets_mcp', 'perform_index', '0'), ('30', 'Assets_mcp', 'finish_index', '0'), ('31', 'Assets_mcp', 'get_s3_buckets', '0'), ('32', 'Assets_mcp', 'get_gc_buckets', '0'), ('33', 'Assets_mcp', 'get_rs_containers', '0'), ('34', 'Assets_mcp', 'move_folder', '0'), ('35', 'Assets_mcp', 'rename_folder', '0'), ('36', 'Assets_mcp', 'create_folder', '0'), ('37', 'Assets_mcp', 'delete_folder', '0'), ('38', 'Assets_mcp', 'view_file', '0'), ('39', 'Assets_mcp', 'move_file', '0'), ('40', 'Assets_mcp', 'delete_file', '0'), ('41', 'Assets_mcp', 'view_thumbnail', '0'), ('42', 'Assets_mcp', 'build_sheet', '0'), ('43', 'Assets_mcp', 'get_selected_files', '0'), ('44', 'Structure', 'ajax_move_set_data', '0');
COMMIT;

-- ----------------------------
--  Table structure for `exp_assets_files`
-- ----------------------------
DROP TABLE IF EXISTS `exp_assets_files`;
CREATE TABLE `exp_assets_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `folder_id` int(10) unsigned NOT NULL,
  `source_type` varchar(2) NOT NULL DEFAULT 'ee',
  `source_id` int(10) unsigned DEFAULT NULL,
  `filedir_id` int(4) unsigned DEFAULT NULL,
  `file_name` varchar(255) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `date` int(10) unsigned DEFAULT NULL,
  `alt_text` tinytext,
  `caption` tinytext,
  `author` tinytext,
  `desc` text,
  `location` tinytext,
  `keywords` text,
  `date_modified` int(10) unsigned DEFAULT NULL,
  `kind` varchar(5) DEFAULT NULL,
  `width` int(2) DEFAULT NULL,
  `height` int(2) DEFAULT NULL,
  `size` int(3) DEFAULT NULL,
  `search_keywords` text,
  PRIMARY KEY (`file_id`),
  UNIQUE KEY `unq_folder_id__file_name` (`folder_id`,`file_name`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_assets_files`
-- ----------------------------
BEGIN;
INSERT INTO `exp_assets_files` VALUES ('1', '1', 'ee', null, '1', 'creativenerds.png', null, '1382550003', null, null, null, null, null, null, '1382797830', 'image', '176', '101', '5096', 'creativenerds.png'), ('3', '1', 'ee', null, '1', 'logos.png', null, '1382885248', null, null, null, null, null, null, '1382885248', 'image', '67', '20', '2153', 'logos.png');
COMMIT;

-- ----------------------------
--  Table structure for `exp_assets_folders`
-- ----------------------------
DROP TABLE IF EXISTS `exp_assets_folders`;
CREATE TABLE `exp_assets_folders` (
  `folder_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source_type` varchar(2) NOT NULL DEFAULT 'ee',
  `folder_name` varchar(255) NOT NULL,
  `full_path` varchar(255) DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `source_id` int(10) unsigned DEFAULT NULL,
  `filedir_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`folder_id`),
  UNIQUE KEY `unq_source_type__source_id__filedir_id__parent_id__folder_name` (`source_type`,`source_id`,`filedir_id`,`parent_id`,`folder_name`),
  UNIQUE KEY `unq_source_type__source_id__filedir_id__full_path` (`source_type`,`source_id`,`filedir_id`,`full_path`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_assets_folders`
-- ----------------------------
BEGIN;
INSERT INTO `exp_assets_folders` VALUES ('1', 'ee', 'Partners', '', null, null, '1');
COMMIT;

-- ----------------------------
--  Table structure for `exp_assets_index_data`
-- ----------------------------
DROP TABLE IF EXISTS `exp_assets_index_data`;
CREATE TABLE `exp_assets_index_data` (
  `session_id` char(36) DEFAULT NULL,
  `source_type` varchar(2) NOT NULL DEFAULT 'ee',
  `source_id` int(10) unsigned DEFAULT NULL,
  `offset` int(10) unsigned DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `filesize` int(10) unsigned DEFAULT NULL,
  `type` enum('file','folder') DEFAULT NULL,
  `record_id` int(10) unsigned DEFAULT NULL,
  UNIQUE KEY `unq__session_id__source_type__source_id__offset` (`session_id`,`source_type`,`source_id`,`offset`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_assets_rackspace_access`
-- ----------------------------
DROP TABLE IF EXISTS `exp_assets_rackspace_access`;
CREATE TABLE `exp_assets_rackspace_access` (
  `connection_key` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `storage_url` varchar(255) NOT NULL,
  `cdn_url` varchar(255) NOT NULL,
  PRIMARY KEY (`connection_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_assets_selections`
-- ----------------------------
DROP TABLE IF EXISTS `exp_assets_selections`;
CREATE TABLE `exp_assets_selections` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `col_id` int(6) unsigned DEFAULT NULL,
  `row_id` int(10) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `element_id` varchar(255) DEFAULT NULL,
  `content_type` varchar(255) DEFAULT NULL,
  `sort_order` int(4) unsigned DEFAULT NULL,
  `is_draft` tinyint(1) unsigned DEFAULT '0',
  KEY `file_id` (`file_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `col_id` (`col_id`),
  KEY `row_id` (`row_id`),
  KEY `var_id` (`var_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_assets_selections`
-- ----------------------------
BEGIN;
INSERT INTO `exp_assets_selections` VALUES ('1', '1', '3', '2', '1', null, null, 'grid', '0', '0'), ('1', '3', '20', null, null, null, null, null, '0', '0'), ('1', '5', '23', null, null, null, null, null, '0', '0'), ('1', '5', '25', null, null, null, null, null, '0', '0'), ('1', '5', '22', null, null, null, null, null, '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `exp_assets_sources`
-- ----------------------------
DROP TABLE IF EXISTS `exp_assets_sources`;
CREATE TABLE `exp_assets_sources` (
  `source_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source_type` varchar(2) NOT NULL DEFAULT 's3',
  `name` varchar(255) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  PRIMARY KEY (`source_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_captcha`
-- ----------------------------
DROP TABLE IF EXISTS `exp_captcha`;
CREATE TABLE `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_categories`
-- ----------------------------
DROP TABLE IF EXISTS `exp_categories`;
CREATE TABLE `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_categories`
-- ----------------------------
BEGIN;
INSERT INTO `exp_categories` VALUES ('1', '1', '1', '0', 'Trustees', 'trustees', '', '', '3'), ('2', '1', '1', '0', 'Staff', 'staff', '', '', '2'), ('3', '1', '1', '0', 'Scholars council', 'scholars-council', '', '', '1');
COMMIT;

-- ----------------------------
--  Table structure for `exp_category_field_data`
-- ----------------------------
DROP TABLE IF EXISTS `exp_category_field_data`;
CREATE TABLE `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_category_field_data`
-- ----------------------------
BEGIN;
INSERT INTO `exp_category_field_data` VALUES ('1', '1', '1'), ('2', '1', '1'), ('3', '1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `exp_category_fields`
-- ----------------------------
DROP TABLE IF EXISTS `exp_category_fields`;
CREATE TABLE `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_category_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_category_groups`;
CREATE TABLE `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_category_groups`
-- ----------------------------
BEGIN;
INSERT INTO `exp_category_groups` VALUES ('1', '1', 'Who we are', 'a', '0', 'all', '', '');
COMMIT;

-- ----------------------------
--  Table structure for `exp_category_posts`
-- ----------------------------
DROP TABLE IF EXISTS `exp_category_posts`;
CREATE TABLE `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_category_posts`
-- ----------------------------
BEGIN;
INSERT INTO `exp_category_posts` VALUES ('3', '2');
COMMIT;

-- ----------------------------
--  Table structure for `exp_channel_data`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_data`;
CREATE TABLE `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` tinytext,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_4` text,
  `field_ft_4` tinytext,
  `field_id_5` text,
  `field_ft_5` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  `field_id_10` text,
  `field_ft_10` tinytext,
  `field_id_11` text,
  `field_ft_11` tinytext,
  `field_id_12` text,
  `field_ft_12` tinytext,
  `field_id_13` text,
  `field_ft_13` tinytext,
  `field_id_17` text,
  `field_ft_17` tinytext,
  `field_id_19` text,
  `field_ft_19` tinytext,
  `field_id_20` text,
  `field_ft_20` tinytext,
  `field_id_21` text,
  `field_ft_21` tinytext,
  `field_id_22` text,
  `field_ft_22` tinytext,
  `field_id_23` text,
  `field_ft_23` tinytext,
  `field_id_24` text,
  `field_ft_24` tinytext,
  `field_id_25` text,
  `field_ft_25` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_channel_data`
-- ----------------------------
BEGIN;
INSERT INTO `exp_channel_data` VALUES ('1', '1', '1', 'http://www.youtube.com/watch?v=ScMzIvxBSi4', 'xhtml', ' ', 'xhtml', ' ', 'xhtml', '<p>Lorem ipsum dolor sit amet, consectetur adiping elit. Quisque id metus libero. Pellentesque habitant morbi tri tique senectus et netus et malesuada fames ac turpis egestas. Quisqu id metus libero. Pellentesque habitant morbtristique senectus et netus et malesuada fames ac turpis egestas. Quisqu id. Lorem ipsum dolor sit amet, consectetur adiping elit. Quisque id metus libero. Quisqu id metus libero. Pellentesque habitant.ing elit. Quisque id metus libero. Quisqu id metus libero. Pellentesque habitant.</p>', 'none', '', 'none', '<p>Lorem ipsum dolor sit amet, consectetur adiping elit. Quisque id metus libero. Pellentesque habitant morbi tri tique senectus et netus et malesuada fames ac turpis egestas. Quisqu id metus libero. Pellentesque habitant morbtristique senectus et netus et malesuada fames ac turpis egestas. Quisqu id. Lorem ipsum dolor sit amet, consectetur adiping elit. Quisque id metus libero. Quisqu id metus libero. Pellentesque habitant.ing elit. Quisque id metus libero. Quisqu id metus libero. Pellentesque habitant.</p>', 'none', '', 'none', '<p>Lorem ipsum dolor sit amet, consectetur adiping elit. Quisque id metus libero. Pellentesque habitant morbi tri tique senectus et netus et malesuada fames ac turpis egestas. Quisqu id metus libero. Pellentesque habitant morbtristique senectus et netus et malesuada fames ac turpis egestas. Quisqu id. Lorem ipsum dolor sit amet, consectetur adiping elit. Quisque id metus libero. Quisqu id metus libero. Pellentesque habitant.ing elit. Quisque id metus libero. Quisqu id metus libero. Pellentesque habitant.</p>', 'none', '', 'none', '<p>Lorem ipsum dolor sit amet, consectetur adiping elit. Quisque id metus libero. Pellentesque habitant morbi tri tique senectus et netus et malesuada fames ac turpis egestas. Quisqu id metus libero. Pellentesque habitant morbtristique senectus et netus et malesuada fames ac turpis egestas. Quisqu id. Lorem ipsum dolor sit amet, consectetur adiping elit. Quisque id metus libero. Quisqu id metus libero. Pellentesque habitant.ing elit. Quisque id metus libero. Quisqu id metus libero. Pellentesque habitant.</p>', 'none', '', 'none', ' ', 'xhtml', '', 'none', '', 'none', ' ', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'), ('2', '1', '2', '', 'xhtml', ' ', 'xhtml', ' ', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', ' ', 'xhtml', 'http://www.creativenerds.org', 'none', '{filedir_1}logos.png', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'), ('3', '1', '3', '', 'xhtml', ' ', 'xhtml', ' ', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', ' ', 'xhtml', '', 'none', '', 'none', ' ', 'xhtml', 'creativenerds.png', 'none', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing eldignissimos exercitationem eum officiis illum!</p>', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'), ('4', '1', '1', '', 'xhtml', ' ', 'xhtml', ' ', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', ' ', 'xhtml', '', 'none', '', 'none', ' ', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'), ('5', '1', '4', '', 'xhtml', ' ', 'xhtml', ' ', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', ' ', 'xhtml', '', 'none', '', 'none', ' ', 'xhtml', '', 'none', '', 'none', 'creativenerds.png', 'none', 'creativenerds.png', 'none', ' ', 'xhtml', 'creativenerds.png', 'none');
COMMIT;

-- ----------------------------
--  Table structure for `exp_channel_entries_autosave`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_entries_autosave`;
CREATE TABLE `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_channel_fields`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_fields`;
CREATE TABLE `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_channel_fields`
-- ----------------------------
BEGIN;
INSERT INTO `exp_channel_fields` VALUES ('1', '1', '1', 'home_video', 'Home: Video', 'Simply paste in a link to a youtube video and it will do the rest', 'videolink', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'xhtml', 'n', '1', 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('2', '1', '1', 'home_points', 'Home: Points', '', 'grid', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'xhtml', 'n', '2', 'any', 'YTo4OntzOjEzOiJncmlkX21pbl9yb3dzIjtzOjE6IjMiO3M6MTM6ImdyaWRfbWF4X3Jvd3MiO3M6MToiMyI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'), ('3', '1', '1', 'home_partners', 'Home: Partners', 'Develop a sliding carousel of company clients and partners', 'grid', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'xhtml', 'n', '3', 'any', 'YTo4OntzOjEzOiJncmlkX21pbl9yb3dzIjtpOjA7czoxMzoiZ3JpZF9tYXhfcm93cyI7czowOiIiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='), ('4', '1', '1', 'wwd_creative_body', 'WWD: Creative Body', 'Add the body copy for Creative Section', 'wygwam', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '4', 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='), ('5', '1', '1', 'wwd_creative_link', 'WWD: Creative Link', 'If the creative section should have a link, place the link location here eg: http://www.google.com', 'text', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '5', 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('6', '1', '1', 'wwd_incubate', 'WWD: Incubate', 'Provide the body copy for the incubate section of the What we do section', 'wygwam', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '6', 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='), ('7', '1', '1', 'wwd_incubate_link', 'WWD: Incubate Link', 'Should the incubate section require a call to action add the link here eg http://www.google.com', 'text', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '7', 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('8', '1', '1', 'wwd_the_ladder_body', 'WWD: The Ladder Body', 'Provide body copy for the Ladder section', 'wygwam', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '8', 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='), ('9', '1', '1', 'wwd_the_ladder_link', 'WWD: The Ladder Link', 'Should the ladder section require a link, add it here eg http://www.google.com', 'text', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '9', 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('10', '1', '1', 'diversity_body_copy', 'Diversity: Body Copy', 'Add your content for the Diversity section here', 'wygwam', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '10', 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='), ('11', '1', '1', 'diversity_link', 'Diversity: Link', 'Add a link to the diversity section', 'text', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '11', 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('12', '1', '1', 'who_we_work_with', 'Who We Work With', 'Develop the who we work with section here', 'grid', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'xhtml', 'n', '12', 'any', 'YTo4OntzOjEzOiJncmlkX21pbl9yb3dzIjtpOjA7czoxMzoiZ3JpZF9tYXhfcm93cyI7czoxOiIzIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('13', '1', '1', 'logos_link', 'Logos: Link', 'Provide a link for the partner logo', 'text', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '13', 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('17', '1', '1', 'logo_image', 'Logo Image', '', 'file', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '14', 'any', 'YToxMDp7czoxODoiZmllbGRfY29udGVudF90eXBlIjtzOjU6ImltYWdlIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjE6IjEiO3M6MTM6InNob3dfZXhpc3RpbmciO3M6MToieSI7czoxMjoibnVtX2V4aXN0aW5nIjtzOjI6IjUwIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='), ('19', '1', '1', 'who_we_are', 'Who We Are', '', 'grid', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'xhtml', 'n', '15', 'any', 'YTo4OntzOjEzOiJncmlkX21pbl9yb3dzIjtpOjA7czoxMzoiZ3JpZF9tYXhfcm93cyI7czowOiIiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='), ('20', '1', '1', 'member_image', 'Member Image', '', 'assets', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '16', 'any', 'YToxMjp7czo4OiJmaWxlZGlycyI7czozOiJhbGwiO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6MTA6InRodW1iX3NpemUiO3M6NToic21hbGwiO3M6MTQ6InNob3dfZmlsZW5hbWVzIjtzOjE6Im4iO3M6OToic2hvd19jb2xzIjthOjE6e2k6MDtzOjQ6Im5hbWUiO31zOjU6Im11bHRpIjtzOjE6InkiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='), ('21', '1', '1', 'about_member', 'About Member', '', 'wygwam', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '17', 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='), ('22', '1', '1', 'thumbnail', 'Thumbnail', 'Provide a thumbnail image for the blog', 'assets', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '18', 'any', 'YToxMjp7czo4OiJmaWxlZGlycyI7czozOiJhbGwiO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6MTA6InRodW1iX3NpemUiO3M6NToic21hbGwiO3M6MTQ6InNob3dfZmlsZW5hbWVzIjtzOjE6Im4iO3M6OToic2hvd19jb2xzIjthOjE6e2k6MDtzOjQ6Im5hbWUiO31zOjU6Im11bHRpIjtzOjE6InkiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='), ('23', '1', '1', 'polaroid_1', 'Polaroid 1', 'Provide an image for polaroid 1 within the blog layout', 'assets', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '19', 'any', 'YToxMjp7czo4OiJmaWxlZGlycyI7czozOiJhbGwiO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6MTA6InRodW1iX3NpemUiO3M6NToic21hbGwiO3M6MTQ6InNob3dfZmlsZW5hbWVzIjtzOjE6Im4iO3M6OToic2hvd19jb2xzIjthOjE6e2k6MDtzOjQ6Im5hbWUiO31zOjU6Im11bHRpIjtzOjE6InkiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='), ('24', '1', '1', 'blog_content', 'Blog: Content', 'Add your blog content here', 'grid', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'xhtml', 'n', '20', 'any', 'YTo4OntzOjEzOiJncmlkX21pbl9yb3dzIjtpOjA7czoxMzoiZ3JpZF9tYXhfcm93cyI7czowOiIiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='), ('25', '1', '1', 'polaroid_2', 'Polaroid 2', '', 'assets', '', '0', '0', '0', '6', '128', 'n', 'ltr', 'n', 'n', 'none', 'n', '21', 'any', 'YToxMjp7czo4OiJmaWxlZGlycyI7czozOiJhbGwiO3M6NDoidmlldyI7czo2OiJ0aHVtYnMiO3M6MTA6InRodW1iX3NpemUiO3M6NToic21hbGwiO3M6MTQ6InNob3dfZmlsZW5hbWVzIjtzOjE6Im4iO3M6OToic2hvd19jb2xzIjthOjE6e2k6MDtzOjQ6Im5hbWUiO31zOjU6Im11bHRpIjtzOjE6InkiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
COMMIT;

-- ----------------------------
--  Table structure for `exp_channel_form_settings`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_form_settings`;
CREATE TABLE `exp_channel_form_settings` (
  `channel_form_settings_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `default_status` varchar(50) NOT NULL DEFAULT 'open',
  `require_captcha` char(1) NOT NULL DEFAULT 'n',
  `allow_guest_posts` char(1) NOT NULL DEFAULT 'n',
  `default_author` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_form_settings_id`),
  KEY `site_id` (`site_id`),
  KEY `channel_id` (`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_channel_grid_field_12`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_grid_field_12`;
CREATE TABLE `exp_channel_grid_field_12` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `row_order` int(10) unsigned DEFAULT NULL,
  `col_id_4` text,
  `col_id_5` text,
  `col_id_6` text,
  PRIMARY KEY (`row_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_channel_grid_field_12`
-- ----------------------------
BEGIN;
INSERT INTO `exp_channel_grid_field_12` VALUES ('1', '1', '0', 'Schools', 'SCHOOLS - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id metus libero. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id metus libero. Pellentesque habitant morbi tristique senectus et netus fames ac turpis egestas. senectus et netus et malesuada fames ac turpis egestas. Lorem ipsum dolor sit amet, consect adipiscing elit. Quisque id .', '');
COMMIT;

-- ----------------------------
--  Table structure for `exp_channel_grid_field_19`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_grid_field_19`;
CREATE TABLE `exp_channel_grid_field_19` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `row_order` int(10) unsigned DEFAULT NULL,
  `col_id_10` text,
  `col_id_11` text,
  PRIMARY KEY (`row_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_channel_grid_field_19`
-- ----------------------------
BEGIN;
INSERT INTO `exp_channel_grid_field_19` VALUES ('1', '1', '0', 'Schools', 'SCHOOLS - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id metus libero. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id metus libero. Pellentesque habitant morbi tristique senectus et netus fames ac turpis egestas. senectus et netus et malesuada fames ac turpis egestas. Lorem ipsum dolor sit amet, consect adipiscing elit. Quisque id .'), ('2', '1', '1', 'Staff', 'STAFF - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id metus libero. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id metus libero. Pellentesque habitant morbi tristique senectus et netus fames ac turpis egestas. senectus et netus et malesuada fames ac turpis egestas. Lorem ipsum dolor sit amet, consect adipiscing elit. Quisque id .'), ('3', '1', '2', 'Scholars-council', 'SCHOLARS COUNCIL - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id metus libero. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id metus libero. Pellentesque habitant morbi tristique senectus et netus fames ac turpis egestas. senectus et netus et malesuada fames ac turpis egestas. Lorem ipsum dolor sit amet, consect adipiscing elit. Quisque id .');
COMMIT;

-- ----------------------------
--  Table structure for `exp_channel_grid_field_2`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_grid_field_2`;
CREATE TABLE `exp_channel_grid_field_2` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `row_order` int(10) unsigned DEFAULT NULL,
  `col_id_1` text,
  PRIMARY KEY (`row_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_channel_grid_field_2`
-- ----------------------------
BEGIN;
INSERT INTO `exp_channel_grid_field_2` VALUES ('1', '1', '0', 'We identify & nurture creatively gifted young people'), ('2', '1', '1', 'We give the young people we identify the skills they need to work in the creative industries'), ('3', '1', '2', 'We identify & nurture creatively gifted young people.'), ('4', '2', '0', ''), ('5', '2', '1', ''), ('6', '2', '2', ''), ('7', '3', '0', ''), ('8', '3', '1', ''), ('9', '3', '2', ''), ('10', '4', '0', ''), ('11', '4', '1', ''), ('12', '4', '2', ''), ('13', '5', '0', ''), ('14', '5', '1', ''), ('15', '5', '2', '');
COMMIT;

-- ----------------------------
--  Table structure for `exp_channel_grid_field_24`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_grid_field_24`;
CREATE TABLE `exp_channel_grid_field_24` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `row_order` int(10) unsigned DEFAULT NULL,
  `col_id_12` text,
  `col_id_13` text,
  PRIMARY KEY (`row_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_channel_grid_field_24`
-- ----------------------------
BEGIN;
INSERT INTO `exp_channel_grid_field_24` VALUES ('1', '5', '0', 'A sub heading', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, quibusdam, ipsum temporibus soluta velit excepturi fuga sunt rem accusantium saepe dolorum error omnis sed adipisci pariatur reprehenderit fugit repudiandae minima.\n\n  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, quibusdam, ipsum temporibus soluta velit excepturi fuga sunt rem accusantium saepe dolorum error omnis sed adipisci pariatur reprehenderit fugit repudiandae minima.\n\n  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, quibusdam, ipsum temporibus soluta velit excepturi fuga sunt rem accusantium saepe dolorum error omnis sed adipisci pariatur reprehenderit fugit repudiandae minima\n\n  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, quibusdam, ipsum temporibus soluta velit excepturi fuga sunt rem accusantium saepe dolorum error omnis sed adipisci pariatur reprehenderit fugit repudiandae minima..');
COMMIT;

-- ----------------------------
--  Table structure for `exp_channel_grid_field_3`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_grid_field_3`;
CREATE TABLE `exp_channel_grid_field_3` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `row_order` int(10) unsigned DEFAULT NULL,
  `col_id_2` text,
  `col_id_3` text,
  PRIMARY KEY (`row_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_channel_grid_field_3`
-- ----------------------------
BEGIN;
INSERT INTO `exp_channel_grid_field_3` VALUES ('1', '1', '0', 'creativenerds.png', 'http://www.creativenerds.org');
COMMIT;

-- ----------------------------
--  Table structure for `exp_channel_member_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_member_groups`;
CREATE TABLE `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_channel_titles`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channel_titles`;
CREATE TABLE `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_channel_titles`
-- ----------------------------
BEGIN;
INSERT INTO `exp_channel_titles` VALUES ('1', '1', '1', '1', null, '127.0.0.1', 'Home', 'home', 'open', 'y', '0', '0', '0', '0', 'y', 'n', '1382722020', '2013', '10', '25', '0', '0', '20131028182443', '0', '0'), ('2', '1', '2', '1', null, '127.0.0.1', 'CreativeNerds', 'creativenerds', 'open', 'y', '0', '0', '0', '0', 'y', 'n', '1382885160', '2013', '10', '27', '0', '0', '20131027151534', '0', '0'), ('3', '1', '3', '1', null, '127.0.0.1', 'Staff member 1', 'staff-member-1', 'open', 'y', '0', '0', '0', '0', 'y', 'n', '1382985480', '2013', '10', '28', '0', '0', '20131028184047', '0', '0'), ('4', '1', '1', '1', null, '127.0.0.1', 'Blog', 'blog', 'open', 'y', '0', '0', '0', '0', 'y', 'n', '1383071640', '2013', '10', '29', '0', '0', '20131029183514', '0', '0'), ('5', '1', '4', '1', null, '127.0.0.1', 'A test blog', 'a-test-blog', 'open', 'y', '0', '0', '0', '0', 'y', 'n', '1383071700', '2013', '10', '29', '0', '0', '20131029185533', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `exp_channels`
-- ----------------------------
DROP TABLE IF EXISTS `exp_channels`;
CREATE TABLE `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(255) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_channels`
-- ----------------------------
BEGIN;
INSERT INTO `exp_channels` VALUES ('1', '1', 'home', 'Home', 'http://ideasfoundationee.cn/index.php', null, 'en', '2', '0', '1383071640', '0', '', '1', 'open', '1', null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', '5000', '0', 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, '0', null, 'y', null, 'n', '10', '', '', '0'), ('2', '1', 'logos', 'Logos', 'http://ideasfoundationee.cn/index.php', null, 'en', '1', '0', '1382885160', '0', '', '1', 'open', '1', null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', '5000', '0', 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, '0', null, 'y', null, 'n', '10', '', '', '0'), ('3', '1', 'carousel_item', 'Carousel Item', 'http://ideasfoundationee.cn/index.php', null, 'en', '1', '0', '1382985480', '0', '1', '1', 'open', '1', null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', '5000', '0', 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, '0', null, 'y', null, 'n', '10', '', '', '0'), ('4', '1', 'blog', 'Blog', 'http://ideasfoundationee.cn/index.php', null, 'en', '1', '0', '1383071700', '0', '', '1', 'open', '1', null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', '5000', '0', 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, '0', null, 'y', null, 'n', '10', '', '', '0');
COMMIT;

-- ----------------------------
--  Table structure for `exp_comment_subscriptions`
-- ----------------------------
DROP TABLE IF EXISTS `exp_comment_subscriptions`;
CREATE TABLE `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_comments`
-- ----------------------------
DROP TABLE IF EXISTS `exp_comments`;
CREATE TABLE `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`),
  KEY `comment_date_idx` (`comment_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_content_types`
-- ----------------------------
DROP TABLE IF EXISTS `exp_content_types`;
CREATE TABLE `exp_content_types` (
  `content_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_type_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_content_types`
-- ----------------------------
BEGIN;
INSERT INTO `exp_content_types` VALUES ('1', 'grid'), ('2', 'channel');
COMMIT;

-- ----------------------------
--  Table structure for `exp_cp_log`
-- ----------------------------
DROP TABLE IF EXISTS `exp_cp_log`;
CREATE TABLE `exp_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_cp_log`
-- ----------------------------
BEGIN;
INSERT INTO `exp_cp_log` VALUES ('1', '1', '1', 'admin', '127.0.0.1', '1382554121', 'Logged in'), ('2', '1', '1', 'admin', '127.0.0.1', '1382558033', 'Channel Created:&nbsp;&nbsp;Home'), ('3', '1', '1', 'admin', '127.0.0.1', '1382561577', 'Logged out'), ('4', '1', '1', 'admin', '127.0.0.1', '1382651941', 'Logged in'), ('5', '1', '1', 'admin', '127.0.0.1', '1382718801', 'Logged in'), ('6', '1', '1', 'admin', '127.0.0.1', '1382720007', 'Logged out'), ('7', '1', '1', 'admin', '127.0.0.1', '1382720185', 'Logged in'), ('8', '1', '1', 'admin', '127.0.0.1', '1382720241', 'Field Group Created:&nbsp;Default'), ('9', '1', '1', 'admin', '127.0.0.1', '1382795641', 'Logged in'), ('10', '1', '1', 'admin', '127.0.0.1', '1382883298', 'Logged in'), ('11', '1', '1', 'admin', '127.0.0.1', '1382883390', 'Channel Created:&nbsp;&nbsp;Logos'), ('12', '1', '1', 'admin', '127.0.0.1', '1382885895', 'Custom Field Deleted:&nbsp;Logos: Image'), ('13', '1', '1', 'admin', '127.0.0.1', '1382886430', 'Custom Field Deleted:&nbsp;Logo Image'), ('14', '1', '1', 'admin', '127.0.0.1', '1382886622', 'Custom Field Deleted:&nbsp;Alternative'), ('15', '1', '1', 'admin', '127.0.0.1', '1382983200', 'Logged in'), ('16', '1', '1', 'admin', '127.0.0.1', '1382983280', 'Channel Created:&nbsp;&nbsp;Carousel Item'), ('17', '1', '1', 'admin', '127.0.0.1', '1382983294', 'Category Group Created:&nbsp;&nbsp;Who we are'), ('18', '1', '1', 'admin', '127.0.0.1', '1382983415', 'Custom Field Deleted:&nbsp;tryme'), ('19', '1', '1', 'admin', '127.0.0.1', '1382986983', 'Logged in'), ('20', '1', '1', 'admin', '127.0.0.1', '1383071627', 'Logged in'), ('21', '1', '1', 'admin', '127.0.0.1', '1383071668', 'Channel Created:&nbsp;&nbsp;Blog');
COMMIT;

-- ----------------------------
--  Table structure for `exp_cp_search_index`
-- ----------------------------
DROP TABLE IF EXISTS `exp_cp_search_index`;
CREATE TABLE `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_developer_log`
-- ----------------------------
DROP TABLE IF EXISTS `exp_developer_log`;
CREATE TABLE `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  `template_id` int(10) unsigned NOT NULL DEFAULT '0',
  `template_name` varchar(100) DEFAULT NULL,
  `template_group` varchar(100) DEFAULT NULL,
  `addon_module` varchar(100) DEFAULT NULL,
  `addon_method` varchar(100) DEFAULT NULL,
  `snippets` text,
  `hash` char(32) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_email_cache`
-- ----------------------------
DROP TABLE IF EXISTS `exp_email_cache`;
CREATE TABLE `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_email_cache_mg`
-- ----------------------------
DROP TABLE IF EXISTS `exp_email_cache_mg`;
CREATE TABLE `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_email_cache_ml`
-- ----------------------------
DROP TABLE IF EXISTS `exp_email_cache_ml`;
CREATE TABLE `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_email_console_cache`
-- ----------------------------
DROP TABLE IF EXISTS `exp_email_console_cache`;
CREATE TABLE `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_email_tracker`
-- ----------------------------
DROP TABLE IF EXISTS `exp_email_tracker`;
CREATE TABLE `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(45) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_entry_versioning`
-- ----------------------------
DROP TABLE IF EXISTS `exp_entry_versioning`;
CREATE TABLE `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_extensions`
-- ----------------------------
DROP TABLE IF EXISTS `exp_extensions`;
CREATE TABLE `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_extensions`
-- ----------------------------
BEGIN;
INSERT INTO `exp_extensions` VALUES ('1', 'Rte_ext', 'myaccount_nav_setup', 'myaccount_nav_setup', '', '10', '1.0.1', 'y'), ('2', 'Rte_ext', 'cp_menu_array', 'cp_menu_array', '', '10', '1.0.1', 'y'), ('3', 'Assets_ext', 'channel_entries_query_result', 'channel_entries_query_result', '', '10', '2.2.1', 'y'), ('4', 'Assets_ext', 'file_after_save', 'file_after_save', '', '9', '2.2.1', 'y'), ('5', 'Assets_ext', 'files_after_delete', 'files_after_delete', '', '8', '2.2.1', 'y'), ('6', 'Matrix_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', '10', '2.5.8', 'y'), ('7', 'Structure_ext', 'entry_submission_redirect', 'entry_submission_redirect', '', '10', '3.3.13', 'y'), ('8', 'Structure_ext', 'cp_member_login', 'cp_member_login', '', '10', '3.3.13', 'y'), ('9', 'Structure_ext', 'sessions_start', 'sessions_start', '', '10', '3.3.13', 'y'), ('10', 'Structure_ext', 'channel_module_create_pagination', 'channel_module_create_pagination', '', '9', '3.3.13', 'y'), ('11', 'Structure_ext', 'wygwam_config', 'wygwam_config', '', '10', '3.3.13', 'y'), ('12', 'Structure_ext', 'core_template_route', 'core_template_route', '', '10', '3.3.13', 'y'), ('13', 'Structure_ext', 'entry_submission_end', 'entry_submission_end', '', '10', '3.3.13', 'y'), ('14', 'Structure_ext', 'channel_form_submit_entry_end', 'channel_form_submit_entry_end', '', '10', '3.3.13', 'y'), ('15', 'Structure_ext', 'template_post_parse', 'template_post_parse', '', '10', '3.3.13', 'y');
COMMIT;

-- ----------------------------
--  Table structure for `exp_field_formatting`
-- ----------------------------
DROP TABLE IF EXISTS `exp_field_formatting`;
CREATE TABLE `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_field_formatting`
-- ----------------------------
BEGIN;
INSERT INTO `exp_field_formatting` VALUES ('1', '1', 'none'), ('2', '1', 'br'), ('3', '1', 'markdown'), ('4', '1', 'xhtml'), ('5', '2', 'none'), ('6', '2', 'br'), ('7', '2', 'markdown'), ('8', '2', 'xhtml'), ('9', '3', 'none'), ('10', '3', 'br'), ('11', '3', 'markdown'), ('12', '3', 'xhtml'), ('13', '4', 'none'), ('14', '4', 'br'), ('15', '4', 'markdown'), ('16', '4', 'xhtml'), ('17', '5', 'none'), ('18', '5', 'br'), ('19', '5', 'markdown'), ('20', '5', 'xhtml'), ('21', '6', 'none'), ('22', '6', 'br'), ('23', '6', 'markdown'), ('24', '6', 'xhtml'), ('25', '7', 'none'), ('26', '7', 'br'), ('27', '7', 'markdown'), ('28', '7', 'xhtml'), ('29', '8', 'none'), ('30', '8', 'br'), ('31', '8', 'markdown'), ('32', '8', 'xhtml'), ('33', '9', 'none'), ('34', '9', 'br'), ('35', '9', 'markdown'), ('36', '9', 'xhtml'), ('37', '10', 'none'), ('38', '10', 'br'), ('39', '10', 'markdown'), ('40', '10', 'xhtml'), ('41', '11', 'none'), ('42', '11', 'br'), ('43', '11', 'markdown'), ('44', '11', 'xhtml'), ('45', '12', 'none'), ('46', '12', 'br'), ('47', '12', 'markdown'), ('48', '12', 'xhtml'), ('49', '13', 'none'), ('50', '13', 'br'), ('51', '13', 'markdown'), ('52', '13', 'xhtml'), ('68', '17', 'xhtml'), ('67', '17', 'markdown'), ('66', '17', 'br'), ('65', '17', 'none'), ('75', '19', 'markdown'), ('74', '19', 'br'), ('73', '19', 'none'), ('76', '19', 'xhtml'), ('77', '20', 'none'), ('78', '20', 'br'), ('79', '20', 'markdown'), ('80', '20', 'xhtml'), ('81', '21', 'none'), ('82', '21', 'br'), ('83', '21', 'markdown'), ('84', '21', 'xhtml'), ('85', '22', 'none'), ('86', '22', 'br'), ('87', '22', 'markdown'), ('88', '22', 'xhtml'), ('89', '23', 'none'), ('90', '23', 'br'), ('91', '23', 'markdown'), ('92', '23', 'xhtml'), ('93', '24', 'none'), ('94', '24', 'br'), ('95', '24', 'markdown'), ('96', '24', 'xhtml'), ('97', '25', 'none'), ('98', '25', 'br'), ('99', '25', 'markdown'), ('100', '25', 'xhtml');
COMMIT;

-- ----------------------------
--  Table structure for `exp_field_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_field_groups`;
CREATE TABLE `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_field_groups`
-- ----------------------------
BEGIN;
INSERT INTO `exp_field_groups` VALUES ('1', '1', 'Default');
COMMIT;

-- ----------------------------
--  Table structure for `exp_fieldtypes`
-- ----------------------------
DROP TABLE IF EXISTS `exp_fieldtypes`;
CREATE TABLE `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_fieldtypes`
-- ----------------------------
BEGIN;
INSERT INTO `exp_fieldtypes` VALUES ('1', 'select', '1.0', 'YTowOnt9', 'n'), ('2', 'text', '1.0', 'YTowOnt9', 'n'), ('3', 'textarea', '1.0', 'YTowOnt9', 'n'), ('4', 'date', '1.0', 'YTowOnt9', 'n'), ('5', 'file', '1.0', 'YTowOnt9', 'n'), ('6', 'grid', '1.0', 'YTowOnt9', 'n'), ('7', 'multi_select', '1.0', 'YTowOnt9', 'n'), ('8', 'checkboxes', '1.0', 'YTowOnt9', 'n'), ('9', 'radio', '1.0', 'YTowOnt9', 'n'), ('10', 'relationship', '1.0', 'YTowOnt9', 'n'), ('11', 'rte', '1.0', 'YTowOnt9', 'n'), ('12', 'assets', '2.2.1', 'YToxOntzOjExOiJsaWNlbnNlX2tleSI7czozNjoiZDA0YjI2MjAtNmFhNy00NmExLTk2NjgtOWU3OWVmMWE0NmUzIjt9', 'y'), ('13', 'matrix', '2.5.8', 'YTowOnt9', 'y'), ('14', 'wygwam', '3.2.1', 'YToyOntzOjExOiJsaWNlbnNlX2tleSI7czowOiIiO3M6MTI6ImZpbGVfYnJvd3NlciI7czo2OiJhc3NldHMiO30=', 'y'), ('15', 'structure', '3.3.13', 'YToxOntzOjE5OiJzdHJ1Y3R1cmVfbGlzdF90eXBlIjtzOjU6InBhZ2VzIjt9', 'n'), ('16', 'videolink', '0.2.0', 'YTowOnt9', 'n');
COMMIT;

-- ----------------------------
--  Table structure for `exp_file_categories`
-- ----------------------------
DROP TABLE IF EXISTS `exp_file_categories`;
CREATE TABLE `exp_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_file_dimensions`
-- ----------------------------
DROP TABLE IF EXISTS `exp_file_dimensions`;
CREATE TABLE `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_file_watermarks`
-- ----------------------------
DROP TABLE IF EXISTS `exp_file_watermarks`;
CREATE TABLE `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(4) unsigned DEFAULT NULL,
  `wm_vrt_offset` int(4) unsigned DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_files`
-- ----------------------------
DROP TABLE IF EXISTS `exp_files`;
CREATE TABLE `exp_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_files`
-- ----------------------------
BEGIN;
INSERT INTO `exp_files` VALUES ('3', '1', 'logos.png', '1', '/Users/macbook/Development/CreativeNerds/ideasFoundation/site/html/images/uploads/partners/logos.png', 'image/png', 'logos.png', '2153', null, null, null, '1', '1382885247', '1', '1382885247', '20 67'), ('2', '1', 'creativenerds_1.png', '1', '/Users/macbook/Development/CreativeNerds/ideasFoundation/site/html/images/uploads/partners/creativenerds.png', 'image/png', 'creativenerds.png', '5096', null, null, null, '1', '1382797830', '1', '1382797830', '101 176');
COMMIT;

-- ----------------------------
--  Table structure for `exp_global_variables`
-- ----------------------------
DROP TABLE IF EXISTS `exp_global_variables`;
CREATE TABLE `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_grid_columns`
-- ----------------------------
DROP TABLE IF EXISTS `exp_grid_columns`;
CREATE TABLE `exp_grid_columns` (
  `col_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned DEFAULT NULL,
  `content_type` varchar(50) DEFAULT NULL,
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_type` varchar(50) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_instructions` text,
  `col_required` char(1) DEFAULT NULL,
  `col_search` char(1) DEFAULT NULL,
  `col_width` int(3) unsigned DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `field_id` (`field_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_grid_columns`
-- ----------------------------
BEGIN;
INSERT INTO `exp_grid_columns` VALUES ('1', '2', 'channel', '0', 'text', 'Point', 'point', 'Provide a short pointer for the homepage section', 'n', 'n', '0', '{\"field_fmt\":\"none\",\"field_content_type\":\"all\",\"field_text_direction\":\"ltr\",\"field_maxl\":\"\",\"field_required\":\"n\"}'), ('2', '3', 'channel', '0', 'assets', 'Logo Image', 'logo_image', 'Provide a logo', 'n', 'n', '0', '{\"filedirs\":\"all\",\"view\":\"thumbs\",\"thumb_size\":\"small\",\"show_filenames\":\"n\",\"show_cols\":[\"name\"],\"multi\":\"y\",\"field_fmt\":\"none\",\"field_show_fmt\":\"n\",\"field_type\":\"assets\",\"field_required\":\"n\"}'), ('3', '3', 'channel', '1', 'text', 'Partner Website', 'partner_website', 'Provide a URL (including http://www.)', 'n', 'n', '0', '{\"field_fmt\":\"none\",\"field_content_type\":\"all\",\"field_text_direction\":\"ltr\",\"field_maxl\":\"\",\"field_required\":\"n\"}'), ('4', '12', 'channel', '0', 'text', 'Tab Title', 'tab_title', 'This is the title of the tab', 'n', 'n', '0', '{\"field_fmt\":\"none\",\"field_content_type\":\"all\",\"field_text_direction\":\"ltr\",\"field_maxl\":\"256\",\"field_required\":\"n\"}'), ('5', '12', 'channel', '1', 'rte', 'Tab Body', 'tab_body', 'This is the body copy of the tab', 'n', 'n', '0', '{\"field_ta_rows\":\"6\",\"field_text_direction\":\"ltr\",\"field_required\":\"n\"}'), ('6', '12', 'channel', '2', 'assets', 'Tab Image', 'tab_image', '', 'n', 'n', '0', '{\"filedirs\":\"all\",\"view\":\"thumbs\",\"thumb_size\":\"small\",\"show_filenames\":\"n\",\"show_cols\":[\"name\"],\"multi\":\"y\",\"field_fmt\":\"none\",\"field_show_fmt\":\"n\",\"field_type\":\"assets\",\"field_required\":\"n\"}'), ('12', '24', 'channel', '0', 'text', 'Heading', 'heading', '', 'n', 'n', '0', '{\"field_fmt\":\"none\",\"field_content_type\":\"all\",\"field_text_direction\":\"ltr\",\"field_maxl\":\"256\",\"field_required\":\"n\"}'), ('13', '24', 'channel', '1', 'rte', 'Copy', 'copy', '', 'n', 'n', '0', '{\"field_ta_rows\":\"10\",\"field_text_direction\":\"ltr\",\"field_required\":\"n\"}'), ('10', '19', 'channel', '0', 'text', 'Tab Title', 'tab_title', '', 'n', 'n', '0', '{\"field_fmt\":\"none\",\"field_content_type\":\"all\",\"field_text_direction\":\"ltr\",\"field_maxl\":\"256\",\"field_required\":\"n\"}'), ('11', '19', 'channel', '1', 'rte', 'Tab Body', 'tab_body', '', 'n', 'n', '0', '{\"field_ta_rows\":\"10\",\"field_text_direction\":\"ltr\",\"field_required\":\"n\"}');
COMMIT;

-- ----------------------------
--  Table structure for `exp_html_buttons`
-- ----------------------------
DROP TABLE IF EXISTS `exp_html_buttons`;
CREATE TABLE `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_html_buttons`
-- ----------------------------
BEGIN;
INSERT INTO `exp_html_buttons` VALUES ('1', '1', '0', 'b', '<strong>', '</strong>', 'b', '1', '1', 'btn_b'), ('2', '1', '0', 'i', '<em>', '</em>', 'i', '2', '1', 'btn_i'), ('3', '1', '0', 'blockquote', '<blockquote>', '</blockquote>', 'q', '3', '1', 'btn_blockquote'), ('4', '1', '0', 'a', '<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>', '</a>', 'a', '4', '1', 'btn_a'), ('5', '1', '0', 'img', '<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />', '', '', '5', '1', 'btn_img');
COMMIT;

-- ----------------------------
--  Table structure for `exp_layout_publish`
-- ----------------------------
DROP TABLE IF EXISTS `exp_layout_publish`;
CREATE TABLE `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_layout_publish`
-- ----------------------------
BEGIN;
INSERT INTO `exp_layout_publish` VALUES ('1', '1', '1', '2', 'a:5:{s:7:\"publish\";a:24:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:1;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:2;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:3;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:4;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:5;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:6;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:7;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:8;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:9;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:10;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:11;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:12;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:13;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:17;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:19;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:20;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:21;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:22;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:23;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:24;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:25;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:5:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:8:\"seo_lite\";a:4:{s:10:\"_tab_label\";s:8:\"SEO Lite\";s:24:\"seo_lite__seo_lite_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:27:\"seo_lite__seo_lite_keywords\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:30:\"seo_lite__seo_lite_description\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'), ('3', '1', '1', '3', 'a:5:{s:7:\"publish\";a:24:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:1;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:2;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:3;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:4;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:5;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:6;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:7;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:8;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:9;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:10;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:11;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:12;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:13;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:17;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:19;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:20;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:21;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:22;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:23;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:24;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:25;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:5:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:8:\"seo_lite\";a:4:{s:10:\"_tab_label\";s:8:\"SEO Lite\";s:24:\"seo_lite__seo_lite_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:27:\"seo_lite__seo_lite_keywords\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:30:\"seo_lite__seo_lite_description\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'), ('5', '1', '1', '1', 'a:6:{s:7:\"publish\";a:24:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:1;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:2;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:3;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:4;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:5;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:6;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:7;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:8;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:9;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:10;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:11;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:12;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:13;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:17;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:19;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:20;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:21;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:22;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:23;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:24;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:25;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:5:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:6:{s:10:\"_tab_label\";s:9:\"Structure\";s:20:\"structure__parent_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:17:\"structure__hidden\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:26:\"structure__listing_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:8:\"seo_lite\";a:4:{s:10:\"_tab_label\";s:8:\"SEO Lite\";s:24:\"seo_lite__seo_lite_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:27:\"seo_lite__seo_lite_keywords\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:30:\"seo_lite__seo_lite_description\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'), ('6', '1', '1', '4', 'a:6:{s:7:\"publish\";a:24:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:1;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:2;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:3;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:4;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:5;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:6;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:7;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:8;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:9;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:10;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:11;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:12;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:13;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:17;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:19;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:20;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:21;a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:22;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:23;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:25;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:24;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:5:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"structure\";a:3:{s:10:\"_tab_label\";s:9:\"Structure\";s:14:\"structure__uri\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:22:\"structure__template_id\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:8:\"seo_lite\";a:4:{s:10:\"_tab_label\";s:8:\"SEO Lite\";s:24:\"seo_lite__seo_lite_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:27:\"seo_lite__seo_lite_keywords\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:30:\"seo_lite__seo_lite_description\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');
COMMIT;

-- ----------------------------
--  Table structure for `exp_matrix_cols`
-- ----------------------------
DROP TABLE IF EXISTS `exp_matrix_cols`;
CREATE TABLE `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_matrix_data`
-- ----------------------------
DROP TABLE IF EXISTS `exp_matrix_data`;
CREATE TABLE `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `is_draft` tinyint(1) unsigned DEFAULT '0',
  `row_order` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_member_bulletin_board`
-- ----------------------------
DROP TABLE IF EXISTS `exp_member_bulletin_board`;
CREATE TABLE `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_member_data`
-- ----------------------------
DROP TABLE IF EXISTS `exp_member_data`;
CREATE TABLE `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_member_data`
-- ----------------------------
BEGIN;
INSERT INTO `exp_member_data` VALUES ('1');
COMMIT;

-- ----------------------------
--  Table structure for `exp_member_fields`
-- ----------------------------
DROP TABLE IF EXISTS `exp_member_fields`;
CREATE TABLE `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_member_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_member_groups`;
CREATE TABLE `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_member_groups`
-- ----------------------------
BEGIN;
INSERT INTO `exp_member_groups` VALUES ('1', '1', 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', '0', 'y', '20', '60', 'y', 'y', 'y', 'y', 'y'), ('2', '1', 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', '60', 'n', '20', '60', 'n', 'n', 'n', 'n', 'n'), ('3', '1', 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', '15', 'n', '20', '60', 'n', 'n', 'n', 'n', 'n'), ('4', '1', 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', '15', 'n', '20', '60', 'n', 'n', 'n', 'n', 'n'), ('5', '1', 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', '10', 'y', '20', '60', 'y', 'n', 'n', 'y', 'y');
COMMIT;

-- ----------------------------
--  Table structure for `exp_member_homepage`
-- ----------------------------
DROP TABLE IF EXISTS `exp_member_homepage`;
CREATE TABLE `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_member_homepage`
-- ----------------------------
BEGIN;
INSERT INTO `exp_member_homepage` VALUES ('1', 'l', '1', 'l', '2', 'n', '0', 'r', '1', 'n', '0', 'r', '2', 'r', '0', 'l', '0');
COMMIT;

-- ----------------------------
--  Table structure for `exp_member_search`
-- ----------------------------
DROP TABLE IF EXISTS `exp_member_search`;
CREATE TABLE `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_members`
-- ----------------------------
DROP TABLE IF EXISTS `exp_members`;
CREATE TABLE `exp_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '28',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  `rte_enabled` char(1) NOT NULL DEFAULT 'y',
  `rte_toolset_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_members`
-- ----------------------------
BEGIN;
INSERT INTO `exp_members` VALUES ('1', '1', 'admin', 'CreativeNerds', '94607ba96e7ef2a9605f43ed6ee7356206281db7599f1a64e6fff66455fab8fedea9d317d419373fa2bac49be2d0cdef3bf51863d2d86d51a45d55a7a4a23a82', 'JmjZ!L8S2\\`>pLTYeY).@s=A=?1hYtkD;dWh3g&*}yF@Amfh-j#>.P+cmQF>|G#:Z\"M$6kpZuVLk;Z\"*WILX,,f/WboQd|>[]RkPbuQ4I&QBA=m.cqq7IzH(ypsB%kiv', '199798d9f2752836533514e1cbc8c1c3adaf91c0', '96b0e3e6b4821e2c426ed84ae899b89c65412eef', null, 'sam@creativenerds.org', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0', 'y', '0', '0', '127.0.0.1', '1382553818', '1382988026', '1383076573', '5', '0', '0', '0', '1383071750', '0', '0', '0', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'Europe/London', 'us', null, null, null, null, '28', null, '18', '', 'Structure|C=addons_modules&M=show_module_cp&module=structure|1', 'n', '0', 'y', '0');
COMMIT;

-- ----------------------------
--  Table structure for `exp_message_attachments`
-- ----------------------------
DROP TABLE IF EXISTS `exp_message_attachments`;
CREATE TABLE `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_message_copies`
-- ----------------------------
DROP TABLE IF EXISTS `exp_message_copies`;
CREATE TABLE `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_message_data`
-- ----------------------------
DROP TABLE IF EXISTS `exp_message_data`;
CREATE TABLE `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_message_folders`
-- ----------------------------
DROP TABLE IF EXISTS `exp_message_folders`;
CREATE TABLE `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_message_folders`
-- ----------------------------
BEGIN;
INSERT INTO `exp_message_folders` VALUES ('1', 'InBox', 'Sent', '', '', '', '', '', '', '', '');
COMMIT;

-- ----------------------------
--  Table structure for `exp_message_listed`
-- ----------------------------
DROP TABLE IF EXISTS `exp_message_listed`;
CREATE TABLE `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_module_member_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_module_member_groups`;
CREATE TABLE `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_modules`
-- ----------------------------
DROP TABLE IF EXISTS `exp_modules`;
CREATE TABLE `exp_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_modules`
-- ----------------------------
BEGIN;
INSERT INTO `exp_modules` VALUES ('1', 'Comment', '2.3.1', 'y', 'n'), ('2', 'Email', '2.0', 'n', 'n'), ('3', 'Jquery', '1.0', 'n', 'n'), ('4', 'Channel', '2.0.1', 'n', 'n'), ('5', 'Member', '2.1', 'n', 'n'), ('6', 'Stats', '2.0', 'n', 'n'), ('7', 'Rte', '1.0.1', 'y', 'n'), ('8', 'Assets', '2.2.1', 'y', 'n'), ('9', 'Wygwam', '3.2.1', 'y', 'n'), ('10', 'Deeploy_helper', '2.0.3', 'y', 'n'), ('11', 'Structure', '3.3.13', 'y', 'y'), ('12', 'Seo_lite', '1.4.4', 'y', 'y');
COMMIT;

-- ----------------------------
--  Table structure for `exp_online_users`
-- ----------------------------
DROP TABLE IF EXISTS `exp_online_users`;
CREATE TABLE `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_online_users`
-- ----------------------------
BEGIN;
INSERT INTO `exp_online_users` VALUES ('6', '1', '1', 'n', 'CreativeNerds', '127.0.0.1', '1383073421', ''), ('3', '1', '1', 'n', 'CreativeNerds', '127.0.0.1', '1383073421', ''), ('4', '1', '1', 'n', 'CreativeNerds', '127.0.0.1', '1383073421', ''), ('5', '1', '1', 'n', 'CreativeNerds', '127.0.0.1', '1383073421', ''), ('7', '1', '1', 'n', 'CreativeNerds', '127.0.0.1', '1383073421', ''), ('10', '1', '1', 'n', 'CreativeNerds', '127.0.0.1', '1383073421', ''), ('9', '1', '1', 'n', 'CreativeNerds', '127.0.0.1', '1383073421', ''), ('13', '1', '1', 'n', 'CreativeNerds', '127.0.0.1', '1383076573', ''), ('12', '1', '1', 'n', 'CreativeNerds', '127.0.0.1', '1383073421', '');
COMMIT;

-- ----------------------------
--  Table structure for `exp_password_lockout`
-- ----------------------------
DROP TABLE IF EXISTS `exp_password_lockout`;
CREATE TABLE `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_password_lockout`
-- ----------------------------
BEGIN;
INSERT INTO `exp_password_lockout` VALUES ('1', '1382720180', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36', 'admin');
COMMIT;

-- ----------------------------
--  Table structure for `exp_relationships`
-- ----------------------------
DROP TABLE IF EXISTS `exp_relationships`;
CREATE TABLE `exp_relationships` (
  `relationship_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `child_id` int(10) unsigned NOT NULL DEFAULT '0',
  `field_id` int(10) unsigned NOT NULL DEFAULT '0',
  `grid_field_id` int(10) unsigned NOT NULL DEFAULT '0',
  `grid_col_id` int(10) unsigned NOT NULL DEFAULT '0',
  `grid_row_id` int(10) unsigned NOT NULL DEFAULT '0',
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`relationship_id`),
  KEY `parent_id` (`parent_id`),
  KEY `child_id` (`child_id`),
  KEY `field_id` (`field_id`),
  KEY `grid_row_id` (`grid_row_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_remember_me`
-- ----------------------------
DROP TABLE IF EXISTS `exp_remember_me`;
CREATE TABLE `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_reset_password`
-- ----------------------------
DROP TABLE IF EXISTS `exp_reset_password`;
CREATE TABLE `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_revision_tracker`
-- ----------------------------
DROP TABLE IF EXISTS `exp_revision_tracker`;
CREATE TABLE `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_rte_tools`
-- ----------------------------
DROP TABLE IF EXISTS `exp_rte_tools`;
CREATE TABLE `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `class` varchar(75) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_rte_tools`
-- ----------------------------
BEGIN;
INSERT INTO `exp_rte_tools` VALUES ('1', 'Blockquote', 'Blockquote_rte', 'y'), ('2', 'Bold', 'Bold_rte', 'y'), ('3', 'Headings', 'Headings_rte', 'y'), ('4', 'Image', 'Image_rte', 'y'), ('5', 'Italic', 'Italic_rte', 'y'), ('6', 'Link', 'Link_rte', 'y'), ('7', 'Ordered List', 'Ordered_list_rte', 'y'), ('8', 'Underline', 'Underline_rte', 'y'), ('9', 'Unordered List', 'Unordered_list_rte', 'y'), ('10', 'View Source', 'View_source_rte', 'y');
COMMIT;

-- ----------------------------
--  Table structure for `exp_rte_toolsets`
-- ----------------------------
DROP TABLE IF EXISTS `exp_rte_toolsets`;
CREATE TABLE `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `tools` text,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_rte_toolsets`
-- ----------------------------
BEGIN;
INSERT INTO `exp_rte_toolsets` VALUES ('1', '0', 'Default', '3|2|5|1|9|7|6|4|10', 'y');
COMMIT;

-- ----------------------------
--  Table structure for `exp_security_hashes`
-- ----------------------------
DROP TABLE IF EXISTS `exp_security_hashes`;
CREATE TABLE `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  `used` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM AUTO_INCREMENT=1301 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_security_hashes`
-- ----------------------------
BEGIN;
INSERT INTO `exp_security_hashes` VALUES ('1', '1382553841', '0', 'cfeae78ab17d15a593cda5a29983778141edb65d', '0'), ('2', '1382554116', '0', '65dddf40a65d47a2d5db9828ae085f0f86aa1662', '0'), ('3', '1382554116', '0', '5283258e6e16a9c1df19b1f9ab1c1e0a99328515', '0'), ('4', '1382554116', '0', '67f92b3e84fe62065ae92cef0e56fbc1041ea2dd', '1'), ('5', '1382554121', '0', '12262f449fcf3e7ce0bb5c73872ccf2f524f831b', '0'), ('6', '1382554121', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '029f4d92be480a69a076cfcca4832ab039cdc9fa', '0'), ('7', '1382554575', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '891305a81ae26905a48312b49d983dc507e3b40e', '0'), ('8', '1382554895', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', 'b0c30494f8cb1e4c3301fd28a39ba7ef4a277550', '0'), ('9', '1382554897', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', 'c0b1b431f573d768aeb854db3f07433338475d35', '0'), ('10', '1382554905', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', 'ee66e303283b9e010f162cf8becaefe4975b9e69', '0'), ('11', '1382554908', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '1c773ee7ae0560ca1351d9bbdbd15257d162fc14', '0'), ('12', '1382554908', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', 'f1ff38f429a122c5f76381350ec2c77605d20b70', '1'), ('13', '1382554910', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '365a11c3f17890ec3f093e6fd58fb2169f2054b0', '0'), ('14', '1382554911', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '0ffda35ff27b44aa99f2734fd618f3bb381b20a5', '0'), ('15', '1382554914', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '76fe259599b3d89d0c4bbf2c501849a6f9f6dc4f', '0'), ('16', '1382554914', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '93052f3e96606f8e2c83836e00fa56e81c8d979b', '1'), ('17', '1382554915', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '99d473d7df6c1da323c010a1293ae35347b0cf55', '0'), ('18', '1382554916', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '7f9994f903feee08b978405c7f371613dca0dca1', '0'), ('19', '1382554918', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '7e9f55b46e813433eafe7a1646804bd967ce0860', '0'), ('20', '1382554920', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', 'f4c84cb8f38c73b2669fedc9fe8cea22eb756af4', '0'), ('21', '1382554920', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '7a81348eac42ec48ea4257d7179047b7127ef1c2', '1'), ('22', '1382554922', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '8285f82e63311fc7166adffd872798168fb24bfa', '0'), ('23', '1382554922', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '68d8791b86f96e68f1f8d7469506c754762c5e22', '0'), ('24', '1382554925', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '1bddb85b308f49ea23842a545bd76d561ca302de', '0'), ('25', '1382554925', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '9c66a5295c80688d7159a81d79bd6056ba3cb246', '0'), ('26', '1382554933', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '23ad8e501d60f3a03e48a56977cdafaea640b44f', '0'), ('27', '1382555002', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '834dbc3279a6a741ec3850ec9cb976c89819bab0', '0'), ('28', '1382555004', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '2345936e0b47c6fb6824d63f05ac31a63c7413c5', '0'), ('29', '1382555004', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', 'dedddcf950a5219fb188a8b0d95e6340cbbc2082', '1'), ('30', '1382555006', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '64632a957406b926283ed119c7f97bb1882317e7', '0'), ('31', '1382555008', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', 'edc4625f201d6d18ae7b31dae97962016286e35f', '0'), ('32', '1382555013', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', 'edffed1e7cf47596ee8d07fede614c851afc9c78', '0'), ('33', '1382555015', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '94ae323fd9da845bd7f98626a6ebbc49df0ed4db', '0'), ('34', '1382555022', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', 'f5f342ab9f4645b6a1fc02fd1b3c751a9cfb0e6d', '0'), ('35', '1382555022', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', 'f351f7b5b8db9a07c1a9887c1541566406ede656', '1'), ('36', '1382555024', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '91332af89859c80f9249b3ff3e03ba6978437be3', '0'), ('37', '1382555024', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', '1cce07263afaecddd198d729aea786f92e68b597', '0'), ('38', '1382555027', '23d9a01e28d9cf0bf7e057ea57560f8ae0b41968', 'c7c86f0d483132db15412f142d2ee029bd35db87', '0'), ('39', '1382558023', 'f659dd0e7fe3367ae6819a5b2c9e4f4feeadaa77', '0d4119f2146f65076af987ab5030b3638f4b7af5', '0'), ('40', '1382558025', 'f659dd0e7fe3367ae6819a5b2c9e4f4feeadaa77', 'eba401db56a201ed7c146fc286b53ce0b137276d', '1'), ('41', '1382558033', 'f659dd0e7fe3367ae6819a5b2c9e4f4feeadaa77', 'c654000bb9f3f94969f754f7b50b8c2f14fcf717', '0'), ('42', '1382558033', 'f659dd0e7fe3367ae6819a5b2c9e4f4feeadaa77', 'ddc01cc76b9615b7e6cc7c5bd21e4549ff42f974', '0'), ('43', '1382558035', 'f659dd0e7fe3367ae6819a5b2c9e4f4feeadaa77', '037cbc668006c6b9722603bea8166571b608f8c4', '0'), ('44', '1382558036', 'f659dd0e7fe3367ae6819a5b2c9e4f4feeadaa77', '779117074ef66bb78639ff161fdf68ce7d4852c2', '0'), ('45', '1382558037', 'f659dd0e7fe3367ae6819a5b2c9e4f4feeadaa77', '44532f5d3b0ca1f70815ee38a14f5488d030e98e', '0'), ('46', '1382558067', 'f659dd0e7fe3367ae6819a5b2c9e4f4feeadaa77', '3f9668e4d9135308f258172607765a9682c98e66', '1'), ('47', '1382558073', 'f659dd0e7fe3367ae6819a5b2c9e4f4feeadaa77', '6c7ce729bf9d84a8c15a731c8fd542c46a83ee03', '0'), ('48', '1382558073', 'f659dd0e7fe3367ae6819a5b2c9e4f4feeadaa77', '95904f98feb6ceb4d8e124dc58b8454fab312ff7', '0'), ('49', '1382558076', 'f659dd0e7fe3367ae6819a5b2c9e4f4feeadaa77', '7d95f50248b262b3eda191eba16adad3997c2294', '0'), ('50', '1382558118', 'f659dd0e7fe3367ae6819a5b2c9e4f4feeadaa77', '95b8acea1055e80dbe7369e41629cc78cc9a7de0', '1'), ('51', '1382558124', 'f659dd0e7fe3367ae6819a5b2c9e4f4feeadaa77', 'a35998d1e6adc6d6a79892b4fa6ffac9d8514459', '0'), ('52', '1382558124', 'f659dd0e7fe3367ae6819a5b2c9e4f4feeadaa77', 'ae9fb7c0cffac9186cce8df16864c3a95fb35637', '0'), ('53', '1382561577', 'f659dd0e7fe3367ae6819a5b2c9e4f4feeadaa77', '3a3bcad20a3993bd3ea8d831b9c0d99a9a6b7a52', '0'), ('54', '1382561591', '0', 'af4297519cfd6338de51fb59c6d86c11d131e68e', '0'), ('55', '1382561591', '0', '39ce4c65a72452c7a6e72fd6bfc79cc9d2d148bf', '0'), ('56', '1382651930', '0', '13e0f10495a3a76857e9c4bb377d70f34ad799d1', '0'), ('57', '1382651935', '0', '1993172548a4b431c3def45cd43712ff46187a38', '0'), ('58', '1382651935', '0', 'c7756afb81129b110e03acc86ef7e77c1ab7fbc3', '0'), ('59', '1382651936', '0', '5d5d8712063ec965d01a287f1ad60993f0a7fa6d', '1'), ('60', '1382651941', '0', '14946094f5c75ff8bb6ab8542d311f4397d1dcb9', '0'), ('61', '1382651942', 'b817f2706cbc8226a0f284d3f0da4b9c53f0182f', 'a39d31f593fcefe67af427c2dcc999d540ca9e6c', '0'), ('62', '1382718643', '0', 'd5716f27b102fdcccfe77047794f89648786e3da', '0'), ('63', '1382718643', '0', '059298fd1dc95d3c3ace655b6df20f4c2dbcbc41', '1'), ('64', '1382718801', '0', '9e7ba84d3cd551e4bf2732f7b4baaa9846130f39', '0'), ('65', '1382718802', '035753871df759c6bf569a0ff9c58963b69541fd', '511bc8fb6f53416fe12a6316951dbd1a53ede975', '0'), ('66', '1382720007', '035753871df759c6bf569a0ff9c58963b69541fd', '66eb6d70fd2ae9ed3105b49cfe798b3836254a89', '0'), ('67', '1382720007', '0', 'dda6a40f9a50e33741aaec677bcd0500d0974e63', '0'), ('68', '1382720180', '0', 'c386332af3c1be6a24df9aa8c2ec4cea487f6d4d', '0'), ('69', '1382720185', '0', '2b749fc1a4e66d9985de067d742824751e4c42cf', '0'), ('70', '1382720231', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '644642b12e5d282692315ae89581fc1e74f512bd', '0'), ('71', '1382720235', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'ae75926fa09c93ea2d2a0f1530083647a81633c5', '0'), ('72', '1382720237', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'd24acfe54e3331a497c2ca0113a8081a3fc6105b', '1'), ('73', '1382720241', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'db97c6871c29a2d48d39a23d443b74bfa1a0d37b', '0'), ('74', '1382720241', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'e04d4f8b0df904becb5c3b5d00b3cf0984fc4f78', '0'), ('75', '1382720242', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'c36728b15c776bb488559181225a27eefbe12639', '0'), ('76', '1382720245', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '5e8cdd423c463ed10de3e9135bb9b8c28f1b4b75', '0'), ('77', '1382720246', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '12fd30bc3f6fae9ae114dee5bacc591d4e7c05e4', '0'), ('78', '1382720248', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '2ae8605177b84658b61365ccd901620d400aadc3', '0'), ('79', '1382720667', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '8199382741accb15c784c13ad27638c22957e649', '0'), ('80', '1382720674', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '0ca7770edae1f5acc494e12ed11d9052890f0265', '0'), ('81', '1382720676', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'f71207f8963dafc157ee17250f3741becf56ab8a', '0'), ('82', '1382720679', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '76076a4dff5cc855231590c91c7f9e6ab58ec540', '0'), ('83', '1382720679', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '6cf16e35672fd5511a778d528664f850fb1f7365', '0'), ('84', '1382720682', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '0af44ff97b3a79acbeb241a397c4879ffc742653', '0'), ('85', '1382720684', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'e857ce5e398ac8216483d5ace62ed5535d6ccf3c', '0'), ('86', '1382720685', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'cef407b568017d584a220552f15bf02db8403bd6', '1'), ('87', '1382720713', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '16421e1735b975201a5cd1ad3ac64026e19450ae', '0'), ('88', '1382720714', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'ba96d193b750b2231874932626e91400a2cd3003', '0'), ('89', '1382720726', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'eef64cefcd0e0c76ca7ee585e5deafcf42f1330d', '1'), ('90', '1382720805', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'ba693a104b552eb865923ceaa89f97d980be7653', '0'), ('91', '1382720805', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '6745424e91a7c984ef3b588936f76919b0f2d7c2', '0'), ('92', '1382720809', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '0c08429744c9676f15a6e9a71dda32e206e65937', '1'), ('93', '1382720816', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '18db035ce37232f8cfebab3936a3e0ae622d7eec', '0'), ('94', '1382720816', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'caa28d9ddf4b6ab465de987ab3fcf189e9d194c3', '0'), ('95', '1382720828', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '9fffc6a4da0f914ff2a419857d3e3ed7a0ed304a', '1'), ('96', '1382720923', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'dda0aa4a3d02dce8d4a19f11553e1276c1a7d5a2', '0'), ('97', '1382720924', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '1f07e920423e8c81780f90f790901894972cf13f', '0'), ('98', '1382720941', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '3a2c23c2b619b7a3943ffcb4b2b4127b72c2f9f1', '1'), ('99', '1382721011', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'cfa3c52c6d5ed174a2446c802c4c2114135f5b9b', '0'), ('100', '1382721012', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'e3a5cd28258ea110dedbd91d0e00acd9a3ff24c7', '0'), ('101', '1382721017', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '0c3ecd96a2e318ab7c6e450656276ea1743ea43f', '1'), ('102', '1382721059', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '33018260745869aecd3a09633232fc68e6f87bc1', '0'), ('103', '1382721060', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '4832514dc1e8d301aa0ed1a6d0343d827397d0ea', '0'), ('104', '1382721102', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '0875bbc7924241857f08bc62dee389db0f0d4b73', '1'), ('105', '1382721134', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'f5c3c169f7d316003e11414e6048c244072fe0d7', '0'), ('106', '1382721139', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '5995af762de754e7b7b89514e3a08a0ecf17d914', '0'), ('107', '1382721221', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '6558a1389cb0bb76a9f89a03155f4926e72b4651', '1'), ('108', '1382721252', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '3a6bca5659ed215dc4cae2d6f4cfedaafba7d42d', '0'), ('109', '1382721253', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '803a786b0f55832cea6af95471760bef39e120be', '0'), ('110', '1382721262', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '029e3b5f21896a623f8e7ce9e9ec50215086cf2c', '1'), ('111', '1382721289', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '56da906220cbbe80c7cac7b14e81b81f329ea40e', '0'), ('112', '1382721289', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '370f3b28f39bdf9a7a4e8dcf5ebb030cfc7374a2', '0'), ('113', '1382721292', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'af156000eb2c7dd35f72f9cb25ba0c5b6ab6c9bd', '1'), ('114', '1382721391', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'b54a1ab680d777ef7cdc58b2db73c5afd8f014a7', '0'), ('115', '1382721391', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '7700b61e2cd0c6af4ab463acc83c40309a37267f', '0'), ('116', '1382721400', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '1234e24bd8c8b5d379560a18d8510a4863fde930', '1'), ('117', '1382721423', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '4015d441ef0acc8de0eb853992e24d1690866532', '0'), ('118', '1382721423', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '855bce6a9636115cfffa4d6c04159abe898cbd7f', '0'), ('119', '1382721427', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'd12d1071f439e5a6fcc70756bfb2762350be7533', '0'), ('120', '1382721430', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '429c8bb940be7bbcae15be352b8d0acc1399a74f', '0'), ('121', '1382721432', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '9dbc6170f6c3d0bf7759cd315a9ac93477167809', '0'), ('122', '1382721433', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'bd3fb581e010fef1cda794054c6689eac893d3af', '0'), ('123', '1382721439', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '0e82e7d21a3f984736bc86c38b08e1e5f551ffc3', '0'), ('124', '1382721441', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '10f7d8d1a1e5d97dd00442f1b119f19a1f830459', '1'), ('125', '1382721443', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '9fd7830cffab6a9ac8adcedd5794ac777a542e6b', '0'), ('126', '1382721443', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '75e056a26cb357e2c60b4280dafbed8e0d169a8a', '0'), ('127', '1382721444', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '682a3e4c25dd79407d32e65f59fe2e40ca29361f', '0'), ('128', '1382721446', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'f5e56e58b0436060b91d863d6c83b925a695ac01', '0'), ('129', '1382721446', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'eaa261ed26b8007f948be9b910d037a83b547f01', '0'), ('130', '1382721481', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '8a5df1de775bb590375d979aa516f5f1f5e295ee', '0'), ('131', '1382721481', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '4ff6fb90fc5b92c7080308778032a6980d676bb1', '0'), ('132', '1382721481', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '34c3f79d65d0632b59f333a3f547eee189d5f993', '0'), ('133', '1382721481', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '6c4a9ed7a0d80d39c7ac73e1073656ec8d837822', '0'), ('134', '1382721505', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'fd21273d48e44c371bc4d9254f56f861c843d735', '0'), ('135', '1382721507', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'c9b498236db4df62b5d45ceeb60f5bae3a437f6c', '0'), ('136', '1382721511', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'd995f82d0d603bdf220e829e2acfb541f4bd83c2', '1'), ('137', '1382721545', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '42436e8085a92c2c4c0508c5cb7800e8f81c7909', '0'), ('138', '1382721545', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '85bb4d1f73742ee2af31f3433a81dca0d178e327', '0'), ('139', '1382721555', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '725e58e5de633d8329939a235714967e2b6e41ac', '0'), ('140', '1382721557', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '299153af521de4d6c424d5e2258d65695de118a0', '0'), ('141', '1382721557', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '204de8c4fa439c3c13028754a91a11bc5d69f376', '0'), ('142', '1382721617', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '06189de4808a36aaeeee6d288e049618ce25d589', '0'), ('143', '1382721619', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'b4d7bab4d415d7ae2374133572338d96af5b1736', '0'), ('144', '1382721622', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '9fed5e2c0f68993b695c2065f8108b27fec6db43', '1'), ('145', '1382721887', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '30aa73c3d3dccd07523d4307efd434e515191c14', '0'), ('146', '1382721888', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '22f564b17375775cc8c258ea4b233f1a9bc012cc', '0'), ('147', '1382721891', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'a223e95e1e65848aeefb88ddfe6fe29c5ecfa561', '0'), ('148', '1382721894', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '5c0262e52a726c34218a71aebfb16b6f80d5866f', '0'), ('149', '1382721894', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '960854d1c2aacbf90f495ea2a7e4a786c650a646', '0'), ('150', '1382721896', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'a2c5224b301b9e6fb8542ceff2370146e620409a', '0'), ('151', '1382721898', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '7d9d5ceb4e1ca9de61c6b5788d2f40cec374f7f3', '1'), ('152', '1382721903', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'f5d26d19fb68ef754ab138f7f1a65177d7ab87fd', '0'), ('153', '1382721903', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '671630b24664c7eb9357521083732891f362a503', '0'), ('154', '1382721934', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'd80c7876a3af8b7fae6b2ec24a7a9ad9790d7c87', '0'), ('155', '1382721939', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '31390454f6424ed2f5ae15535cb67ed19e5e9259', '0'), ('156', '1382721940', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'f8d5c69357beb9b36b9ead40edd0c77ec3161a55', '0'), ('157', '1382721943', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'e184bbb17153fabeefd856d171a3dc7f406593e2', '1'), ('158', '1382722043', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'fd9d03aa447fd0b3441ed857ed9b88db86192ea3', '0'), ('159', '1382722044', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '2f9aa77accd8728cdd35b981726a26162cae3326', '0'), ('160', '1382722047', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '1785af30403491d6e50a63ba5f35605fbd766199', '0'), ('161', '1382722049', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'd5ab616bcfa178d09cdd8fc88a94b8a3d5f37d95', '1'), ('162', '1382722050', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '1f541c53d496de8b4d2c98bc145a86796552d368', '0'), ('163', '1382722120', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '7fb8d6f8eb6c8bdaca14a994d552c5cfe0f85526', '0'), ('164', '1382722121', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '731900010db57479dcc25f71a6347928aa5e1eb5', '0'), ('165', '1382722123', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '350a2dfaf1538be6e24f21c3e4af3d73dd3d42bc', '0'), ('166', '1382722127', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'e68a9e7f75246e83281dd05c6fcb1a202086e360', '0'), ('167', '1382722127', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'ec3b522f6b822016ecf704cc7fae844c65be7bfb', '0'), ('168', '1382722580', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'd70823c39eab9149a486d7fc1cfd31800ba58258', '0'), ('169', '1382722583', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '11e56231ae667359ba1d146318154ddd9e00006b', '0'), ('170', '1382722592', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '376b4181bde1be5f91be7eed8923a92585f177a1', '0'), ('171', '1382722597', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '74a728b9b37e67d2687f1f3d6dd448a0bb1e0e91', '1'), ('172', '1382722606', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '02b1eacbfd669859755c79c84180f53c1a17befe', '0'), ('173', '1382722606', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '9cc4dcbe86ff9bd2ee334523bc5e01afbfe5a698', '0'), ('174', '1382722608', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'ab43d2f82ff57aafac3ca90207a75681f3dad32b', '0'), ('175', '1382722610', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '7060a83557428bb35d6df140fa8eabe4a0601f9a', '0'), ('176', '1382722617', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'dfa863476df859af59bf8107ddfd2890bff74828', '0'), ('177', '1382722619', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '388ec6673dd62d35006b5425c65d399f49f07669', '0'), ('178', '1382722623', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'e99f89866d675e7e78c8b364059ecebf557ff660', '1'), ('179', '1382722634', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '39b914cd05dadf1f5339a3b9a208e03bc04b9db8', '0'), ('180', '1382722634', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '9b876d3d903d48cd1137282163c5925891a8b8d8', '0'), ('181', '1382722637', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '62aaad18a4639579053d70c66c40b53726213829', '0'), ('182', '1382722638', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '00fdb496e0204b1fef9370da3ebb4599d9272152', '0'), ('183', '1382722641', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'd939160f8a9137fcc6c1dfc10b12b3d8340f7247', '0'), ('184', '1382722643', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '4adf0423efcde9fb5a5be7e63856188cc94495eb', '0'), ('185', '1382722645', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'd32327193dda9c415d4c180b2925da3165430c5f', '0'), ('186', '1382722647', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'bcb89498626b66f507e82f7556b666b3dba0ccb0', '0'), ('187', '1382722650', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '3a0bdb58eef0ed2a3cc745313f921ffa850d936c', '0'), ('188', '1382722670', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '41952651509312aa27fbe84c63e02cd9d0d0922b', '0'), ('189', '1382722672', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '6f35f9d00afadb7462cf4b4bb434426f5dc64bc8', '1'), ('190', '1382722685', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'ea1a25ff665fdae7133ca537c0e32d4b45761272', '0'), ('191', '1382722685', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'ae803d5a9bdd0fc32024152d9e79697b9363af46', '0'), ('192', '1382722698', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '4126db3b1377a3e1a8e7381aa079509c09e30f10', '0'), ('193', '1382722708', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'e869b3a57afae6ff5c7bb5c818fbe58850103973', '0'), ('194', '1382722740', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'b846c989402b0c97497617f90fd3374514176485', '0'), ('195', '1382722742', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'a505574f4b7966ac79c3e78037518a6ec778f523', '0'), ('196', '1382722742', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '2de6e1e3ec3020ad1e89e0ca10d2373466666d60', '0'), ('197', '1382722743', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '102469f39d7b3fc8e07e6fb725f01a0728555ae3', '0'), ('198', '1382722744', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '599c9a5302199739564df9a0cabb0ef4b4896a79', '0'), ('199', '1382722744', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'c18770fa3c5b1146b4a19eae1b950870a61f1a57', '0'), ('200', '1382722750', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '822d69cf05fc9307800501e61323b3afbbc06a27', '0'), ('201', '1382722775', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '2cadbd0bca8a5de9a42ba821fa128e61b2097ec6', '0'), ('202', '1382722790', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '1bda6740d1c0286d90a93ba5451b29e8e0d9b2b8', '0'), ('203', '1382722791', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '9280fd2f71ed9018d6f7207357788431a6312e37', '0'), ('204', '1382722792', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'ec12a4a230830864d1e017bb41fb8f0b30019e6b', '0'), ('205', '1382722802', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'e102dd8f49dd9463b565ad3404d4c0fc521a322d', '0'), ('206', '1382722811', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'b43c55ddb36bf1d8122171e9447d1fae38ccd765', '0'), ('207', '1382722834', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'fa4347a70bfb4a357bd43f9ad9d34bbf7ceff631', '0'), ('208', '1382722836', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '589f2879aa9c25a6e2da6a617801182e670c9170', '0'), ('209', '1382722840', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '341b1dec9c041ff6d79bb4985e35395f89dbbc23', '1'), ('210', '1382722842', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'd0ee2336ff045f5df8e19c62f8dd4564c6fd5fc5', '0'), ('211', '1382722843', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'f3138a7b4d78f086d8eeac71830d706117ec3649', '0'), ('212', '1382722844', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'c165c53411a9629d4bd6b2f67953fb5f6ee53d9a', '0'), ('213', '1382722845', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '236d4a001163573a8afad06bc804ae5bd64ec503', '1'), ('214', '1382722848', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'f25983da611c5733f75a64d5b1a47d67f50ba00e', '0'), ('215', '1382722848', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'd8280f1869a4d3532e55dbb60c6925a98fcc9c98', '0'), ('216', '1382722850', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '123d52c06a9f4a81a5b1e0f98d9576a44b03c4c2', '0'), ('217', '1382722850', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'a54bed1539c2764090cf9cdf28f68a9767095ac5', '0'), ('218', '1382722852', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'eb35110f795d3806b06a99418edc7a7bb0f9be51', '0'), ('219', '1382722852', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '79fb07eb883e7d5c06236582338158eb7fc2dbd6', '0'), ('220', '1382722855', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '3cf274f51bb5e7dd9e27534bdfbcb864284b1471', '0'), ('221', '1382722880', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '2b503f85e679babcc15ec5f114b9d5c9ebe1948c', '0'), ('222', '1382722888', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'd7bc56a953bcc913f1d3e8351144c320bfec898b', '0'), ('223', '1382722890', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '96f5e5975aace433b5db6b718f3f39d8378d7898', '0'), ('224', '1382722893', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'a48e92935439ee2147d1ac1d0fb07ca609949995', '0'), ('225', '1382722901', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '19511ab1f546e0200d8e62b9b180af3d1f679f5f', '0'), ('226', '1382722901', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '3c683b0dea7478281f5e6142a5707cd10f19c4bb', '0'), ('227', '1382722903', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'c3a2db24c7d3ed9d6d99db7154975b66b409167b', '0'), ('228', '1382722904', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'fb67931c1f97d671da98d3b389328e7fc3816f12', '0'), ('229', '1382722907', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '52d50c8d16140867797455e8e8b2ad725c5f2170', '0'), ('230', '1382722917', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'ec3d98d3c6594541043153aedead672a1f52b890', '0'), ('231', '1382722929', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '15855db4feadfaaa7f79dcc78fedad8e372ae2b3', '0'), ('232', '1382722931', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '532f7afc9755dcf912d39f31ade2aac027b56008', '1'), ('233', '1382722932', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'a284befaa6ecb4327affcbae867199a9535fd14a', '0'), ('234', '1382722937', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'caacf1b2b0ae32d33ee5a705a5e934235a9a444e', '0'), ('235', '1382722937', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'dc115533f636e8cdc25b78f64662d4a429589ba9', '0'), ('236', '1382722940', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'e9dd48014cfd79248ee442f1e8a9bce45ae278a2', '0'), ('237', '1382722941', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '5b31d3b387b81bd3852a36966479f2e953699507', '0'), ('238', '1382722945', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '498db7f5c0a9534119a3b338a3c4fa9a5d4fc69f', '0'), ('239', '1382722959', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'cf72ca5a7d29ff6b59aa3731a8df51a00a72d694', '0'), ('240', '1382722960', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '92fa025ea85cf103a324eb8352b06e65ec309625', '1'), ('241', '1382722960', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '6a53cb47f84809cbe08b94e44cf0fd0769f0df46', '0'), ('242', '1382723010', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '4ae10c70a1170ca81fb68c1659371e60d8c9d431', '0'), ('243', '1382723030', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'e80fdddd2085f4e722200ff46d100e24c371ee4c', '0'), ('244', '1382723031', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '86a71f8c7bf5f154783171a493b64d7213fa3210', '0'), ('245', '1382723032', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '6b9fc1d845d7aa721235b39873678a2526cb5f20', '0'), ('246', '1382723032', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'd48c465a16ff1959c5408c951ff4f5f9a95169d2', '0'), ('247', '1382723056', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '7740603c8d84b3a23dd0c4367dd9bd29c2d75796', '0'), ('248', '1382723056', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '326e31800660cd72ad5c08d815660e8bdd1708dd', '0'), ('249', '1382723056', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'e04d7ae3bcaf6887d521f13b1d962690f938f17c', '0'), ('250', '1382723056', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '40ec6aeb44f9580947b8a7710543358698fc9d56', '0'), ('251', '1382723059', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '497106b04cbfea8535e5d809b0e1a4b92105b12a', '0'), ('252', '1382723068', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '07933ba6e8e0317e7752161cb875a9de446f2559', '0'), ('253', '1382723069', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '07302191cce548067fb4e00fe65c29f98c6f24f5', '0'), ('254', '1382723069', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '12e65a5c282359b0e405e44275578042f3e80251', '0'), ('255', '1382723069', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '669196d417402eda0f882873e9a813f257ae3799', '0'), ('256', '1382723069', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '98dfcfbc9383dabd1cbeb5902c4a1d862c18f84d', '0'), ('257', '1382723070', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '7c30d3f6927a4688238d7176e66f7e65db12c18f', '0'), ('258', '1382723070', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '736b4c839aa63895fae06770c9b411fefd6f4853', '0'), ('259', '1382723072', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'ffd478d54195103faa56b4b03695589e0e547390', '0'), ('260', '1382723081', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '8487617ce3a7341168dff05a6294e45b85ed14a1', '0'), ('261', '1382723083', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '71b81ef1d4c3fc135970c07d6e611354c1c566d7', '0'), ('262', '1382723096', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'b56ac205625856f76228f024d97f2180691bd80c', '1'), ('263', '1382723100', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'a89d86d1f5a1094a6e765bf35061d1e9a6c7ccd6', '0'), ('264', '1382723100', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '0ade7a9fd1f5462f4f4d8575bcf8a53295aa8dd8', '0'), ('265', '1382723103', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '35c4855297f635c2acc172cd8f7bbb2142313d09', '0'), ('266', '1382723105', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '98d91d098c71771c9c46d5d1193a42fa4fb516e4', '0'), ('267', '1382723118', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '46e671f19cc8144e85b6fc095a52697ef570a5a4', '0'), ('268', '1382723120', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'ebba0a24e6139cb0748c7ec5201f5813c62e02cc', '0'), ('269', '1382723130', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'b2cd81c2d8d84b09ee545adf71a57b73029dbaa3', '1'), ('270', '1382723132', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '30556999e3497d425dc62847a7d2444620fbdff5', '0'), ('271', '1382723138', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'c3cce1efc426412b26248f13b659d9c2d6b0004f', '0'), ('272', '1382723145', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '54c164592c6b95b364c9a298735cf626aab004fb', '1'), ('273', '1382723158', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '3f9359d940e44129bfa1453817b7d5c862f5a956', '0'), ('274', '1382723158', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'cd3e17292368182e0c681351c4187312099c5352', '0'), ('275', '1382723165', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'bdf2c08d366bff7adff2c12aae6dfa8688b3a693', '1'), ('276', '1382723167', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '9d410558e4934dafca362da6ab950434f0692b63', '0'), ('277', '1382723167', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '5a6c2dcbf9693be1642f3ef0ed6c5e57070c713e', '0'), ('278', '1382723172', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '338e9dd05e99c2a01be9996da3e80cea7d753372', '0'), ('279', '1382723173', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '3a30f802311bcb8de8939dc1475da35bfff02646', '0'), ('280', '1382723175', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'df3c2a04e01f6a4be048dc5ac0cba725fa6a48d4', '0'), ('281', '1382723177', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '6c4dbd3fa56b98c0d19244e804f5ebe506e7a27a', '0'), ('282', '1382723192', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'bfc9c793c9aa649a09f75ebf8239f1c56b4dc884', '0'), ('283', '1382723193', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '40cd95b0fab963a249873941b4193071da79af3d', '0'), ('284', '1382723195', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '3ca9ba51d8ab3043cefb07345581f30870b66a66', '0'), ('285', '1382723196', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '4670f300a8b0ccae62a6e2c4f17c772d7898dee7', '0'), ('286', '1382723197', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '65c5b3566ef96fb6f61f59723048f9a747802e03', '0'), ('287', '1382723197', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '23156cefbdf7765a49a70976f4052a059b009bd2', '0'), ('288', '1382723198', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '83f6ad64f4bf72fb105dccd913837b54206bb4ce', '0'), ('289', '1382723200', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '079386750f3d4cde51f5bc71f3ed593387e5ef54', '0'), ('290', '1382723345', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '05db52b3d46237b10ae40bdab9f513bc27dbf38a', '0'), ('291', '1382723427', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '40f765e02ae7a16fcb6c0e1686f49cc37fff54c8', '0'), ('292', '1382723429', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '383d29e05b166dd6a9ad023dee1f23a6e2e939f0', '1'), ('293', '1382723430', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '3acb67adbfe324a47baa60d5cbaf552bc0480ffd', '0'), ('294', '1382723430', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'b7e677898523708e1e649cf5477ca99227fdffb9', '0'), ('295', '1382723430', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '6eee30190c4c29cb424117e4b607951a215bc944', '0'), ('296', '1382723440', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'ea03642b74f55a0f18ba86ceb56c35077dabc662', '0'), ('297', '1382723441', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'f7e2ec4191e193b93552ad1a47000933ca0d835e', '0'), ('298', '1382723444', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '885a68c0f6db7f139ddd16dd1a0eafb7efae6b5e', '1'), ('299', '1382723449', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '480e2ff61f12c36c67b9065341916153d3deabe8', '0'), ('300', '1382723449', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'cf20f3607a94c64146657e1f709b86533184ca40', '0'), ('301', '1382723454', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '1a5a7cca2206ab4aca3b61710a5ce0494888567e', '0'), ('302', '1382723455', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '9ee303e127ab00728a1d52480a856a3edb3a5933', '0'), ('303', '1382723455', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '0e5dbde14c3d1e04a26db5e388d43aba130e700e', '0'), ('304', '1382723461', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '88ba9f6f528401c39e9d5b72fe2584ec80f70dc4', '1'), ('305', '1382723467', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'a68fedd3403486e596f94473f719f48ed5549809', '0'), ('306', '1382723468', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '18403aac7491c950016c3701fd580be62003ec29', '0'), ('307', '1382723473', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '529fccf68853012a3fe220d151f6a9b096cd55e3', '1'), ('308', '1382723476', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'ca6f9243747b073e763518b7a34566bec8889ce7', '0'), ('309', '1382723476', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'adbdc84c692ce6f8dd9f6c0b1522455c8de12b1c', '0'), ('310', '1382723496', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '7f2b11686af06977c46611a309e627ef5c83385e', '0'), ('311', '1382723499', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '219aec2bad5907e4cd70fe52fc265ffd24572f1e', '0'), ('312', '1382723502', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '1dc1525a8df3aac356fbd6aa459a6bb436423bf7', '0'), ('313', '1382723503', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '03d1f006188d5f19550d17ce9cb4dd1fc865935e', '0'), ('314', '1382723509', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '2b801e9d5a47a775fb346ba55f595b2c6f0e745a', '0'), ('315', '1382723511', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'dd590aa2fdc4836994892e927b371310360c7814', '1'), ('316', '1382723514', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '402cfccc0ed5646560159a206a97bde45ec62f07', '0'), ('317', '1382723514', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '85ec1ded00b6b6ba1d3c32b14b5b3f7ba557df20', '0'), ('318', '1382723516', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'c55867e02ef30dc0ef97fa47ca5ca556a146626b', '0'), ('319', '1382723518', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '13b6197d0f38f55c600c743ecfc7339b64f06950', '0'), ('320', '1382723529', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '60b96693de16271331eb3337cfca395931467dfa', '0'), ('321', '1382723529', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '7cc5bb68511934c329d96a7723c56e4b4d820fd1', '0'), ('322', '1382723529', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '1cd0860921aaa9ac0b756c90516e6df35f868078', '0'), ('323', '1382723533', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '761a4bd9acf7d336cb6b4a527a81544532f436c9', '0'), ('324', '1382723543', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '9569d680c8e89f12e6cfff463d7d11a283837e59', '0'), ('325', '1382723547', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'cc18dc186dbc9f74ad9e85c2edb9408efcfac5dd', '0'), ('326', '1382723549', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '2adfb1c85ec270f58fe07ee70d8066d48dc4c9a5', '0'), ('327', '1382723549', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '20bd13ec6ae494de68041ac517cf10f452e8e64d', '0'), ('328', '1382723549', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '4dde10af859a7c6325a52044faf3e311d2f377a0', '0'), ('329', '1382723549', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '32dc986208f490d78f4b20d11a960799e8e6ed5a', '0'), ('330', '1382723619', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '90da8788fceabea8483678404807f43f871f1361', '0'), ('331', '1382723632', '4fe35a5ffbe9b246c732d474764c55196388cf9f', '86524d23c5aaea501603294aefccd4909d52b70a', '0'), ('332', '1382723687', '4fe35a5ffbe9b246c732d474764c55196388cf9f', 'ec8c207f27626ca933d516a1de2a32e6ced5f370', '0'), ('333', '1382783507', '0', '36fa3d76fa7f1e1b756bdc56e1cb84244470f00f', '0'), ('334', '1382783507', '0', '22541ec07efc11db80e2563e47414ada68d74f44', '0'), ('335', '1382783518', '0', '63846ab658b12bf1b1bbe88cbc19cf731c0f9258', '0'), ('336', '1382783518', '0', 'cc933c70fd816b4ff409c99ea7f25b017aba4df3', '0'), ('337', '1382795638', '0', '33a218ac9fa721f85d5fd27dfa9d148fdcaaf232', '0'), ('338', '1382795638', '0', 'adae0db0405131926ac0c32748296a22fd94d20e', '1'), ('339', '1382795641', '0', 'c539caacd227b8c5d337bc96de39d3d996581d29', '0'), ('340', '1382795641', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'e96f0505e02c330fd86e399d3aaa86c7c074a1fa', '0'), ('341', '1382795648', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'e86b95fdb3c8b1d2daa802de528fd1aec7f73f0b', '0'), ('342', '1382795648', '3809e1552cfec0040614ea12ddfde831c7a03f10', '10eecc0b18607706173729b0a43fd46b5f1a487e', '0'), ('343', '1382795648', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'e61ae2c7a3dbfb2fd2007f033b8dc0ea9059aeff', '0'), ('344', '1382795649', '3809e1552cfec0040614ea12ddfde831c7a03f10', '668a03c2cb33e25caf12dbc225651a5ae807c46e', '0'), ('345', '1382796088', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'd5720848a3ab14ebb8863e4ec66bcab99c80d917', '0'), ('346', '1382796092', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'c76ce08bc9647606a72a18de9adda83eee8a7269', '0'), ('347', '1382796093', '3809e1552cfec0040614ea12ddfde831c7a03f10', '1bd19eeaa62e35b7133e98ed251618cc957aa620', '0'), ('348', '1382796514', '3809e1552cfec0040614ea12ddfde831c7a03f10', '266a899a400c3343cbb35889bec9ac64d5919b96', '0'), ('349', '1382796549', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'c3f8cd0206934198bb2e336ba0ee0a74ec2aa62d', '0'), ('350', '1382796580', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'db190a890ba782176a6c37e58647ff1970cf6e07', '0'), ('351', '1382796610', '3809e1552cfec0040614ea12ddfde831c7a03f10', '3156c5beebf8746583705c0b93c8c61515cdd789', '0'), ('352', '1382796682', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'f641c396e73c7a3c3b94793277c6f593af344a67', '0'), ('353', '1382796684', '3809e1552cfec0040614ea12ddfde831c7a03f10', '454c22ed43826998e2789fffa4a94797a85ebf84', '1'), ('354', '1382796684', '3809e1552cfec0040614ea12ddfde831c7a03f10', '066cfc8be89a220a86cb291ec1b77e9b517e3474', '0'), ('355', '1382796684', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'dcc789452f3068a6cc5a058cfa3bdcf4dd09dcd3', '0'), ('356', '1382796684', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'ce1050948da2b0894cb98ae1d19a5237370a183f', '0'), ('357', '1382796697', '3809e1552cfec0040614ea12ddfde831c7a03f10', '4088b1ed0820255ff5559a96ae59f6d5b60e1064', '0'), ('358', '1382796697', '3809e1552cfec0040614ea12ddfde831c7a03f10', '137458cdeeebbff3e7add945e62e30601826234b', '0'), ('359', '1382796700', '3809e1552cfec0040614ea12ddfde831c7a03f10', '505062dcc16c89fb69f41d2aa743e003865c4bb3', '0'), ('360', '1382796703', '3809e1552cfec0040614ea12ddfde831c7a03f10', '78d7f70921aa5c3a5af7aeea93261a9a46e797e9', '0'), ('361', '1382796703', '3809e1552cfec0040614ea12ddfde831c7a03f10', '6882d871c2af21ff5ebfb765b95df5ec1262b596', '0'), ('362', '1382796704', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'c7a69f0512b418d98f5b2cfc15ce35118e563b13', '0'), ('363', '1382796704', '3809e1552cfec0040614ea12ddfde831c7a03f10', '6411a5d43b4687ce319f3ada52d82e447eaa8cbd', '0'), ('364', '1382796714', '3809e1552cfec0040614ea12ddfde831c7a03f10', '18ee227b66da74c073df4a652881d88131c6a3bf', '0'), ('365', '1382796720', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'b52b025a62be893d2dc4eca733c2e140dd26c1ab', '0'), ('366', '1382796723', '3809e1552cfec0040614ea12ddfde831c7a03f10', '9af37833d33314b2037f1ca2c933d499d9eae4ca', '0'), ('367', '1382796724', '3809e1552cfec0040614ea12ddfde831c7a03f10', '2623227718153618b124eafc31fccd0063323c8a', '0'), ('368', '1382796726', '3809e1552cfec0040614ea12ddfde831c7a03f10', '4c71ac5f81aa2d1c97fffa52b541eb6125940302', '0'), ('369', '1382796748', '3809e1552cfec0040614ea12ddfde831c7a03f10', '48143882fcbafd362be355e62cce5cdbe247bc3f', '0'), ('370', '1382796792', '3809e1552cfec0040614ea12ddfde831c7a03f10', '5c5d916fe69f9253bf9cb7e474586242ab3442fa', '0'), ('371', '1382796884', '3809e1552cfec0040614ea12ddfde831c7a03f10', '3607de8e36f85f1910fc8c2c509d8aa6f5f9e9df', '0'), ('372', '1382796887', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'f6b4dc4b8ae423e92790c85175142d76a7fe1884', '0'), ('373', '1382796888', '3809e1552cfec0040614ea12ddfde831c7a03f10', '0ff75892cb8ffdf4b1a9650f9f83973cb9114128', '1'), ('374', '1382796888', '3809e1552cfec0040614ea12ddfde831c7a03f10', '0305c73653bac6c79a41b70ff6aa57bc0a85556f', '0'), ('375', '1382796921', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'fb7536e36f381c0b81a6bb6ecaf7960b5c347513', '0'), ('376', '1382796921', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'a4b4ed37b73bfd25c4d74c85fa2c2efb9d2fc0b7', '0'), ('377', '1382796923', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'f73d860b75b1066bb98d26de3d15a9af8c75ff4e', '0'), ('378', '1382796932', '3809e1552cfec0040614ea12ddfde831c7a03f10', '1807611e01db6c6ff5f6cdb2f96e03155e4e9022', '0'), ('379', '1382797015', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'fe1db5dd3c19e76409ae0c78a3d7d97525510f13', '1'), ('380', '1382797016', '3809e1552cfec0040614ea12ddfde831c7a03f10', '9ce8d6c69530038b9babf497be67a420d6e7fbb8', '0'), ('381', '1382797186', '3809e1552cfec0040614ea12ddfde831c7a03f10', '1a0f0bb0a51e703ec3325ad1068f195ab3f15b3a', '0'), ('382', '1382797188', '3809e1552cfec0040614ea12ddfde831c7a03f10', '39c252e9f4cbb9969e9c48845ca3a5eb125f14d1', '0'), ('383', '1382797189', '3809e1552cfec0040614ea12ddfde831c7a03f10', '305df7a66ac94007eb6dc86a485387890026f91b', '0'), ('384', '1382797223', '3809e1552cfec0040614ea12ddfde831c7a03f10', '5d8c03ff8b333e3174ffdc4355129f069bf868d8', '0'), ('385', '1382797223', '3809e1552cfec0040614ea12ddfde831c7a03f10', '97e6b2cddc77321a7e416b4c10ef66f0cd38e561', '0'), ('386', '1382797223', '3809e1552cfec0040614ea12ddfde831c7a03f10', '46b82669c061bd3d4c8cfdef3aa0505eb2fd4cd9', '0'), ('387', '1382797330', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'c27acbe7d118ddd0a45fe640ce2ffa3460115ac3', '0'), ('388', '1382797336', '3809e1552cfec0040614ea12ddfde831c7a03f10', '1f48b5d439dcdf5e74782d1f53c2f682ebc6f5be', '0'), ('389', '1382797337', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'ec996d992d218bce65ede6132b8d38bdcfe9a0fb', '0'), ('390', '1382797348', '3809e1552cfec0040614ea12ddfde831c7a03f10', '9f743fe8a589d259662b8cd0860bdf91c6319d0a', '0'), ('391', '1382797349', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'd64090ca17cfc5bc2088303279a2359a71dcbfc3', '0'), ('392', '1382797351', '3809e1552cfec0040614ea12ddfde831c7a03f10', '089bd2a6b4c9b5d069134ddfad114ac8fe6c6825', '0'), ('393', '1382797418', '3809e1552cfec0040614ea12ddfde831c7a03f10', '3ac8b1db4279aa4f03da2c80a263263fd5108027', '0'), ('394', '1382797418', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'c9df25adc35ae062c906a81dbf647be2a44005db', '0'), ('395', '1382797418', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'd2ca25ee8ef566c3af96fd26f1cd7557c1529617', '0'), ('396', '1382797418', '3809e1552cfec0040614ea12ddfde831c7a03f10', '507d6f2709f21825b8cc891b901c42582bd4ddd0', '0'), ('397', '1382797420', '3809e1552cfec0040614ea12ddfde831c7a03f10', '8491491c9d6ecf28d1c22a6730ce3c6466d96c28', '0'), ('398', '1382797426', '3809e1552cfec0040614ea12ddfde831c7a03f10', '6a10ad89f9729d8aa91f6c66af33b0d2f3fd6ea3', '0'), ('399', '1382797426', '3809e1552cfec0040614ea12ddfde831c7a03f10', '8bbb15df13894e5bd298889893f5be7d07bbe039', '0'), ('400', '1382797426', '3809e1552cfec0040614ea12ddfde831c7a03f10', '6d71fd1012203a77c8b76571e5dfdd7e47bfb824', '0'), ('401', '1382797526', '3809e1552cfec0040614ea12ddfde831c7a03f10', '7b9407e7bf879a7bf8c932793d85a54eb704d341', '0'), ('402', '1382797528', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'c31c16bcf8bd692a455837eb28c1d7ba9941e5ae', '1'), ('403', '1382797574', '3809e1552cfec0040614ea12ddfde831c7a03f10', '37e84388eadc46ca428419d44ff53da9f4637d24', '0'), ('404', '1382797574', '3809e1552cfec0040614ea12ddfde831c7a03f10', '29bed86565c33a3dbaa20d3ee1a6c8c31f24b9e5', '0'), ('405', '1382797580', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'e9411834b834db5713a6adaccfd6b303afd1f629', '0'), ('406', '1382797582', '3809e1552cfec0040614ea12ddfde831c7a03f10', '37518ac6790b47a6adabadb7986c60705f0b9095', '0'), ('407', '1382797582', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'fab0f6154ec5ba6c34525d9d20d5d2bc8d68e27d', '0'), ('408', '1382797582', '3809e1552cfec0040614ea12ddfde831c7a03f10', '35cf0e84c9e7d650ca337ef27aa1ce79a05c9df4', '0'), ('409', '1382797583', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'e176a6282a68dfe4e1797fe139c1bb53c303b24b', '0'), ('410', '1382797586', '3809e1552cfec0040614ea12ddfde831c7a03f10', '7c1200389360618013461ad8876a48e9f99f09bf', '0'), ('411', '1382797587', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'e25c2cae8bd49e6dd35a54392686c1cc45712842', '0'), ('412', '1382797587', '3809e1552cfec0040614ea12ddfde831c7a03f10', '98d15e0649295f720eca156eab5f2d813ec63e61', '0'), ('413', '1382797587', '3809e1552cfec0040614ea12ddfde831c7a03f10', '8e237d432c8f65b27e4d946c0b1fedb9553d27f1', '0'), ('414', '1382797588', '3809e1552cfec0040614ea12ddfde831c7a03f10', '4b7fcf3f90c70d05be5ae0990d10fb32bd765cf9', '0'), ('415', '1382797589', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'c7e0d4a3bb0ca5bd789676c41e3a792f9e380836', '0'), ('416', '1382797589', '3809e1552cfec0040614ea12ddfde831c7a03f10', '4e4c886e02ce508f3ae1944ec6b951a51db3dfa5', '0'), ('417', '1382797589', '3809e1552cfec0040614ea12ddfde831c7a03f10', '202646a8fab188d0c4551e453d97fd8d07750282', '0'), ('418', '1382797597', '3809e1552cfec0040614ea12ddfde831c7a03f10', '485a7634f1fa9e326cfbc5527597e056ca1b335d', '0'), ('419', '1382797599', '3809e1552cfec0040614ea12ddfde831c7a03f10', '8eaf957ec188fcfbfe00f10caeb7f5f826bba415', '0'), ('420', '1382797601', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'fbaf32cfe9770b036ed0ccabced85b33d0be3592', '1'), ('421', '1382797611', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'd0762fa714150c8f79795d31b264977228c1b1b6', '0'), ('422', '1382797611', '3809e1552cfec0040614ea12ddfde831c7a03f10', '06b1c776c74900239114257e6f79d423721f042d', '0'), ('423', '1382797614', '3809e1552cfec0040614ea12ddfde831c7a03f10', '5ad1a7a89dd9f1ac5f97cdcb04840a2e8f3d0603', '0'), ('424', '1382797614', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'ae611be32fb6d22747d8779962eb6d56bfa92795', '0'), ('425', '1382797614', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'f480fb659d91d63262370c57b386f9c8f9df7e5a', '0'), ('426', '1382797616', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'c5f6310d91188582cf9852f5524bec84aa259f14', '0'), ('427', '1382797618', '3809e1552cfec0040614ea12ddfde831c7a03f10', '454ba76017ed573b2e6583e5c22ecb8674b935b8', '0'), ('428', '1382797643', '3809e1552cfec0040614ea12ddfde831c7a03f10', '2d0033dffccc3012d4e15ddc41df7d193ed82d4e', '1'), ('429', '1382797645', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'cf09cdb65d6631072a4517e7b60186d21af21ccf', '1'), ('430', '1382797646', '3809e1552cfec0040614ea12ddfde831c7a03f10', '7d3333708ee4b5fe69ea7fc1ec17dbf40259c828', '0'), ('431', '1382797648', '3809e1552cfec0040614ea12ddfde831c7a03f10', '7bf9909b088a4f01084f1ead8623f02ab85b8e29', '0'), ('432', '1382797649', '3809e1552cfec0040614ea12ddfde831c7a03f10', '6269957daba9c45d58a07fd65d22f7f406926c1e', '0'), ('433', '1382797649', '3809e1552cfec0040614ea12ddfde831c7a03f10', '8d63772316bbc5cf5536ee5baf4248f7d73fb2d4', '0'), ('434', '1382797649', '3809e1552cfec0040614ea12ddfde831c7a03f10', '3cffd285bb0081dc2052b97b3fc77f8716578a88', '0'), ('435', '1382797650', '3809e1552cfec0040614ea12ddfde831c7a03f10', '7e4fc826fd7ddf48c3a34a6e00abd7ed2a99be2c', '0'), ('436', '1382797653', '3809e1552cfec0040614ea12ddfde831c7a03f10', '12ac81e1537c2d8b71f96f56d6282a3655c61560', '0'), ('437', '1382797653', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'f0d90288ca8aa5ecf3a6ddcf82ee6f768c410919', '0'), ('438', '1382797653', '3809e1552cfec0040614ea12ddfde831c7a03f10', '00ffd12c227614754a58a2bdd8190eced65d9d49', '0'), ('439', '1382797653', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'e83ff1c7633e4cee42f254fa4128fcde812fcccf', '0'), ('440', '1382797654', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'f40bfb33970d35e132cdc9e8cf85e97990b9c1f1', '0'), ('441', '1382797655', '3809e1552cfec0040614ea12ddfde831c7a03f10', '779311cf011dda1d976f3ea00ebf017830fec1ad', '0'), ('442', '1382797655', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'a18af2219720dc8e0c3206ae82fb6a7980343c06', '0'), ('443', '1382797655', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'a468656c6a40aeb81ce127d90b7db98c09fe053a', '0'), ('444', '1382797655', '3809e1552cfec0040614ea12ddfde831c7a03f10', '4200734daeee72dee7968fcbd8ed9ddad23cdddb', '0'), ('445', '1382797657', '3809e1552cfec0040614ea12ddfde831c7a03f10', '1115602d4bcc50511d37d747644bac05520a4f60', '1'), ('446', '1382797665', '3809e1552cfec0040614ea12ddfde831c7a03f10', '1031bb3aaf704e3727cd53300afb01ad1604bf4b', '0'), ('447', '1382797667', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'd27f0cb58c29ff9a82688e060f27ae70c8ca8e15', '0'), ('448', '1382797684', '3809e1552cfec0040614ea12ddfde831c7a03f10', '49a7e1df8ed21a2709c088e1763b4d7309c2f3d9', '0'), ('449', '1382797773', '3809e1552cfec0040614ea12ddfde831c7a03f10', '53739113e95830d8cbe3d515fe8424f8a0718edf', '0'), ('450', '1382797773', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'c98480a35e5a300f8734b2143f1a0a76f35f8224', '0'), ('451', '1382797773', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'c982444c619508bae767ef0682862805397c0123', '0'), ('452', '1382797777', '3809e1552cfec0040614ea12ddfde831c7a03f10', '03111c895fd7775aa655778c5064cc4d86ba3719', '0'), ('453', '1382797777', '3809e1552cfec0040614ea12ddfde831c7a03f10', '7b255296e435f67ddfb0d4e6446e8f32739ef09d', '0'), ('454', '1382797777', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'a182345e80e42b0e5ddbc5886e2c44265b9df60a', '0'), ('455', '1382797777', '3809e1552cfec0040614ea12ddfde831c7a03f10', '38aade04a66b1d96edb839ac99a34bd752c3ab9a', '0'), ('456', '1382797777', '3809e1552cfec0040614ea12ddfde831c7a03f10', '82314a07cc1290549364ea1265d66454b6308542', '0'), ('457', '1382797780', '3809e1552cfec0040614ea12ddfde831c7a03f10', '1df3b861e5dd667093b5c01785ee64fd3633db96', '1'), ('458', '1382797784', '3809e1552cfec0040614ea12ddfde831c7a03f10', '341e6c0a9b68f8ad75cf0009c35ef0aef1956b74', '0'), ('459', '1382797793', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'c08774c0704623f844ae7cfa38b6848e74de2c54', '0'), ('460', '1382797793', '3809e1552cfec0040614ea12ddfde831c7a03f10', '64965d181c32210c8353152c70bf9505d77c4480', '0'), ('461', '1382797799', '3809e1552cfec0040614ea12ddfde831c7a03f10', '9bf356cec286def546806550495318ef3ce60bb5', '0'), ('462', '1382797799', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'afeb87c936f5ab40db5741bd5661bffeadff936b', '0'), ('463', '1382797799', '3809e1552cfec0040614ea12ddfde831c7a03f10', '11dbf12dbb20348f6bbf623f58fa64d388f5436b', '0'), ('464', '1382797800', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'e7ef529dcb806a2c145d317ef30fe250d5314285', '0'), ('465', '1382797806', '3809e1552cfec0040614ea12ddfde831c7a03f10', '0a3bc4c14573ce4087167876a919eaffa8b92b47', '0'), ('466', '1382797806', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'a8ad681ee2fed9d977ce53d2f0edfec2344bf240', '0'), ('467', '1382797806', '3809e1552cfec0040614ea12ddfde831c7a03f10', '43c49ca7afaf0bb3df109ffc3962665cd194950e', '0'), ('468', '1382797806', '3809e1552cfec0040614ea12ddfde831c7a03f10', '1ecede87358ba80478fef25e455c2d249c743420', '0'), ('469', '1382797806', '3809e1552cfec0040614ea12ddfde831c7a03f10', '41ba215bd78ca10a0977708e7b88a43d3ae5940e', '0'), ('470', '1382797830', '3809e1552cfec0040614ea12ddfde831c7a03f10', '4f8be5b04022ee64938795a072df6fbd908dff7f', '0'), ('471', '1382797833', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'b6e362441172d456473c5458e99310087cf42b41', '0'), ('472', '1382797833', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'eb8b9f3128c2d9330bd934b822676c3057726f0f', '0'), ('473', '1382797833', '3809e1552cfec0040614ea12ddfde831c7a03f10', '1acd71c4fe32946a6d65ac7f387c3dbf7883d43c', '0'), ('474', '1382797833', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'aad14682623c02bdeeb1d27bfb8b7f41cfc1ec13', '0'), ('475', '1382797836', '3809e1552cfec0040614ea12ddfde831c7a03f10', '0197abee8fce991f76f59e7b7df6542a260e87c6', '1'), ('476', '1382797840', '3809e1552cfec0040614ea12ddfde831c7a03f10', '78af075ce0b086c6e430f636278667303faa8f54', '0'), ('477', '1382797887', '3809e1552cfec0040614ea12ddfde831c7a03f10', '394872a86f4cb853b7cf3f254ee332c3ae5e2b92', '0'), ('478', '1382797887', '3809e1552cfec0040614ea12ddfde831c7a03f10', '5cea07e47904a145ddd5a918aa856a4a2620e3e0', '0'), ('479', '1382797887', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'f5cd94bfc7a37ab8156e893ceaffa0fd9b8d86da', '0'), ('480', '1382797891', '3809e1552cfec0040614ea12ddfde831c7a03f10', '0f12ac8cb7daab6e78c3c822f8333cd005ec44a7', '0'), ('481', '1382797895', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'e7ac5b03f8ef35e7e717d4c04605bb09bea9a34a', '1'), ('482', '1382797898', '3809e1552cfec0040614ea12ddfde831c7a03f10', '9a9e379e5a262c42575bd72afa426d1faa04f511', '0'), ('483', '1382797970', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'ca9ef9f0fe9246eae9afc021eb6c317201e0401a', '0'), ('484', '1382797972', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'a5c4e6a2b6a83ae9e300646a270c6b62be0e5a11', '0'), ('485', '1382797972', '3809e1552cfec0040614ea12ddfde831c7a03f10', '29661154d6819deb3f6446485716221fe8d9efca', '0'), ('486', '1382797973', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'fc45474eb125d235cda942b027108dd22bdb65e6', '0'), ('487', '1382797973', '3809e1552cfec0040614ea12ddfde831c7a03f10', '33b2614ed3dd20606bbf3356a1be41d8392f8660', '0'), ('488', '1382797973', '3809e1552cfec0040614ea12ddfde831c7a03f10', '52330400aa1794bdd6b0de485c3e9f156ef4330f', '0'), ('489', '1382797974', '3809e1552cfec0040614ea12ddfde831c7a03f10', '41c4454fe54f8b8fe7bd0db856d26b0ecf0334ab', '0'), ('490', '1382797978', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'e4123b783fec3fa4a4d47ecdd0669271388906e4', '0'), ('491', '1382797979', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'd642f7676e932cfa8216ca3062ac98cebef23f75', '0'), ('492', '1382797979', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'ff8b1bdf5ee6f18ecfb979078eca400abb2b2f3e', '0'), ('493', '1382797979', '3809e1552cfec0040614ea12ddfde831c7a03f10', '638a9d3d2a4fde220c4ef480646ab0839c5f6cba', '0'), ('494', '1382797979', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'b60658f23b84fb9f192aa3efea2d73af10fd6b7f', '0'), ('495', '1382797980', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'd991e62106fa1f90c57471887326e3617b2828ac', '1'), ('496', '1382797982', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'e602641eb34b87f2db0960f4de2a207c36edca4b', '0'), ('497', '1382797982', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'bd16d91fd644a137e9ecd9f59610b17c31560a1c', '0'), ('498', '1382797985', '3809e1552cfec0040614ea12ddfde831c7a03f10', '88fcc246d1620a4e1c0d1d1954112c77d83e011a', '0'), ('499', '1382797986', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'a634bbdd2b6967a9d45f6860f6f2c35d2fb2b84a', '0'), ('500', '1382798003', '3809e1552cfec0040614ea12ddfde831c7a03f10', '8d39d1548788159ea5acb203462f8c3d0b3c6dbc', '0'), ('501', '1382798021', '3809e1552cfec0040614ea12ddfde831c7a03f10', '30342fcf89c35aaeae1c4138d3d7dafb604370df', '1'), ('502', '1382798022', '3809e1552cfec0040614ea12ddfde831c7a03f10', '83ea3435f2421585d662257eefc6e9b80c2dfebf', '0'), ('503', '1382798022', '3809e1552cfec0040614ea12ddfde831c7a03f10', '445536b689ec3489b2017a10cae9351439c90a00', '0'), ('504', '1382798022', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'ffe45583544944286c24827fea8fa0fa0f6c1899', '0'), ('505', '1382798096', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'efb8a2a5aafa71d5740dd427cab0b2e86f44d36e', '0'), ('506', '1382798170', '3809e1552cfec0040614ea12ddfde831c7a03f10', '42c2e619778de09bea616d5ba36a96b1a7de759d', '0'), ('507', '1382798170', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'c67559b13f4ba67ffaa7e706c27ec20454735b84', '0'), ('508', '1382798171', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'acbdb786f2a1f8c4bde785a405ae3e7bf58a1125', '0'), ('509', '1382798173', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'b26718c02670ff3ff2768d1655b2dd36881fab71', '0'), ('510', '1382798399', '3809e1552cfec0040614ea12ddfde831c7a03f10', '516d5e5ab0532ba6f11f8f8d03e0a8210586579c', '0'), ('511', '1382798694', '3809e1552cfec0040614ea12ddfde831c7a03f10', '56a7a32295fcf4ba836b96297910282add12636b', '0'), ('512', '1382798698', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'b90cb8d0cc5e1fb87114f82f835e80379c9a0beb', '0'), ('513', '1382798786', '3809e1552cfec0040614ea12ddfde831c7a03f10', '5320c316e71ee0f0a4257dac8ece717988bce185', '0'), ('514', '1382798794', '3809e1552cfec0040614ea12ddfde831c7a03f10', '6747709fcb161b94666cf6312120ea24e8006404', '0'), ('515', '1382798901', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'f17ca78b28aa031f33079d536feee8ab3d58adda', '0'), ('516', '1382798932', '3809e1552cfec0040614ea12ddfde831c7a03f10', '18f03d1914a903cbcbfc6cb5cd9630b5af18dd8b', '0'), ('517', '1382798970', '3809e1552cfec0040614ea12ddfde831c7a03f10', '33bd09fa32a04cbaceae512f92f1e76ae892df9c', '0'), ('518', '1382799021', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'acfc07f7a5b19683a1f5e711abfe8a362519cdad', '0'), ('519', '1382799033', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'c499e4391106451c87c0f03d3dc4d93ec3f254e6', '0'), ('520', '1382799047', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'e18ff7dd3a68e7352d3529aabdc391a4fff2e3bc', '0'), ('521', '1382799093', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'b02310f25371e00eca89a584d695515654739f93', '0'), ('522', '1382799112', '3809e1552cfec0040614ea12ddfde831c7a03f10', '76e13c9da46364c1620aae161b4d283f9c1c87b7', '0'), ('523', '1382799140', '3809e1552cfec0040614ea12ddfde831c7a03f10', '948d93cc71bf128fd7fa4dd2238abcd522d289b1', '0'), ('524', '1382799154', '3809e1552cfec0040614ea12ddfde831c7a03f10', 'ce33b0d42bf3694824415975dbd3a843cf3a3e4f', '0'), ('525', '1382799167', '3809e1552cfec0040614ea12ddfde831c7a03f10', '16d52b022e3dcc2135acf60f52dbc3612eb2dac7', '0'), ('526', '1382799279', '4658b4ce4445433647d91811cfea09aa9cb641d5', 'ff7374ebeeec922aa5c89f7b8c2c2656411657aa', '0'), ('527', '1382799280', '4658b4ce4445433647d91811cfea09aa9cb641d5', '982093d0847883bde7ac3bf1f4cd9dbb08cd8f23', '0'), ('528', '1382799281', '4658b4ce4445433647d91811cfea09aa9cb641d5', 'd761070d952ed8c62a17322655494e00376ff702', '0'), ('529', '1382799281', '4658b4ce4445433647d91811cfea09aa9cb641d5', 'bd1cbc9149cde9390319b8b8009af2526f237036', '0'), ('530', '1382799346', '4658b4ce4445433647d91811cfea09aa9cb641d5', '87f1232969ad6c76b2df3275f87f0b9b9d17422b', '0'), ('531', '1382799346', '4658b4ce4445433647d91811cfea09aa9cb641d5', '3ecbf16b386d1a8bf82d32ce572cc7d49a469f02', '0'), ('532', '1382799346', '4658b4ce4445433647d91811cfea09aa9cb641d5', '41546a7b23e17fe89514c3fe6b7d12e50916fce6', '0'), ('533', '1382799346', '4658b4ce4445433647d91811cfea09aa9cb641d5', 'b013f9943c1da7b890e20e9e105e8572503a8cbf', '0'), ('534', '1382799346', '4658b4ce4445433647d91811cfea09aa9cb641d5', 'a3e2a85433b43e92811ce1cf3e57713b0c2db8b0', '1'), ('535', '1382799350', '4658b4ce4445433647d91811cfea09aa9cb641d5', 'fb6c1221161351669c8e1efcb92f2719dd306169', '1'), ('536', '1382799354', '4658b4ce4445433647d91811cfea09aa9cb641d5', 'e9bdcee17a0f3a4532676c6f16f04ac1e2f18535', '0'), ('537', '1382799354', '4658b4ce4445433647d91811cfea09aa9cb641d5', '3b8fab0385a9cf0344d89cb63983e6ea7c2b4b8a', '0'), ('538', '1382799357', '4658b4ce4445433647d91811cfea09aa9cb641d5', 'f43990ad6df99600bb1f9d43c5827681ce1da663', '0'), ('539', '1382799444', '4658b4ce4445433647d91811cfea09aa9cb641d5', '6c7c938076422bc7a921c579e4ac00c2ed2af243', '0'), ('540', '1382799534', '4658b4ce4445433647d91811cfea09aa9cb641d5', '61c73f9cdedc6d8299722e62d3f8defd3764cfee', '0'), ('541', '1382799552', '4658b4ce4445433647d91811cfea09aa9cb641d5', '03fed8565b58df30eff1f536132d733aeb52148f', '0'), ('542', '1382799658', '4658b4ce4445433647d91811cfea09aa9cb641d5', '2dd203ce65c3cbd8c88630d41c01a396293f9100', '0'), ('543', '1382799770', '4658b4ce4445433647d91811cfea09aa9cb641d5', 'd57ebb3680a67fb00afeeed683457c62c2d8acf8', '0'), ('544', '1382799777', '4658b4ce4445433647d91811cfea09aa9cb641d5', '3781b94bb4295559cef3f4bf3e31443cf6961515', '0'), ('545', '1382799834', '4658b4ce4445433647d91811cfea09aa9cb641d5', 'd5d0a30f61eda8f52a80227b8d400d521bdeda31', '0'), ('546', '1382865477', '0', '6786f9f9e9f8c10ec3cc798ce4d67a20f78c5270', '0'), ('547', '1382865477', '0', '625d948ec9736503820461b7cf45922ef1ddc967', '0'), ('548', '1382865489', '0', '70cdc1d1b9701b11e783a9647844074ce1cdfd44', '0'), ('549', '1382865489', '0', '3d7444c5fc9d463906fc6871401aec36381d92e7', '0'), ('550', '1382883116', '0', '6d59ce17a2eb0aeb7aabf00631578103ffe86028', '0'), ('551', '1382883116', '0', '0ac05358ab1b222f41f58b30f32692238e1686d8', '1'), ('552', '1382883298', '0', '9416b4fd77de2951d019074935e4a8c983cf5ce5', '0'), ('553', '1382883298', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '72522ec55b453adc56986d72a45ca55f65cd0411', '0'), ('554', '1382883303', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'b97d274889a323ac11e92e6418ce8d7966253685', '0'), ('555', '1382883377', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'c265476deab6e9d0089ac9724bb883fbe1a8d238', '0'), ('556', '1382883379', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '7b4779f71778a7872013ec01317e1e68901a6c2f', '1'), ('557', '1382883390', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '334fa96f87e2e49d698b220014a2eec98876a585', '0'), ('558', '1382883390', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '39feaefe888b0d196b649f87ac3f32ca41ed51e0', '0'), ('559', '1382883396', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'becf360486c7a36f307565e4e3ce159c29b19b96', '0'), ('560', '1382883399', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'c4db55127979c0bf48186bebc0ba19ca42383d8a', '1'), ('561', '1382883405', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '75a218ce9150d8f03fae2f987c04ebf7d5dee537', '0'), ('562', '1382883405', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '81542318f05837991d46fd4e6eeee17211e03786', '0'), ('563', '1382883407', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '69da10558fb32a797e85c4acb0f17b9f29110a89', '0'), ('564', '1382883525', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'a4b64b32c98d302bccd7d100088611b330bcd209', '0'), ('565', '1382883526', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '2834454c21eb7f9a8e73b68ebe40b30a462e3948', '0'), ('566', '1382883526', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'cb371cab68ce1d57873f1990e2cd04c392e454e9', '0'), ('567', '1382883526', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '6a3cbbdef11a7bb42abe8a62cd0a8b85663951ad', '0'), ('568', '1382883802', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '02d2b7c77ea9d773bd9a9089dffc7046e0780bb0', '0'), ('569', '1382883808', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '28f1edf1a03a4451a3ee9bbd8118b9a6fbc6c303', '0'), ('570', '1382883955', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '259ab3df1d175e31f298069c7116c98c64ebc72e', '1'), ('571', '1382884558', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'a70d2b6b9e74fec29c935c3a80466566e89d2f72', '0'), ('572', '1382884559', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'c25264c5ff4aea2e29e28ab0345fd9fadef62584', '0'), ('573', '1382884570', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '5d1b069443cee5e4da3b5afa3a56cfe6d0f41e72', '1'), ('574', '1382884651', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '522532a337323542f3e6df6b04c2ff3cffb57bd8', '0'), ('575', '1382884651', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'e4c32d4f5b7ce92b933a2bfb4e8755f98fc45746', '0'), ('576', '1382885201', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'c19fc56fafa84ee4462c5c57117c7b08a361c427', '0'), ('577', '1382885204', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '35aca80d0d4d9dbb16cda368eb7842775a922d92', '1');
INSERT INTO `exp_security_hashes` VALUES ('578', '1382885205', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'c876cf099f7018e942397975abd78d2ba7fe7e3b', '0'), ('579', '1382885206', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'ef9b7740a1457b7a2f34ca1f386ee2abaec31847', '0'), ('580', '1382885206', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '2125add0c9925164da395cfa4058541cefb62c68', '0'), ('581', '1382885229', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '66e9b73c2d60984cad423332f718e50a0ff560fc', '0'), ('582', '1382885234', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'c9c42335c6fb2b2c4d7df78c9fa75a5c4cec2f7b', '0'), ('583', '1382885235', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'c214bf50eca0eb2a1b27645fd8bd5b8fe0012c47', '0'), ('584', '1382885235', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'cdc9ec18d395ec0c96e382270eac0de8b21c8e82', '0'), ('585', '1382885235', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '7fec282a2a48c68365a03facc7ccf890fdf201b3', '0'), ('586', '1382885235', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '629a13307cbcc74ebc128577aa116f17ab5acc05', '0'), ('587', '1382885235', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '57c01760116405d3a6c557da39e4cd6a3dbbf7d4', '0'), ('588', '1382885247', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'cee120a05b1d6d833dc22a305ea006f4fbf75d22', '0'), ('589', '1382885249', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '596cced75f68bfe6fda135bbf0361e6074745ac6', '0'), ('590', '1382885249', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '7fd263e8fdf181648e6edf4538f77adf0681cb86', '0'), ('591', '1382885249', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'c07a1b73131efb30c88a698889c21448ab72939f', '0'), ('592', '1382885250', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'ef44180a839968d498a7b33bcb340ece374bccf0', '1'), ('593', '1382885252', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '64f8a36ac3667430e36e5de7d4709d1fd4ef5285', '1'), ('594', '1382885252', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'bd0a06db8f74f3693ee4dda98e25ea14969b6598', '0'), ('595', '1382885253', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '0d97f4c0f76eec0d7cc36b162e47a606584cda30', '0'), ('596', '1382885253', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'ec06bb12de8ceac93a1f42e0a6b8378d5465b55a', '0'), ('597', '1382885261', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'e0d5202fdbb697d8360beba5b11220d25782ed90', '0'), ('598', '1382885262', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'b968463e7f1d15af6ded4c5e8ddc237dbdcb0bd3', '0'), ('599', '1382885265', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '7b5b792ad3ee92e0e72c84ea48ba8c21cc3fb7a5', '0'), ('600', '1382885300', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'dcf6542e6665e0a896fecf7a5a410efef5342c64', '0'), ('601', '1382885300', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '46060cf296256f53aed7d237f8b98d65ee0c60d0', '0'), ('602', '1382885300', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'e106567e64c737cb4060e47e42b93a0e19463e5d', '0'), ('603', '1382885301', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'ed18dd804e299e2adf99891133a227fe85715698', '0'), ('604', '1382885305', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'a5cdc78ad72dbd0df3117319b8ba893fcb7b2270', '0'), ('605', '1382885306', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'f09be5e48e69c23ce351820bc86ad4bf6344ed91', '0'), ('606', '1382885343', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '1c16e908c766b8e035cd0a52347bd3ca2dec5fd1', '0'), ('607', '1382885362', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'd615f23c377b6a4ff357e007552c6f1d8ba6f1f2', '0'), ('608', '1382885365', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'cd64fa2d373cff0289ff034da3f93da97dd6f9f3', '0'), ('609', '1382885387', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '7b40d34b6834f4ab5f724466c5d8260990f63c94', '0'), ('610', '1382885453', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '512e485fde6390c70256d9c9acb94d54c7b87cc7', '0'), ('611', '1382885454', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'eb7ab53299df652b0e8c70143e37c2569d421da8', '0'), ('612', '1382885457', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'bd8cf0fb4545471461ada2631b32e5a70894bb0e', '0'), ('613', '1382885467', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '34cee4d08e3ce7baad31229cd70ee1ebcd56a648', '0'), ('614', '1382885481', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'f7e8ef794c89173f7fd5b1dfdc8ae330dfb134cf', '0'), ('615', '1382885483', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '136abf1209c4dd3b4e53bdb35eb94ee4d6a560c6', '0'), ('616', '1382885484', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '14a344f2acc70eac62241c2e5b494a4b12e69bf1', '1'), ('617', '1382885485', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '018a40aa0eb4f683dad6520e8944aea045732fe3', '0'), ('618', '1382885485', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'e2c0402519a6420b07e7792581b2b09c03bb92c6', '0'), ('619', '1382885485', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '7dc3ea14018ccc7ffc33af9936449ca1f6a95c6f', '0'), ('620', '1382885490', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '7811d6863ee709b14de252b22dbe029a7a3843dc', '0'), ('621', '1382885490', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'fcfe93a7c80b48fbc7bc9b96845cfd762a71a94b', '0'), ('622', '1382885493', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '851b5f14e715a6786932ba8cff2a7f6c9e7c60f0', '0'), ('623', '1382885551', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'c0e275ab6c037e442badb4be64b462b0b4976d02', '0'), ('624', '1382885563', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'b81d682c345f2420874d2ac22bbae1503f01c77d', '0'), ('625', '1382885586', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '6829a2a6fa196412fad5713e70bf3c02de497a7c', '0'), ('626', '1382885587', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '0c499768cd57909400a711c671429d04c0cf722c', '0'), ('627', '1382885590', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '6a7cabd1d401df72ae1aa7df999a0f4dda8e0eff', '1'), ('628', '1382885597', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '4c493fcb0815f60e4f17f4465708b2b11c5b56ba', '0'), ('629', '1382885597', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '869b6677f6784a9000df2d73579402c8efaa0e53', '0'), ('630', '1382885604', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '53512c29632dcc917a32500f8b71931e42a398cb', '0'), ('631', '1382885606', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '9882a86b0135b32dfb86d4d5d0bd24854786cfac', '0'), ('632', '1382885608', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'a5b03e29022a428cfe6b64a5e66ee19f81fe0b23', '0'), ('633', '1382885609', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '33231954cf3ad83441fc01fdf8ff06a53b222ee6', '0'), ('634', '1382885609', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'b78b0b6e7cac29af3a1569206f6259aabfa95b0d', '0'), ('635', '1382885609', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '4f6842d4ba963c426746c3486545e20f387be6a2', '0'), ('636', '1382885612', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '0f5bb056218bce658369a4167c615d30d9547cf3', '0'), ('637', '1382885612', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'a34211c773fa18d936ee415dd2912bc97e25a85f', '0'), ('638', '1382885612', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'e45dad5fce34bff02c897c4ca9c4ed38ac5a5889', '0'), ('639', '1382885612', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'd54f7af2d1ad26f35ad5a1e99cb6e6d861f0d0a8', '0'), ('640', '1382885612', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '88d0f5342c27e199b7f0c7c4ed99e205d469e0a0', '0'), ('641', '1382885614', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'b437b9346238eeecebd2ad93f4fd4e0d50525879', '1'), ('642', '1382885615', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '31f1e91d0770043e820be96abc24c3128a316662', '0'), ('643', '1382885615', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '85464db57de7657b58404ca6a114d4767c3b5924', '0'), ('644', '1382885617', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '915de660ce5dab72fb04f918eec66c911ac732a8', '0'), ('645', '1382885633', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '83b83b54efdd5422f26894b57389a9d0f7c7f9d9', '0'), ('646', '1382885634', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '4857161aa057cb7472f3c7684615ec7643b9312e', '0'), ('647', '1382885641', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '31566fb3d58a0bcbbc6f0c56661cb0472db973bc', '0'), ('648', '1382885673', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '82eb3879c1a5b6a291c0928554a39ac82dfb8014', '0'), ('649', '1382885692', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '1b74a21731600be02d99c4b96e9a3f6408f0cbb0', '0'), ('650', '1382885694', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'bdabb3aed1a354635ed770541d347c799a52cf54', '0'), ('651', '1382885695', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '613e25962f93e0965253745047abebcc8e58c33f', '0'), ('652', '1382885696', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '1deb3f19dfdb66057f456ac20e87be4df8db9ed9', '0'), ('653', '1382885696', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'e137937db97b658bc47c0165ee6a5ec057cc6839', '0'), ('654', '1382885696', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'e02f5a310f33261092b4b36d0b25da7c3fcbf5bb', '0'), ('655', '1382885700', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'd2d2a7b11c49c0a69e189d0c92976641addd78f7', '0'), ('656', '1382885700', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '9e85fe2a9959e9a702e9cf46b31190c3f733eb52', '0'), ('657', '1382885700', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'fde3c631154a6fd39987e86c0f9b06bc5ddce35e', '0'), ('658', '1382885700', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '012089e4c49a67d3c2c7fb0dfc7d8b8271933781', '0'), ('659', '1382885700', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '55442344ba9ddb36b4e406762f99d5a683765da9', '0'), ('660', '1382885701', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'a3b93e0de2b807d1803b22854702f4c5b7730979', '1'), ('661', '1382885702', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '939957ba37d7b0cac65e7059097e6d8583af0da8', '0'), ('662', '1382885702', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '456d6699419376b88ad30f3c106c70c788c2f5fa', '0'), ('663', '1382885704', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '0d62975e51aae99f01f9ddc9dccae844d04699b0', '0'), ('664', '1382885748', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'a6fd5d00945feeaf93dd37eb369e6006643296f3', '0'), ('665', '1382885749', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '31d06157286264d4ba734c436cbdac2699869f96', '0'), ('666', '1382885750', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '2e6d75d4cb8caa24842d1c2aa381972325d944d8', '0'), ('667', '1382885750', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '829fd829cad59fad337addffc60dc656a0048970', '0'), ('668', '1382885750', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '4461f9e32c8fdb8988d9a5b9486d15d030585d01', '0'), ('669', '1382885750', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'a2f868e6e974f26aef81c92955ae43b5440f4b14', '0'), ('670', '1382885752', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '126b38af300165fc1c5398312f9e7f99053f9b17', '0'), ('671', '1382885753', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '6d74d90de80f3ff3124d8e9bfb308249496e4afb', '0'), ('672', '1382885755', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '3499c4dac0d0a9c11617527759d7439b3d19b552', '0'), ('673', '1382885757', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'bd2acfe9f70d6a881bf99c5608f85558df7c552d', '0'), ('674', '1382885757', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '1be75fcc6af5f280c963a7e8be7213d98552b8c5', '0'), ('675', '1382885757', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '6c8b190c88c2db00d79796009b8234c0b52df80c', '0'), ('676', '1382885757', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '16d6a92bcdeef982dd1a411a0d7049db0604a35c', '0'), ('677', '1382885757', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '52eb6fb789bf0b51ad1f0459eadbf4bf5b20b89a', '0'), ('678', '1382885759', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '64de3ce94e965ffee85d537251804d6ea1e57b62', '0'), ('679', '1382885759', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '632fddbc06ba9a841a0cea6ef7c385345afaffae', '0'), ('680', '1382885759', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '06c75457d9cd267a850fc769666c501047998e50', '0'), ('681', '1382885759', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '7214f329be2dc0a22f7ca2b6f23dad4c29d8da83', '0'), ('682', '1382885759', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '904c696eef4133029ea6ac326c03a0f53d333dca', '0'), ('683', '1382885762', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'e8ea81d67abf24c57d4e15a465c74270bf212b14', '0'), ('684', '1382885765', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'cdb1345beb33223989984405403fd7ccad978ec5', '0'), ('685', '1382885780', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'fa2ce40fbcba9cb52cfb6ad11fe1f9667eb6bfca', '0'), ('686', '1382885806', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '727cb9b329ae454ff69116dc15594a31f46dc181', '0'), ('687', '1382885812', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '5ad24311aa81ce2dbb05d88f87f7d447038936b9', '0'), ('688', '1382885812', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '182739b3bd0f0a76470131af869415a3e1b4b292', '0'), ('689', '1382885816', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'f38b7ae7536e5dfd6aa1dd7218195601acce82eb', '0'), ('690', '1382885820', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '0050b0369cd3899219a416ad9667bdb38b595d2f', '0'), ('691', '1382885859', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'c28cea1cff32343c1e8259b6de28dfa27bb29175', '0'), ('692', '1382885891', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'ffc9aa18b0fd152921d07248b1bdad4139cabd17', '0'), ('693', '1382885894', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '8b3dd63b489e1bd1c879ef18c471a4ea26838a31', '1'), ('694', '1382885895', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '8eaeb5c04710f3d4c6d489b800543e04de2a78eb', '0'), ('695', '1382885896', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'e61eae491ce42d12941894f95473479e24428e06', '0'), ('696', '1382885898', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'cfbce6ccc38ea56959edb02cbf36082ec818fb1e', '1'), ('697', '1382885917', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'c9d8ed062ef73ce2f29b81dd3e84603fbd5ea0ef', '0'), ('698', '1382885918', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '7a55704ae39ae5074545fb569f5fc7b03a130cd9', '0'), ('699', '1382885921', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '28daf76b0d0bcc552021715be1b741910291d717', '0'), ('700', '1382885935', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '3af03740d909e7a84832db792cb1349caf86d053', '0'), ('701', '1382885944', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '554cb619ebfb0dd20ebe85c4e5f040c142c4c1ac', '0'), ('702', '1382885946', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '9c64c729bc67c518afebe2f1328f0d70d20e08e0', '0'), ('703', '1382885948', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'a973afb1742a78e0f3c9876de3372ec369af6f61', '0'), ('704', '1382885948', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '658ea9db06bf4da65fb57433f851b2fa9ac97663', '0'), ('705', '1382885948', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '3ddeda2b4190626f52a77ab6a5a4c61b434c6321', '0'), ('706', '1382885948', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'c6471384c14e4207c75de3f784aeb5cf288e6533', '0'), ('707', '1382885949', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '683374eb949997bb5202f3e132586a77a32cdfb5', '0'), ('708', '1382885949', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '7332d178ab91bf3fe9b7d62f9f34937a7657b56b', '0'), ('709', '1382885949', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'f82494caf90371e72d79c6ba55f888ddb53f1ac9', '0'), ('710', '1382885949', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '36936c2fccb040d721e33c2132854fd79efffea4', '0'), ('711', '1382885949', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'cfa2e2b19da96ac6429eadfe9a790341ac64331a', '0'), ('712', '1382885953', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '0e1d2ca9a19bd465507e004d754de9e6a7ece389', '1'), ('713', '1382885955', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '7a52739fe08c056e5e9f5af70cd802d163620065', '0'), ('714', '1382885955', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'ccfd1e38163ae7e9d535bda0df2e91458241a5b9', '0'), ('715', '1382885956', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '5ee1d91e630a1e52cf420533a407883220c800c5', '0'), ('716', '1382885971', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'dffd27bd084fbb89028b874b2cb440f3bf7e4b89', '0'), ('717', '1382885973', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '1bb44aed962fcbf3f95736fff848f3abc5350e42', '0'), ('718', '1382885974', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'c909eeff60d8cb44bb7e66e0d865b989d38be63c', '0'), ('719', '1382885984', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'a3b95018c2d84e00673990f925f47712ce20fbbe', '0'), ('720', '1382886040', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'cbc32ddea5b7dad55091cd37ab61d339a62e12e2', '0'), ('721', '1382886045', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '174c060733b970e19ab4a5e759c842683932107d', '0'), ('722', '1382886045', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '1a86dc892ee9adac3e0d4272db8e149fa3ab1ec9', '0'), ('723', '1382886045', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '21ea50926bf307c7a3a9555a96ab4c6aea454529', '0'), ('724', '1382886046', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '7e3bb236350dcd5082e898fb32b74ed2678d7e7a', '0'), ('725', '1382886048', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '54ca8534b4fc74ea645b40deb1da7c6103734b1c', '0'), ('726', '1382886048', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'aa6c5072d6f05ea2d23f9fd4a42fc7c9089ab48d', '0'), ('727', '1382886048', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '9e628383c87fd27add36ce72b1fa73edf2f3ca49', '0'), ('728', '1382886048', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '58c7c8ab81e2974a7fd9f7933a1f5a33a203a2bb', '0'), ('729', '1382886048', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'a6ae9766183f0263118aa41c5f24dc954131b431', '0'), ('730', '1382886051', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '363e5f1f71fc2c920a6cfd7ff0af792d5db28c85', '1'), ('731', '1382886052', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '23e260eafb102550f44f84ec1b8044b8e748c0db', '0'), ('732', '1382886052', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'da6cbae6afc52740fc1a05c5cfdfece638291273', '0'), ('733', '1382886076', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '4c4513320d59924d4533352d195c73d7d56d23fa', '0'), ('734', '1382886112', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '030dfc16c668c9afdec36fcb2dc82e9d239cf799', '0'), ('735', '1382886151', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'ea0fdb8a9321855c7ed7dc5ca3ba0d651e5b3e2d', '0'), ('736', '1382886183', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '673ac770d0889f00d0945bd944d0639bbc71983b', '0'), ('737', '1382886183', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '0b2874bd905ed968cdd5cdf9310e2dabcada35db', '0'), ('738', '1382886183', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '74d222502149c2e7778e6998b76b2610a545078d', '0'), ('739', '1382886184', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'b394320f5ca7cc990c8dc93806849cd05d08e275', '0'), ('740', '1382886185', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'cb95389c956ebebf8c39e3fbf207442be10dc722', '0'), ('741', '1382886187', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'f6a5ed0e4ebd9f41a0c4025750c51576e3d588f7', '0'), ('742', '1382886202', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'becbdb0905dac30010f64a636d9557c4e499e0a8', '0'), ('743', '1382886251', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '60879ed89d3566e3ff69054e21bd4d4bdd286459', '0'), ('744', '1382886264', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '29a2faae555cee0b36c9d909596d6968d10e2d53', '0'), ('745', '1382886264', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '0a5e0490298a1d387c2faf015adabde0dc5bb63c', '0'), ('746', '1382886264', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'f3fec743a50ca70e9543bab5f20d94b14813a6ac', '0'), ('747', '1382886264', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '31a9664647072dec6981bbf2e01c64906293a2d4', '0'), ('748', '1382886265', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'f00404d27b4a808710014f9c0712bb8e0d72c70f', '0'), ('749', '1382886265', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'f970ef1ba362cefd1ccdfa99a8438f91c9ee5e03', '0'), ('750', '1382886265', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '1937a905f945d6365b3ddebf07e5dbcc9a55bdc0', '0'), ('751', '1382886265', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '192e309b341ed5697014cc0eb1b5d21c7dcf9c82', '0'), ('752', '1382886265', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'db54873db5e4466166b91b5e6dcaf9db64ed3bbb', '0'), ('753', '1382886265', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '4638e8bf502a6425ffb6e9d6f6ee443fc2e696fe', '0'), ('754', '1382886278', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '5243bdfa32a383d9651ee91e9c02005459d674d6', '0'), ('755', '1382886352', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'a9699536af269714cad7a02fa2de945b154cefb4', '0'), ('756', '1382886410', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '3b851efc14f3f80601e276754c856557c1213969', '0'), ('757', '1382886419', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'ab761a2314421f71c53ea85a8fe3aa09a5abfd2f', '0'), ('758', '1382886421', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'bfcc2d3b80b23a9028f224cb25cdb9480d8c7897', '0'), ('759', '1382886428', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '29fedca996a309c23fef8afa446e0b5a2148fa82', '1'), ('760', '1382886430', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'f34f8cabc764e8d0d5faef28918bf099e26f635b', '0'), ('761', '1382886430', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '2d0a48b9d06b6a98b89eb0dce6ad2715d451c652', '0'), ('762', '1382886432', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'c937af626d98dc813ef6fbfd1b493cecc6c2e8e3', '1'), ('763', '1382886452', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'fac211a849c42473ad0f884ca22ec9371bcadcc0', '0'), ('764', '1382886452', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'e751bb8a9357d26d5927ef247c8914e4a53675a4', '0'), ('765', '1382886462', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'cef7f07d1340ed9380e3d8c0d78b97f8549e22ba', '0'), ('766', '1382886490', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '567ce2175b4e9dc27da4d562b259118ac4457764', '0'), ('767', '1382886545', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '96c0e31a8eeac8834a45296ad3b724f5724723c9', '0'), ('768', '1382886547', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '08859ca20443059c4e142be5b990bcfd41d95b82', '0'), ('769', '1382886549', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'f8a906443c7956a0315154a25802b11231e0fd3d', '0'), ('770', '1382886549', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '182950b0a763acad642b2348fff901b25ca5867c', '0'), ('771', '1382886550', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '641631d29bdb1ddc5624ec5429834f3bef3ef676', '0'), ('772', '1382886550', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '41a486ea4339e6c9000729bffd274caf714e447a', '0'), ('773', '1382886551', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'e11214ad474f9dad7bc73a3c4bfa2829f354f07f', '0'), ('774', '1382886551', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '4530e6d5848a3832f704b0b9eb7eecf266e8c984', '0'), ('775', '1382886551', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '9968add0a3b9a4adc7870d6bd0d09c83afaf2568', '0'), ('776', '1382886551', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'd748cb95c626dc9c49c82206d39abcfa753675b1', '0'), ('777', '1382886551', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'cf2c7ba8a57f50b5c18e0a128f7895dd50da8864', '0'), ('778', '1382886554', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'f370f9c9bea7b36e30ace10d2e063c0f4e0a9e0c', '1'), ('779', '1382886556', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'c4e51c8796615757f1a7f71f24d81949cb2411d7', '0'), ('780', '1382886556', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '3fdbb6d65096bf64bbd9ed7c0f5e73deb13601cd', '0'), ('781', '1382886559', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'd41e3ccba1a6100c126065a165f32f2ca7040105', '0'), ('782', '1382886582', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'deb68259759424008323f9b1f25b41b8f82e73ec', '0'), ('783', '1382886584', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '58bf88904b0c7dd2d25e88493cee4ccd336c95bc', '0'), ('784', '1382886606', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '330db7c90dbaf999a2d27c94b09d69516af64b54', '0'), ('785', '1382886621', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '651316257d32db0d803a54c4e464e9222ea26ec5', '1'), ('786', '1382886622', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'a3b9a4bf3ef978b195bcd86442d79cd8f431ada8', '0'), ('787', '1382886623', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '84962353b2384362d04f01354b54782a026447a2', '0'), ('788', '1382886625', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'e8830fd9a1003039466d4649b1dc5aad849795a7', '1'), ('789', '1382886636', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'e105f4fd84f39c225acf6a78a350190e2ab06987', '0'), ('790', '1382886636', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'e44018e252ffd1ac4968f2394a661dc4bd25b63c', '0'), ('791', '1382886646', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '10376110c4fd64df455e8042a8e713ca0e835562', '0'), ('792', '1382886654', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '2a9a60eb90ccf6eda5a1bed3558cb1e21136215d', '0'), ('793', '1382886657', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'b022c66a1e8915e082cf704267f6a057c598b29c', '0'), ('794', '1382886658', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '3e1a8e7900882567ab6aa4326fe37b4d64f02f66', '1'), ('795', '1382886658', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'f72c1b2201e81a9d8d55f1458a40474f427ad735', '0'), ('796', '1382886659', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '51689564e4bffce0c1aed40941fceba665b43ddc', '0'), ('797', '1382886659', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '86b1a2f90ee02568ae9b35b0cf327c603d5968ec', '0'), ('798', '1382886660', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '7fd5e4e087d01e70a5fcfa0945ce00857c84312c', '1'), ('799', '1382886661', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '9ba7f3042d6eb4611a20fc026f8dcec51f4c5fea', '0'), ('800', '1382886661', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '1caaf8ba7e45f20c4964c688f9467d999e145f81', '1'), ('801', '1382886662', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '0c0b2f9537a9a394adc0fef4cf42a9a595298d33', '0'), ('802', '1382886662', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '9284a7ad1e4fc0b1cd565e00587bff6dc4c14d72', '0'), ('803', '1382886665', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'dcef16ab95ee45058b511970aba31308888f13a0', '0'), ('804', '1382886691', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', 'e0e8f64ebb4fdb0226200fa2ec972593cd80c9b4', '0'), ('805', '1382886785', '363b26dc1572ee0e47a61b4330aae33b6d2c0378', '7c34c3211336f84b690a3495265546c53ad9e205', '0'), ('806', '1382886912', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', 'b70469f47eef865c289ab8fc382bbeed2b5e1c67', '0'), ('807', '1382886914', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', 'db8b0bc1ced0378815a01445cd236c32d8a5b0b6', '0'), ('808', '1382886917', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', '570f90a686d7fb113158445b957f3275cd53b47c', '1'), ('809', '1382886924', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', 'd5d302732bee83d4afb20e01674f22b9d780085c', '0'), ('810', '1382886924', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', '6f7bb223918239e22ebfaefccb5e67d4cc2cd623', '0'), ('811', '1382886926', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', '709300e3ab42aa812e98c1d94c630f70a4bdc7ef', '0'), ('812', '1382886928', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', '12118b471ba131d542473025b1b35c812550e9e2', '0'), ('813', '1382886929', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', 'd8298aaee2b33ef455ac9dd31ff5ecab63877aa9', '0'), ('814', '1382886930', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', '719ce416fb66bcd5ed52bc50fb31b1054dcb694b', '0'), ('815', '1382886930', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', '20b6aac1fc0b35280a2598b754777c2a8db661c2', '0'), ('816', '1382886930', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', 'cd58a8ce298c233bfb1da1f27ec0edbf85718a07', '0'), ('817', '1382886932', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', '9897482d91eb3f6de142c7e4eca328e7a8ca7bbb', '0'), ('818', '1382886932', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', '96db89c3d6ce0a0f670632e95314d44101727abf', '0'), ('819', '1382886932', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', '4638945cc9fe4fbe45ebcf8a4e69164ae03e8961', '0'), ('820', '1382886932', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', '9487e3fd20daa9192f5e35888b49676302509c23', '0'), ('821', '1382886932', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', 'c27bba9b3cd8ea6e44289a33ba7be0d5d84c11a8', '0'), ('822', '1382886933', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', 'ac30737ee304f54dc30adc2ea2dd515ae260fa49', '1'), ('823', '1382886934', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', '332530aa1b53a2a708ae9737e73147477af530a6', '0'), ('824', '1382886935', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', '49cc1ee87a9ce3bb0304651c39d14a6ff9cf7eb8', '0'), ('825', '1382886946', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', '96499a469cabbe05de4d24b199dad9623ff5c46c', '0'), ('826', '1382886955', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', '238c34f0e1d4780802ca7fd51e7c84b77b211b52', '0'), ('827', '1382886989', '3ae52dfd2c90a6219e23ef92a9379dba6f7f222a', '76ece1bcf26007549631f4aaefa8882be4303d58', '0'), ('828', '1382983052', '0', '28b6b7c256adabdb4beab4d0c67ea56a96cea031', '0'), ('829', '1382983197', '0', 'cee04caf4fca634c9b3ba1c3fa50625461666365', '0'), ('830', '1382983197', '0', '94d1e6fa5168f25b37daed807d5e14bf42ff0a8f', '0'), ('831', '1382983197', '0', '1904d67ecc2ac25e95fc3cf3c600ad587e4bdfaa', '1'), ('832', '1382983200', '0', 'dd1068a3b81bfef6af5a413da60d2336c8c4ad05', '0'), ('833', '1382983200', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'a09ad959fb3ecf17b16264c711abca725ea3288f', '0'), ('834', '1382983218', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'c6240c49ebd15a7f88447946270ff103610edfcd', '0'), ('835', '1382983220', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '7832eb9f9f2b33423d51501ba6897be6c6544612', '1'), ('836', '1382983280', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'ade54ce305c54567c687992798a971c521358826', '0'), ('837', '1382983280', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '0d088266e401dd27a2aeb684ad327429b6f9a285', '0'), ('838', '1382983283', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '908a6955dcfe49027f4102277a65367116c68df7', '0'), ('839', '1382983285', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '6c271f83486ebfbe93d040d26b2c69675ee42d11', '1'), ('840', '1382983294', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'fc665b53786f78ca3cb8f1fda4935ce4353956aa', '0'), ('841', '1382983294', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '29b8258e7f23656d90aca6a1bf6ff0f4fe1ddf35', '0'), ('842', '1382983295', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'bbbc70410d327659c70a119be5f447ea618b1182', '0'), ('843', '1382983297', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '4c186db924698b1ae5ee5830dabe6896fcf43323', '1'), ('844', '1382983297', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '7aaa137084cf0ca5090a05a938ad8940a73f2583', '0'), ('845', '1382983298', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'ea07c0563bbb39558540f41e2199c46e8851a8ad', '0'), ('846', '1382983298', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'b23b5dc8b4e7ad38ac9492486a466134483a2c07', '0'), ('847', '1382983304', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'c1e37dfb5742fe55a137d66e5d9ea4cce0e7f403', '0'), ('848', '1382983304', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'd759e9993690cbf866bcaa57cf62c4a1fcc02107', '0'), ('849', '1382983306', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '4afe20ed866aa88e751291193fc6c6c570445eb6', '1'), ('850', '1382983307', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'd52b08e48dd3ce400f002e334fb16bc3e118e268', '0'), ('851', '1382983307', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'c94c5b73cc5225a01508fefdd96d2004de42d38d', '0'), ('852', '1382983307', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'fb6251c0a1f5d955e1f88b520fa6c22ad80b5214', '0'), ('853', '1382983313', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'fd66536ac1877bbf41b68ec629a6e0df2b8376a2', '0'), ('854', '1382983313', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'b9664602238d811152bf016206504d38e4781730', '0'), ('855', '1382983314', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '48735cab377d011521496c2d495fbe6ce8b71a69', '1'), ('856', '1382983314', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '002dddada8ea0289fe562c78d2cbf0ffe47f0266', '0'), ('857', '1382983314', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '62eb7a7e46d898beb7b761aadc9eca541e010b40', '0'), ('858', '1382983314', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '28790ccd55e6d002e0855a4c415260fcc03d58fb', '0'), ('859', '1382983321', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'e8ba028506f0b7fbfd15e33a9d6427198288b0ac', '0'), ('860', '1382983321', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'f1780a56a42fd808639eb5bbc95d69de8da73d47', '0'), ('861', '1382983362', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'bbf41fd6fb0ef53da247c7617789030695bffdcb', '0'), ('862', '1382983364', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '22a4b717085553688dce361b6ba4727b97e0bbc0', '1'), ('863', '1382983367', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '2f8cbbda9761b6a235248bf8af15a7c25683acf6', '0'), ('864', '1382983367', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '554071aa8fdf5df1840bf90196b3b7a454e96a85', '0'), ('865', '1382983371', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '8d858472d142eaff6605597be86e1ce74ba25e58', '0'), ('866', '1382983372', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'edddc897e28e3579d51d15324532990406bccda5', '0'), ('867', '1382983373', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '4d97ac0702fe2c5964dca5a52959174eba9126fe', '0'), ('868', '1382983373', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '9490fc0f4a1275b3a98370fd45c9b697baba6c9c', '0'), ('869', '1382983373', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'f7d15e7111e4d892ac110307a4587dd609628bb2', '0'), ('870', '1382983393', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '89753d3b30dea2b9113b9f03359ddf0050c07efd', '0'), ('871', '1382983394', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '7f14f7bf511bf6e333fb2e8c3c6e67df662357c4', '0'), ('872', '1382983396', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '90106e6b2c5e6b743f38f19b2f25c0b0b0a5a9f0', '1'), ('873', '1382983407', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '25160db8da02ac998dfe7248b7172c566c13e6cf', '0'), ('874', '1382983407', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '99803cd0b60787086b8d8896c476561b7a5dd489', '0'), ('875', '1382983414', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'e633a328c918fe84733c8dbc524c3a634126b6d8', '1'), ('876', '1382983415', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '3a84eb4c08d58557c4608cddf26b5276ba5d020d', '0'), ('877', '1382983415', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '83229d25c47c7a6208a2ec9ddac521e9cb4c35ce', '0'), ('878', '1382983524', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '54f12165109f45fa4cc499dfd383e32738e05099', '1'), ('879', '1382983594', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '6aed0f3b9a0a64eac36fca4680819b5d5e3276fe', '0'), ('880', '1382983595', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '696ecb73fab19eaacb035b02f26f2db8b1f6b640', '0'), ('881', '1382983770', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '338071f110fac01ba657c7cb6ee2e6acd6a95135', '0'), ('882', '1382983771', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '8c298bc66f2f3f627cc20c5fd9daab1d35683219', '1'), ('883', '1382983771', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'f9619e77c6d13cf3ad4e07eb3297135d80ea22fe', '0'), ('884', '1382983771', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'ec2929d4ed8fb81d4e1239d1cb9bd9054e12e31a', '0'), ('885', '1382983772', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '08bb834f8be40291b2d8908bf9ac5b149d4294b0', '0'), ('886', '1382983772', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '4096610d129e0305ffb42ff7f7b6f6cb8e44a168', '0'), ('887', '1382983862', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '78f6cf2a4b8955fc037c9178d2d2e745ba0471cd', '1'), ('888', '1382984188', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'adf3fa5046d6a7f3f84cd1df0f274c37a44d1b34', '0'), ('889', '1382984188', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '4e31831324fbd4427be997b9ab9a8a3d0c25db82', '0'), ('890', '1382984191', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'b9fc03ac518ff51f012386f79d4b33fd0748c375', '0'), ('891', '1382984192', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '35630ecb94377273ebc2a183d620311717b33c4e', '0'), ('892', '1382984195', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '3401b1889a6ef45b09e1830387857517851b5dd1', '0'), ('893', '1382984209', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '778a06083bbc5304cefb8221da3071937ced87a3', '1'), ('894', '1382984255', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'e67059373b84968a44a04f9e0edaa33fd495b542', '0'), ('895', '1382984256', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'c0c3e519258e8639fcd5ffa12969ab59914d7b15', '0'), ('896', '1382984258', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '8cfe6883e170ca448e160cbdf3fd65c752481b45', '1'), ('897', '1382984264', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '1c2a38a3b04245f1ce4c32167e175187cdffee42', '0'), ('898', '1382984264', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'bbbcd739a8f36c428e69451d2b8ac3914ea19ecb', '0'), ('899', '1382984267', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'c597b9425b0f40da2ee1fc6d3c37f8d4df619bb3', '0'), ('900', '1382984276', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '961bed7cb6399f8e7dcc5de67c65e3ea322ac39d', '1'), ('901', '1382984276', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'a6efd0f0a9eeb232f922164a2bcb8e196183f499', '0'), ('902', '1382984276', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'be2bbe9cbdfa1caccc0cfc710d471cf21caba18d', '0'), ('903', '1382984276', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '8fde174ac14c71466e8fff16cdb5a91c7e122676', '0'), ('904', '1382984334', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '722559ba6277cfd8018a0e78002b73a79ef11c94', '0'), ('905', '1382984334', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '9d2932b5d9a48024af57a60bc6da4894b617e53f', '0'), ('906', '1382984335', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'cfd00b03b18fdc67e2e6897443910b329ceb9030', '0'), ('907', '1382984365', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'f2b41d74b32432c9e9e658e37f33f8d4afa02c83', '0'), ('908', '1382984382', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'b3c7d6c36e40e22e246ef7f8b2c1969aa5b4a0f8', '0'), ('909', '1382984466', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'c175190ec548f21d589ff3fb9fdcaacae767059f', '0'), ('910', '1382984540', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'eb174a8a58612954429666c4290df272c42e0822', '0'), ('911', '1382984578', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '2315b71d0fd9cc0aefadc8e33940bf947b672c43', '0'), ('912', '1382984591', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '199fcdbd51b962bf4d0c6cf1700667fc213a92c2', '0'), ('913', '1382984610', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'e8065f258824debb24f4ea73fe615f687365f7f0', '0'), ('914', '1382984651', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'a2cc26c89d664a88c4b34b505cfa6274b87348e2', '0'), ('915', '1382984671', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '639693566ef3da6188c4816bfad619b3ea430904', '1'), ('916', '1382984672', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '45412b8a659e8dec859f80fcbc390e91d43451ea', '0'), ('917', '1382984672', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'f951d462cb9a97635336dc94f7f9fede498c0222', '0'), ('918', '1382984672', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '80e966be44dc2fb66713bf69955ece5d1f12a0aa', '0'), ('919', '1382984683', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'f936069830f10f9d6b7ee6193860c66d17b68ebd', '0'), ('920', '1382984683', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '3411bce0f2f0ac6e1fdbc212ca66f7391b35fd11', '0'), ('921', '1382984684', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '8089e526c4114d87cd44f2fb69f16fa364e1911c', '0'), ('922', '1382984691', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '396ba7d73ea48611b27c25e0e448000cd98d030e', '0'), ('923', '1382985498', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '70486576036cd031f31e7590a9fbbeb24eb3109f', '0'), ('924', '1382985499', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '076a8bc26006929a399b97a3279b71bf8a3fec8b', '0'), ('925', '1382985505', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'ec817e392eadda8b61db749a12b37e1e8bb037db', '1'), ('926', '1382985511', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'a544d014fc60a2a4036cf83c816913166b8df532', '0'), ('927', '1382985511', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '21e1bde963ba2e2694ea2721c86b5ac171ccef28', '0'), ('928', '1382985513', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '7fd96907b5d86ddef60ce302c1f8070401817212', '0'), ('929', '1382985523', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'afa8eb46685db77893eac275fbc98499665c1502', '1'), ('930', '1382985523', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'd9477ccdf034b016bd9fe1b252bbc0b59725a3f7', '0'), ('931', '1382985523', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '6dd7d1c1846c75f7d7fefc554a3d6e1a68c49d89', '0'), ('932', '1382985524', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'aa72d5cdc176af937474e8d56df87723b1da40d0', '0'), ('933', '1382985583', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'dacf9379c0dacc8f855110edc72187b9f455ad4d', '1'), ('934', '1382985587', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '35d880c197db93c036acfd06fcee4c5b2398d439', '0'), ('935', '1382985587', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '1ed023e55a6cd6e0ea31a1c56bb53fd17d0fb0fc', '0'), ('936', '1382985590', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'c8cb5d38de0a9bf58efcb1dddeba75917c28c2c0', '0'), ('937', '1382985592', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'c9e0395ba4dda1a810d46ea0be77eab50b72d633', '0'), ('938', '1382985593', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'd17c76ed39c10bf7a0fdac703dae1b73666c7b31', '1'), ('939', '1382985604', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '9754b7d643fb311883f0b407aa9724b37095803e', '0'), ('940', '1382985605', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '8835a35ca849156cf840cc7924502cf509dd717c', '0'), ('941', '1382985606', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'b818f5b86bc6df13a69d44a48253366387690051', '1'), ('942', '1382985626', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '893c76cfc028273479f7384c6971e2b472a445e7', '0'), ('943', '1382985626', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '63661a4897db9e532c4e661584f99994e5a6fc1a', '0'), ('944', '1382985628', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'd02ae4438f26c57eda889c07445e14589fcc0548', '0'), ('945', '1382985630', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '73a7f7627fc7adda81012d44b7ac6cf41046c69a', '0'), ('946', '1382985631', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'dbd4cecb8a351b06ffc7844197e8afadc0cfb8ce', '0'), ('947', '1382985632', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '510e38b1488926fc356c84a418e49201adb8ea39', '0'), ('948', '1382985632', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'd452aabd5c871d957919c87510759e32a06223fd', '0'), ('949', '1382985632', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'ce0388f7376d9b7dfd54d191a7eca3f75250d5d5', '0'), ('950', '1382985635', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'd25151dbea36f8c18876553390c86f9490837306', '0'), ('951', '1382985635', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '2857e9e251cdba0362a1b0fa43caad0f5d189aa0', '0'), ('952', '1382985635', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'bda73eadbc6960a9d1bf7d8327c152ba71a9144f', '0'), ('953', '1382985635', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'fae405b90448b7b64d81c836b64bc99f9607ba55', '0'), ('954', '1382985635', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '7fdb36c98b8b93f4f46fa17bb53800c75382c205', '0'), ('955', '1382985639', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '4fb7e77c299a28bcbdff37e383be17c9e2dd924f', '1'), ('956', '1382985647', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '8bc3e6ae3b246f3ecb30b291d978237c057cd633', '0'), ('957', '1382985647', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '4af5cde50c97e1b14215f11634d8673a580326d4', '0'), ('958', '1382985717', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '8f2af6586a601df83a0048e4c0e0402a01537f92', '0'), ('959', '1382985768', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '2a2f6f3aa0625bc4c0ab333c45c491cca83bbd3a', '0'), ('960', '1382985789', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'aba5856bb0064d9b9b824b9bc7c010c3b417587e', '0'), ('961', '1382985845', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '7e1a2fb2dcb59bce2f8ed1a242cf6f639ebaf9b5', '0'), ('962', '1382985921', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '8288c6422ff146c55b4d8dd59380b6ee7451eda3', '0'), ('963', '1382985935', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '7fa1530f5020807bfe6d9a20c62c1d2f230e6ea1', '0'), ('964', '1382985935', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '1b0fb1ab0c018b4902e4eb7d966a782a8416d99b', '0'), ('965', '1382985935', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'b18e0bc8fdb12d3d1505f987c315a239384ea74d', '0'), ('966', '1382985936', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '66edb1d6b8e08e3a518cdc5235086a2431bb6bb1', '0'), ('967', '1382985978', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '05f784c09910a886b54c2bcdbc35a4fadf93f896', '0'), ('968', '1382986009', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'd9701821a7c1a76a834b8885766d86a278b9b155', '0'), ('969', '1382986014', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '0ff7fe56d182c8a4676dcca9eea15ac32ef85038', '0'), ('970', '1382986019', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'd607eeaea3ad18b99f6787b5e03fdc3c67b0c5df', '0'), ('971', '1382986051', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '0fc2d87c475d07bdd53655a3a5523ef3a126bd7d', '0'), ('972', '1382986072', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'f254d14ea0e33be7a7ed61976e024a06d96f1599', '0'), ('973', '1382986084', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'b17267c3e63957d9c1c85a1e37e05cb689438c99', '0'), ('974', '1382986097', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'e479586457deef5072649d564637fe662341d834', '0'), ('975', '1382986102', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '6f78c27cb232865ed63005aafe19cb3574074159', '0'), ('976', '1382986104', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '8a5b525d32dac77520d24b6366e7e5c2b3e4c8bb', '0'), ('977', '1382986111', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '9830130407ba64aab3ad68bc1063c5f3339f935e', '0'), ('978', '1382986162', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'a06d47b526d0daa637306a51c6a648827ea846ec', '0'), ('979', '1382986164', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '6ea5ae75ee12a8f65eb38d09172b9fedf3369584', '0'), ('980', '1382986164', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'e327118916c6876b8d5583ff342b605bf203afb8', '0'), ('981', '1382986164', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '899cf1176753ef5f71c140790a7a262b1c6fe1b8', '0'), ('982', '1382986164', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '234fa475a806fdfaa452b72d43bcdde50bf99ed3', '0'), ('983', '1382986164', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'fcdf2691080e97e639cf38551e39cbd42574e315', '0'), ('984', '1382986167', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'a95ccd31c279e1198594107271570da03001bd55', '0'), ('985', '1382986169', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'b92172231a7ea59ad646b996fde759473ab36667', '0'), ('986', '1382986169', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'e0d6c781e68c23d584d74d3d00aaefa83b61394d', '0'), ('987', '1382986169', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'f24fd9ecb6a5e420f1eebd0379b279da1cfa45ba', '0'), ('988', '1382986169', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'ab365d850cd34c03f85d9b5e0149a733b2452ed0', '0'), ('989', '1382986169', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'f96cb20514bb5c79c385835a6625a93ba7c088d0', '0'), ('990', '1382986170', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '86897dd7e5d78ea0338a5802fb93e782aba725f8', '0'), ('991', '1382986172', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'ae84e70d2ea910d28f5241e18abc56ca598c7ea6', '0'), ('992', '1382986172', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '966f8c3cd6326279c3182aefd55343d18fd12172', '0'), ('993', '1382986173', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '8bb326f312adad21aa40a579a5f13e5a48a13399', '0'), ('994', '1382986173', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'a7182f88d5f0c34818f732dd51ae02539e41cbbe', '0'), ('995', '1382986173', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '0e81bdff0996476cc7dfac6fcf12afb5291265b4', '0'), ('996', '1382986174', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'e9ca0e0bc7747601a89c901f50a443bc3a88325e', '1'), ('997', '1382986176', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '6b6303e4d3ecdb7ce340001b8f5d881063b054b2', '0'), ('998', '1382986177', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '1653a913387a7fc7e17057065b31aa00a884ebbc', '0'), ('999', '1382986178', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '259b1fb7342f3bce1d332cbce5fa7c4218ef56c6', '0'), ('1000', '1382986178', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '42db282b88e6d731d2adce8148cbc1def437dcc4', '0'), ('1001', '1382986178', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '9855877a349a20ee40e6ae94e80903803b0ba705', '0'), ('1002', '1382986178', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'e75575ad47c966dfac5a9e7ea22a83b3d1af2ff4', '0'), ('1003', '1382986178', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'd276fec9d5b669b80070a0162120b558cdbec03e', '0'), ('1004', '1382986180', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '4c96f028e132158172c059f1acd7f88b9036a419', '0'), ('1005', '1382986182', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '6c0ca8e178ad691d580a34e152c67eb33c50e884', '0'), ('1006', '1382986183', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '8fdb4bb1a7cbddfd6b808316f5b9a90482f4ecb3', '0'), ('1007', '1382986183', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '1777bfd9c7a461cf2b189772f199a62d276b757f', '0'), ('1008', '1382986184', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'b738d722bd94cf48dd681f3c6fa0f673208e2908', '0'), ('1009', '1382986184', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'bb5c64ac8b0cb053f901dfd91a487e0f479a8fb3', '0'), ('1010', '1382986184', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'b44f49086986d5bd897073e77e8d0f6cbdbd3ecc', '0'), ('1011', '1382986184', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'dfb1f043e81c6ac3b28f9208f017135bbbd9078d', '0'), ('1012', '1382986184', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '01f9ee503568a775b5d566ca6f455793613ec0a2', '0'), ('1013', '1382986184', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '28eeeae8e3f393e29159f632da757fb343e5ce79', '0'), ('1014', '1382986184', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '162a3b8dc043c0b3a257d9928f4f29d77443411c', '0'), ('1015', '1382986190', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '52dd637758992ba090bc96847ac1b9247cc288e3', '0'), ('1016', '1382986192', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '5da804dde564231a37562b59cd7cf70d756afc0f', '0'), ('1017', '1382986200', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '3ebaac81b596f565a992c00f2c8ce7e4cfd2ffad', '0'), ('1018', '1382986200', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '83dd20d2d044ad2e61b23147f9b63153b49dbb10', '0'), ('1019', '1382986362', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'd8c270433391c23334c00e00e930843d019e0ee4', '0'), ('1020', '1382986376', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'c9bf13ec511973d2ce09736c35e56dd37c58c5cc', '0'), ('1021', '1382986386', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '426ab70beb8a72058e18fda600e97d5d34075f26', '0'), ('1022', '1382986387', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '51350d36b5136f5a96a0e37994153081f973ad1e', '0'), ('1023', '1382986441', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'a32c60940c3fc35450632e4be4e98c718caec569', '1'), ('1024', '1382986457', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'ff4c139c65b424d8e9eb920eb9d182f05ad2cc21', '0'), ('1025', '1382986457', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'b1c20c814c114ffe88ac4ee68d57083e9dde9670', '0'), ('1026', '1382986459', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'c09f69c8e244f5916c7c18ea5e1836da4776bb0a', '0'), ('1027', '1382986835', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'dab622f31534fc74001c8fdd147098e91eeadecf', '0'), ('1028', '1382986926', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', '30c7e48da64d5b8500df8cb4903bf750e679c2bd', '0'), ('1029', '1382986959', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'afd1fae38d9818083691d863bd10c30983f8b2aa', '0'), ('1030', '1382986963', 'e034f02dd1ef0dea88fe1c7fbb7c0d8d3dd06a43', 'dfcab01e55d0721509f97ff55569d067579f774d', '0'), ('1031', '1382986978', '0', 'abb57d18a14684a9c8390a1d9bc6f7403a5585ca', '0'), ('1032', '1382986978', '0', 'b847435f5324030818f9ea6504f79c9e9378647c', '0'), ('1033', '1382986978', '0', 'b2e0c15c8de74714056d9568f503c74c1f6dcebe', '1'), ('1034', '1382986983', '0', '96d412bf7148e33121fccb7a3db1d9a2e5c6e751', '0'), ('1035', '1382986983', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'd1afb5260bd2d93317311941ec5d890f604a3842', '0'), ('1036', '1382986991', 'a05afe0ad44a806877665a1f75f2e4644565b004', '226e9d621110576d497aff151dbd3aa07a281d15', '1'), ('1037', '1382986996', 'a05afe0ad44a806877665a1f75f2e4644565b004', '0335badc271ca0e807a60cce09de9fbc2c0cfd06', '0'), ('1038', '1382986997', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'd743da74e9f1805e5e8cffb1c7905fa89ed81b60', '0'), ('1039', '1382987000', 'a05afe0ad44a806877665a1f75f2e4644565b004', '2e911f75f089ab16bf5a2912c56f958235f5b160', '0'), ('1040', '1382987010', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'c25988f6a0b44221e43d275c4fc734657580d827', '0'), ('1041', '1382987011', 'a05afe0ad44a806877665a1f75f2e4644565b004', '8c398f128c2a5c3e1f1d95146a181b0cab00f885', '0'), ('1042', '1382987012', 'a05afe0ad44a806877665a1f75f2e4644565b004', '5043cdb3779902b022414d0d5e412b3d949b631b', '0'), ('1043', '1382987012', 'a05afe0ad44a806877665a1f75f2e4644565b004', '3141846f3f88677dc10b5d7ac623a504a8f6a3d8', '0'), ('1044', '1382987012', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'ab62b3c25c3ab0985e76eb41ffddb691b748ba79', '0'), ('1045', '1382987012', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'ca3a6c9c9083ec6cde0182e659e9ac46d1ab6d59', '0'), ('1046', '1382987015', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'c4d46a32d925b1e0aab623e3b012598dd32f8ca9', '0'), ('1047', '1382987016', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'e96ef0a9e0c16c936ec0bd9a03546566a2310f23', '0'), ('1048', '1382987017', 'a05afe0ad44a806877665a1f75f2e4644565b004', '6c454fd39352cc8c3ca866f44c9bf5dab1b7bbe0', '0'), ('1049', '1382987018', 'a05afe0ad44a806877665a1f75f2e4644565b004', '0c536513e098d9390cc53a25938979ee1315106b', '0'), ('1050', '1382987018', 'a05afe0ad44a806877665a1f75f2e4644565b004', '0424ccf4c5da947b20c38d48789ee8872685079e', '0'), ('1051', '1382987018', 'a05afe0ad44a806877665a1f75f2e4644565b004', '172e709ad1a8d35a1853ec7c3a47becfcb7c5c84', '0'), ('1052', '1382987018', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'cd3e78089ef8ba034bbf64d334ab5df82ae6f481', '0'), ('1053', '1382987018', 'a05afe0ad44a806877665a1f75f2e4644565b004', '8193807401ba7d4a901d95206390620b94bb54f9', '0'), ('1054', '1382987026', 'a05afe0ad44a806877665a1f75f2e4644565b004', '090bf490b0c99568f0c37041fa70ee7d5829064c', '0'), ('1055', '1382987027', 'a05afe0ad44a806877665a1f75f2e4644565b004', '7cb2be237e01ca8ddaadc68185cb5c2fb2941252', '0'), ('1056', '1382987027', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'b8cafc0ef02ba4c8d24ce66b66d923040898a722', '0'), ('1057', '1382987028', 'a05afe0ad44a806877665a1f75f2e4644565b004', '3ee088d809f0e8501520de1e6e6cd36916994586', '0'), ('1058', '1382987029', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'dcd47e38707f58825dc443502203fb8678996b2f', '0'), ('1059', '1382987030', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'af3b508a0875bd863a1787422b87d54c14c237a5', '0'), ('1060', '1382987030', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'e3388ca7a62104e310efb1f45c868ab85dad70ba', '0'), ('1061', '1382987030', 'a05afe0ad44a806877665a1f75f2e4644565b004', '057f0f884483c992d5eee31ba77bad20f7c9cb97', '0'), ('1062', '1382987031', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'cea9440bad8ed462ec8615b358d2f62b4e66bfef', '0'), ('1063', '1382987032', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'cf21146075cb9affc408d2e5dfbeeb35c8d64747', '0'), ('1064', '1382987047', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'dd058fea223e9f33c21f672053b4790941de5752', '0'), ('1065', '1382987088', 'a05afe0ad44a806877665a1f75f2e4644565b004', '5e36421274775097fab0790e395e654ffe3a3801', '0'), ('1066', '1382987089', 'a05afe0ad44a806877665a1f75f2e4644565b004', '5fa12fa36a422ebba032c7d061feb369c585449c', '0'), ('1067', '1382987090', 'a05afe0ad44a806877665a1f75f2e4644565b004', '9b784be5f16bc168c0ca81bc0a56dc1c09e302db', '0'), ('1068', '1382987092', 'a05afe0ad44a806877665a1f75f2e4644565b004', '0e7b78b0372761aea979ca3405d9c48e777d639e', '0'), ('1069', '1382987095', 'a05afe0ad44a806877665a1f75f2e4644565b004', '1d638339077332acc5bc3cf5c12c4e92aae6b41a', '0'), ('1070', '1382987099', 'a05afe0ad44a806877665a1f75f2e4644565b004', '0908ae84ed2dd078b9c0cf9fde6986e39e892099', '0'), ('1071', '1382987117', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'f75bbc3f8779d7d98d6e0753c03ca24d134c41f5', '0'), ('1072', '1382987119', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'ead5a10f051dbe2ab25806de61012ce061d810c3', '0'), ('1073', '1382987121', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'b4aa23f412cb51509de09b0cdc31bb9aee7a38fe', '0'), ('1074', '1382987121', 'a05afe0ad44a806877665a1f75f2e4644565b004', '36dee85a9f9463f16c695cd1501d2479092195a8', '0'), ('1075', '1382987121', 'a05afe0ad44a806877665a1f75f2e4644565b004', '72491b0486de0d3aa6d9ac2281e202a97c875eee', '0'), ('1076', '1382987121', 'a05afe0ad44a806877665a1f75f2e4644565b004', '20d6fc6b5b379468532136de941e3e605ed610a1', '0'), ('1077', '1382987121', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'c2e00c3819118cb2101da870399ce96aa1c65665', '0'), ('1078', '1382987121', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'e3cb3bbb8ffefb3c059bfda2408fd1fefaaea8d6', '0'), ('1079', '1382987121', 'a05afe0ad44a806877665a1f75f2e4644565b004', '4700a0c60e17c08604352c2d9420ae2c818d52c0', '0'), ('1080', '1382987125', 'a05afe0ad44a806877665a1f75f2e4644565b004', '84ea7ec8dfdbfceaa0aaa608f10eb0df3d7b2eb1', '0'), ('1081', '1382987127', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'df95d568a7793bd2883e67ab2e3a5571e284616a', '0'), ('1082', '1382987142', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'e59b3456a09d89d65bc7f664ba22f44bcd9edce1', '0'), ('1083', '1382987147', 'a05afe0ad44a806877665a1f75f2e4644565b004', '3b1d5b22b0cabd958998a01a945f4a31ca5bc3a9', '0'), ('1084', '1382987149', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'c0332341a4dcb0c86a78f0f1a970c3d6f71da38b', '0'), ('1085', '1382987174', 'a05afe0ad44a806877665a1f75f2e4644565b004', '7df81b555460d5f860c74530b531c98811d598a8', '0'), ('1086', '1382987177', 'a05afe0ad44a806877665a1f75f2e4644565b004', '4e75f399e60583a9420d90bcccd58e436ced1d30', '0'), ('1087', '1382987181', 'a05afe0ad44a806877665a1f75f2e4644565b004', '50d8c093b01042b810d14227758ca7f7ebe04433', '0'), ('1088', '1382987192', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'dc193582a80104e1d3be04886c55c59704340f72', '0'), ('1089', '1382987212', 'a05afe0ad44a806877665a1f75f2e4644565b004', '7cb5c70c3a864608b1a373c65da4872693b401a2', '0'), ('1090', '1382987213', 'a05afe0ad44a806877665a1f75f2e4644565b004', '991fb91d136b36a2b08318bc1b035eaaebc128e4', '0'), ('1091', '1382987218', 'a05afe0ad44a806877665a1f75f2e4644565b004', '8f9e65f29e39af0da6d97521f3210983292f918f', '0'), ('1092', '1382987219', 'a05afe0ad44a806877665a1f75f2e4644565b004', '12e4c584fa9d6441c93a8a77698f11f7c606b367', '0'), ('1093', '1382987220', 'a05afe0ad44a806877665a1f75f2e4644565b004', '88fb1c848bd15a7162907624c45c630108c75054', '0'), ('1094', '1382987221', 'a05afe0ad44a806877665a1f75f2e4644565b004', '05017cb1afcb2b86a10a9e4eb377537b86c2de0a', '0'), ('1095', '1382987225', 'a05afe0ad44a806877665a1f75f2e4644565b004', '4f267de423fa81b4b93cf6b4e6474f3ae799945f', '0'), ('1096', '1382987227', 'a05afe0ad44a806877665a1f75f2e4644565b004', '620f24534595f67b66cd4c442aa78642d2904d90', '0'), ('1097', '1382987228', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'ac3475ef35c6106f7d4d54224b091080b5e36c67', '0'), ('1098', '1382987317', 'a05afe0ad44a806877665a1f75f2e4644565b004', '19bf7341534ca044dfd9f04c8b69adbe532a5cae', '0'), ('1099', '1382987321', 'a05afe0ad44a806877665a1f75f2e4644565b004', '141f90d4876f49244fa3b809e6b4488d1dd89d37', '0'), ('1100', '1382987322', 'a05afe0ad44a806877665a1f75f2e4644565b004', '884e7fa1c8934fd7bd044415577f793c90b78040', '0'), ('1101', '1382987324', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'f3e7da1bc1775b4b1644e71db88195cc2c30ae6b', '0'), ('1102', '1382987337', 'a05afe0ad44a806877665a1f75f2e4644565b004', '96fbc0fada640bb3aa8aa7a05178579aadddcbd7', '0'), ('1103', '1382987341', 'a05afe0ad44a806877665a1f75f2e4644565b004', '882a9813baad1eecd545e546609174403b370604', '0'), ('1104', '1382988026', 'a05afe0ad44a806877665a1f75f2e4644565b004', 'f30c3ead8aeeb45657ed10846ca97ed3b9ddc2d0', '0'), ('1105', '1382988027', 'a05afe0ad44a806877665a1f75f2e4644565b004', '4082f24a7ce08364ee3aeb9e5c00d017c02826a3', '0'), ('1106', '1382988034', 'a05afe0ad44a806877665a1f75f2e4644565b004', '46570af685130b9e5ebd8bdf0900bff9301e25c4', '0'), ('1107', '1382988051', 'a05afe0ad44a806877665a1f75f2e4644565b004', '5f25d5d79f23b716970cd2977fbee930118cea32', '0'), ('1108', '1382988083', 'a05afe0ad44a806877665a1f75f2e4644565b004', '005348a92bfe1ca99c4528a0483969ac1dd5129c', '0'), ('1109', '1383069735', '0', '91e95e90386be2dc2e61154b5ecc94fb3f1e708d', '0'), ('1110', '1383069736', '0', '72c99ea5e3eded10e55e0c2dbeebb722b0a7104c', '0'), ('1111', '1383069748', '0', 'd04b8bbaa0891178ee6e4c7edaaec7334684af86', '0'), ('1112', '1383069748', '0', 'b8d2f554c368de3f53ae7d8ce80a6bd94a406482', '1'), ('1113', '1383071415', '0', 'b55cb4e7cc078704cf67501f6b03350c82be311e', '0'), ('1114', '1383071417', '0', '1039050387bdb902d78fa00238ff2c381866f3d9', '0'), ('1115', '1383071440', '0', '87b55425f05d3c49f5d1726c8ae34a52f0e0f483', '0'), ('1116', '1383071440', '0', 'e2d88726be74323879f20236cf174d23ada390e7', '0'), ('1117', '1383071441', '0', 'd41a54640d93a5a7803045de648aaab7a188fe1b', '0'), ('1118', '1383071441', '0', '1af8f1095ab6aad5df49da546b859a5032f3e964', '0'), ('1119', '1383071441', '0', 'e5149af96138186a3daa12ecf7d39c976e332557', '0'), ('1120', '1383071442', '0', '7213a0ff1abd7ce12082145f9035b04288c3314b', '0'), ('1121', '1383071444', '0', 'c941e85a19348b9c006ef0fefd3f5b86bb42545a', '0'), ('1122', '1383071445', '0', 'e070742b79616339be79e48e261bd6665eb02166', '0'), ('1123', '1383071445', '0', '503e6b9426c4e569e9edd9812176d042a7612040', '0'), ('1124', '1383071445', '0', '9d662a76143855c76e6e250b872f835701c2cd66', '0'), ('1125', '1383071445', '0', 'a941da40663761577d9fe47954876a61152d2de1', '0'), ('1126', '1383071446', '0', '8aebaf1cc1a42c320dcbc1e765b66540a34ab81f', '0'), ('1127', '1383071446', '0', '72716285c00facc5dbc419041ccda1625f461979', '0'), ('1128', '1383071446', '0', 'e187f0c1bffa4e536c8b29060c84de3004da642a', '0'), ('1129', '1383071446', '0', 'a3b90d451d052582cc4235f8b312d43791938dd0', '0'), ('1130', '1383071447', '0', 'b512f88d90ff19dbaafe224ddc2abee81846f427', '0'), ('1131', '1383071448', '0', '203b9d8b82324112417c75ba621fd6729f694260', '0'), ('1132', '1383071448', '0', '49682d4670f3abf62459f0db0ce3913d62bff591', '0'), ('1133', '1383071448', '0', '6f396f0eeda883e441ee4efd21a31adda1417e3c', '0'), ('1134', '1383071448', '0', 'dad16d086e49a917b7c8125de8e1397cbd4ffc6c', '0'), ('1135', '1383071458', '0', '4f81c9d6dc8229cd5497e865ad3882096a8debdc', '0'), ('1136', '1383071460', '0', '2017cc51b51667d9eed4a7d051b532e585e0c245', '0'), ('1137', '1383071475', '0', 'ae19d3e191a30945ed9e84271ec5732e56433d4e', '0'), ('1138', '1383071476', '0', '488834c483cd6fd7d40e0c0a86cdd27fd229891f', '0'), ('1139', '1383071494', '0', 'e17d5ef9616fbe098cf2890940860d6cb3e82544', '0'), ('1140', '1383071495', '0', '4d8f45a625a279f624b9ac753571a49e03458fe3', '0'), ('1141', '1383071499', '0', '3ac89517ccf535edf61d7db5f48d986bdfce5a33', '0'), ('1142', '1383071504', '0', 'd112f4151029df97677b1d0c198aef2f061595df', '0'), ('1143', '1383071505', '0', '188a767a46326290e07a231bb1f77160c9af329b', '0'), ('1144', '1383071533', '0', '19f01acd670b5b07dc8008366a75e566a3adcf08', '0'), ('1145', '1383071545', '0', 'fc3367e3da6e254779f5756709f8127026ada928', '0'), ('1146', '1383071547', '0', 'a6066ea04d59c3b227f93bfafc5d87e1fc69af23', '0'), ('1147', '1383071570', '0', '4b0b702761b76d78579cf7dd37dae39f14c04168', '0'), ('1148', '1383071572', '0', '9c8129191a1ce4d23302898ee59d10e3c7a81340', '0'), ('1149', '1383071575', '0', 'f00b4b18449d066eb334f961c1870b29bc023a80', '0'), ('1150', '1383071577', '0', '82b3408b0950144b8ce5882b9434ea94a92aaaaa', '0'), ('1151', '1383071627', '0', '2a0346eac4437368687fc49577123575cac32a28', '0'), ('1152', '1383071627', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '2c20f5b78bf8c6b2805b29ee17cf97c14e19d54e', '0'), ('1153', '1383071651', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '059cdba64281b61b87905cec4a8e954fe7b6085c', '0'), ('1154', '1383071653', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '062a6885ea7c125b81d5b652b6fe94c75b2c3aed', '1'), ('1155', '1383071668', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'bdede596dba993c4ec6e9ba0d8da59c7cf9a0475', '0'), ('1156', '1383071668', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '127d0e7d1fdebe408855866979bb4a3243ea8a10', '0'), ('1157', '1383071671', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '83c3512c870556095f7f7f907896d2cb5151aaef', '0'), ('1158', '1383071680', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '31ce1d3689267ce6ee8e2db6a136dab03848e9cf', '1'), ('1159', '1383071681', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'f310c82ca05ce3237e4ce6bbadd6556e24354f93', '0');
INSERT INTO `exp_security_hashes` VALUES ('1160', '1383071681', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '93e9ad07470d51f970e34dd7a07ff0d0a56dbfa6', '0'), ('1161', '1383071682', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '822083f62663132719365f2a4c8065e8b6fa39e9', '0'), ('1162', '1383071689', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'c6e221440328527e386a5c190df1e49c1d4acf48', '0'), ('1163', '1383071690', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '805b97c4f40fac8fade31c13ad8bb136181de6a3', '0'), ('1164', '1383071691', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '011bef9e74a99a485c5c82e0ee56fea1db77574e', '0'), ('1165', '1383071694', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'b73546732d0a64a38d246f5e3e1d86d759c749c3', '1'), ('1166', '1383071704', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '0c35e06a91c059618537ec23e298ade6b1cfc4dc', '0'), ('1167', '1383071704', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '7d3cf3f837e2d35260c829a1242d633b2a6807cf', '0'), ('1168', '1383071706', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '56c6daa29e276cc3141659f44cf0c256a125aece', '0'), ('1169', '1383071708', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'b22c94d5ea7e6e28374e9f06793c7a783fd68486', '1'), ('1170', '1383071709', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '36dfd4bfed656dbd23d28be72db776700244b136', '0'), ('1171', '1383071709', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '6a5d956a72b55246c982515eaa3fd01b78d0e9bd', '0'), ('1172', '1383071709', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'b78309cf433e815de49e0cc87debf90dcafb853c', '0'), ('1173', '1383071714', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '841179b1586c84eabdaafb45ba543c9ebcdf1408', '0'), ('1174', '1383071714', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'ac0cb0872261ba87abc0a35cb0be37917f643be4', '0'), ('1175', '1383071716', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '06dbbe995dd0971ed8424d1c1f2c2575271ea861', '0'), ('1176', '1383071719', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '132d1f89df46aa42038d89689d5c3dba0dc744bf', '1'), ('1177', '1383071720', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '9b52b935f97488d3221d84c320c8fa4ef217609e', '0'), ('1178', '1383071720', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'da4ad6e0482d7d1f6c8d35d3faebcf0ba5f331cb', '0'), ('1179', '1383071720', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '5c71781a0212c572a7747a9a2ead3d49019088c7', '0'), ('1180', '1383071746', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'afb385db6454680ecb04df2ac7ffdae195c569b5', '1'), ('1181', '1383071750', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'f044eb7eb7d55ca53958c390ccac971508a4b812', '0'), ('1182', '1383071751', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'e4f536809fa5acc8c3f75adf42b9340fffcf0322', '0'), ('1183', '1383071761', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'd5978d24bda99c2ffa2963e330aa0017e600ef7f', '0'), ('1184', '1383071949', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'ea655870277b3824f608c67b771a46c61beece47', '0'), ('1185', '1383071963', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'b009ddbfe8090fd325d5055bb16b02f7a4907dab', '0'), ('1186', '1383071968', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '64cccc75e9cf3ecc3674823e44a668ee6d3f0bcb', '0'), ('1187', '1383071975', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '16f2afc5895fdfe32254217ce3ad63483afaa68f', '0'), ('1188', '1383071985', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '6b0832f0f6215379e193c0a9e1553a9db5a8b0cd', '0'), ('1189', '1383071987', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'ea00a1e3a4e965f1bba1cda552bceb439cb87dbe', '0'), ('1190', '1383071988', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'd0d9ef823d83c8f26662ccbacddae810b3c9ad8f', '1'), ('1191', '1383072024', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'c02ad62c8fecfbaf5421dbf2ba467c3f120d18ba', '0'), ('1192', '1383072025', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'e742c4cc444ed0c42257cf98d2a8a8bb804b77f9', '0'), ('1193', '1383072044', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'e0c85f8a63976dbd0f79c9e869870fda8fbc3162', '1'), ('1194', '1383072072', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '7d95830eedb9dd3fc4fde3f7a72e4b986c7262e6', '0'), ('1195', '1383072072', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '166f317082a6d892c8034bd47a7fcaf7f53bf166', '0'), ('1196', '1383072098', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '6b4e80ebc8e18b69d292ee76569685420e6e3a90', '0'), ('1197', '1383072108', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '1ba161de615773f6aae5caeeffb16b65b854899f', '0'), ('1198', '1383072131', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '4831b14064081277ab0401caa8bd8bad370b93e0', '1'), ('1199', '1383072238', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '45bcc47a426c52958cbbb50787cd683c48a5a4bc', '0'), ('1200', '1383072239', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '8c20fc6092067821ad304c9315ca11fa5a89cfdc', '0'), ('1201', '1383072242', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '28b2de976e13f7d22f0a763d46099af3bbfdddc0', '0'), ('1202', '1383072244', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '172d7f740d0a6095b7159bb2340c042c6a31c52e', '1'), ('1203', '1383072244', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '79cfaa9aebe41c54202bafb7340026f8b57e429e', '0'), ('1204', '1383072244', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '7ad8744716ba621978dbfbfaacb3ee66849fbabe', '0'), ('1205', '1383072244', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'db3ee6ae7ec3cf947fe671626a592ad86b0de35e', '0'), ('1206', '1383072272', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'a8cca3af6bc89716a02bbd4bf792b58342e5dacb', '0'), ('1207', '1383072283', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '8451439d19082c0f759e2d3f2d42cfbbf6bc8f05', '0'), ('1208', '1383072287', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '4349fc17f5af6815d777ac3e405a49fa4559d90f', '0'), ('1209', '1383072288', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '8417312b6eecd804084e1bf2b9677ff18b2cda13', '0'), ('1210', '1383072288', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '4e3592c73c85b8c81417a1dccda8dd3dc8177be9', '0'), ('1211', '1383072289', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'fac02ccaaf6c353efff7316c2a03054ab060b6b5', '0'), ('1212', '1383072289', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'e79605cb95b549b2c942b5a3bfbfb92b02f4cf21', '0'), ('1213', '1383072294', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'c8413c2fb2ab3c6f07dda3c548a7951da0654061', '0'), ('1214', '1383072294', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'e4924f8e8b6b190c999eed69b5a5b3a18103e743', '0'), ('1215', '1383072294', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '550fcf5902416967f6882d3a060ebc8dba6a6441', '0'), ('1216', '1383072294', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '11a236c4cce5fef0b4dff354ac4cd4c001de23d3', '0'), ('1217', '1383072294', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '476b9d541c8f822a3e5640842b02b3c9665b86c1', '0'), ('1218', '1383072296', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'b4b66257307f139a07189e26b934e86bef279253', '1'), ('1219', '1383072298', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '392c99ac9ca60dc1414fc247679b667613e98eb9', '1'), ('1220', '1383072299', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '1023a5d23252b4236ac1a95e13e089e58b55eb06', '0'), ('1221', '1383072299', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '95dccb048c0e255ffdb52d1e37cb62cf9f8067ab', '1'), ('1222', '1383072332', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '275116f056675792f2f0f083c0a23c79881d43b0', '0'), ('1223', '1383072332', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '102ffeac52455539a1a73984888c22b9032e79e8', '0'), ('1224', '1383072333', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '81e1cc9bbac4845fce73c49a2b8c5c9a038fac62', '0'), ('1225', '1383072348', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'eef44db784b971fd3256d69618adbf18cd4cfe17', '0'), ('1226', '1383072353', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '21bb7e3bc70b52a4e462562348d492e5f4fc78bf', '0'), ('1227', '1383072361', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '1ce66c7dc67191434fef6ed461a8e93cf42c0548', '0'), ('1228', '1383072373', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '78405415ad48cc6ef5cc7da3911d179d41f44af8', '0'), ('1229', '1383072375', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'c6b3a091c158e359603533571f00819d071fdbb1', '0'), ('1230', '1383072680', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '31e6f7833ef471a46f686ef53e03e2d0d9e9a054', '0'), ('1231', '1383072684', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '5dbc5b81b18f34b6ff182bc9b6e21d68f6e86b84', '0'), ('1232', '1383072716', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '6a26cc017318089c5c5597b3a60714cbbd3de3ca', '0'), ('1233', '1383072718', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'a0b8e9059f16f2063f325a53ead8e0701c8450a4', '0'), ('1234', '1383072775', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'b27e6def5fb7d2ce93be21051feb3853fc3e4ef1', '0'), ('1235', '1383072777', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '3b8177f38668255ebef3e3ca2bbd49e5efca6dff', '0'), ('1236', '1383072788', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'c2a65d28ed91c795e72cc29a5f0d8bc5af3ba4a5', '0'), ('1237', '1383072790', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'fe14d036ca1ff58adb0b6dd7dd9e8fe8b5785bb1', '0'), ('1238', '1383072810', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '2518ac30b223f7f73a375417e8c6bc8d2fb1f5df', '0'), ('1239', '1383072814', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'e573e02719e65d6a1b961ad966135e3e4ae3bee8', '0'), ('1240', '1383072820', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'd11fd6625a990c2e25cc8b3dacddbc03985ca2a2', '0'), ('1241', '1383072821', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '2284cbfbc96216a4f7ad9c671e84edff86d1ebb0', '0'), ('1242', '1383072824', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'aedf442c20a8fff2aa4f3317fee3beed4f3f93e1', '1'), ('1243', '1383072832', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'c20ba614f64d2a5bf530d1dcd0ac27d569c3c6b0', '0'), ('1244', '1383072832', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '3c60118cfc698065909ddb8ed7f898d1410c924b', '0'), ('1245', '1383072834', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'c40e0ddcfae44c102603df8ceb42d0e8612c4b70', '0'), ('1246', '1383072836', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'd6dcb5f886eb88d718cc630ff1e098b15b4123a9', '0'), ('1247', '1383072836', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'd2db3650eaaf2b3d40710c894f551fc4a13d84df', '1'), ('1248', '1383072837', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'b254657bfa647d3757c0d37d7358e818ce2e5470', '0'), ('1249', '1383072837', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '6c0b39b4e7c5e83a689bbd576497d7b90dd5b9b6', '0'), ('1250', '1383072837', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '08a22a30a032f187f05995c530e9a43251283682', '0'), ('1251', '1383072852', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'ea62dfb43fcad42f05c64c3d314c64ae7bf89e32', '0'), ('1252', '1383072856', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '2786953a28b52170921b15dfbfc122934ad6945e', '0'), ('1253', '1383072856', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '91d21c61f3ac0914e589cbe8dc20db9e96699f2a', '0'), ('1254', '1383072856', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '76ea60a1d3534f24992057d94a5545bf40db8f03', '0'), ('1255', '1383072856', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '5407efb88a0917ab8ff523e94317a9abdbbeea10', '0'), ('1256', '1383072856', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '2ab4d81e8dd0a0014ea7f0f8b6e0e7655e97cbef', '0'), ('1257', '1383072859', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '94dca100e5718dfd38490b65455cd061653e570b', '1'), ('1258', '1383072860', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '6c69cc2ce3a039a1053f009043de757d41664f43', '0'), ('1259', '1383072860', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '4871aeb666001fa05737c63765693c58f8e0ae68', '0'), ('1260', '1383072862', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'ad9d0f08f71a42d734e167dd0d5b57af55fd6d8b', '0'), ('1261', '1383072864', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '2a57229e6e670520ca589e06d1466268f265a027', '0'), ('1262', '1383072876', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '733d518091dac446821eafa949bdb730d15f0437', '0'), ('1263', '1383072878', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '61cf9f573e5a1b72e22e2554d51e16b82a33e70a', '0'), ('1264', '1383072888', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '0827f818539417792b00bbe19e412c39cf6e4662', '0'), ('1265', '1383072897', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'a99e72482d409c59bd020f84a09d78ba5b151780', '0'), ('1266', '1383072916', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '1df595eaecb4e42ab6ac61ac519d8d5f9a75246c', '0'), ('1267', '1383072917', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '8f7b3fe042fcdeb090a6fdd8a94bd8485ebd6f21', '0'), ('1268', '1383072919', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '78a9026de88a835c4ef33367b5c2ea07030444ed', '1'), ('1269', '1383072923', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '4eb581e5e5ffdf09809cbbdfabd5d12b11bd9e6d', '0'), ('1270', '1383072923', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '903f6465ec6b5fc43be330fe6806df7a10bd2693', '0'), ('1271', '1383072925', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'ff91aeae36cf3f9653da0277458ee655454ae5b1', '0'), ('1272', '1383072926', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '6819e05e7943cb43e17d0ddc2a5e9986a6009e69', '0'), ('1273', '1383072927', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '5fba778b2e608562e8f63048e0afde0459795b8c', '1'), ('1274', '1383072927', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '2feb20377a8430006906e1a8280df01015ba563c', '0'), ('1275', '1383072927', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '3cbb634ea9133f7702f3681844aab9f4646db9f2', '0'), ('1276', '1383072928', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '5a1c8fb7c879967613db4afe22d5aa219988f4c0', '0'), ('1277', '1383072933', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'd51fdd702941fa04066e97f2d02643809e103b8b', '0'), ('1278', '1383072933', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'a610f2769ffcb1d55e8349f8b5aeafa396534c6c', '0'), ('1279', '1383072934', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'ae64a5c73a110a3eff6f020f265374c9c44fc08d', '0'), ('1280', '1383072948', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'aae4096fe2a01a2fb97adc711c5cc41d9723f562', '0'), ('1281', '1383072972', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '11cb84fd2189db5bd2c70d41ba54a3a625486aee', '0'), ('1282', '1383072982', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '42319d6b143649253015d38abddfe08e56cd7d9f', '0'), ('1283', '1383073084', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '69598cf9d7040a02e85e329294c27d45f820e625', '0'), ('1284', '1383073148', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '4eb23ae9b876fb7e9c4c88e84239062b4cda1c78', '0'), ('1285', '1383073155', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '7d9266e601b7bdeb97af93a81b18fb366654d90a', '0'), ('1286', '1383073175', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '5e750cfe8aa619c41c3a48f188d9dda92f9e733a', '0'), ('1287', '1383073189', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '81c1ead0ccf5ad6dbd60ce539681ecbc2beccafd', '0'), ('1288', '1383073191', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '45274645c5db5e480d912b18aa1ae01f07205bf6', '0'), ('1289', '1383073202', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'fe4b5648405fdde860149d69ef240a62a53dbbf3', '0'), ('1290', '1383073220', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'd8818de309c4cf577498504908c0e64a683dccb1', '0'), ('1291', '1383073304', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'b667297e97ad34f7e1e26ae91f3f7f09a9737318', '0'), ('1292', '1383073308', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '450494aa83c2f03d95fda322fdffe0169cedc32f', '0'), ('1293', '1383073329', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '610a250eeee6dd31bb8c9f955a8918c5b0e99bd3', '0'), ('1294', '1383073382', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '27c09fa78c6fbc3814e07010728f0cc72de2157f', '0'), ('1295', '1383073388', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'a6c57cd800658773603389cf5df6f70334849093', '0'), ('1296', '1383073400', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'ee965e138babc14e40ef25a8f9e5f3f3ac2414e0', '0'), ('1297', '1383073413', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', '22cac01f66dafcdf6da3c2053391bdafd9371e55', '0'), ('1298', '1383073416', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'a0e25f9e8e337b7fed90d92f24bb74cfbd419a25', '0'), ('1299', '1383073421', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'fc60e17ec4cd0705687b03b2c5cd06102f9aeb35', '0'), ('1300', '1383076573', '42f0ee8138c1eb0fbbec73310d583c5a633f852c', 'ce52414017e27dffebc83f2f1a099de376c0a084', '0');
COMMIT;

-- ----------------------------
--  Table structure for `exp_seolite_config`
-- ----------------------------
DROP TABLE IF EXISTS `exp_seolite_config`;
CREATE TABLE `exp_seolite_config` (
  `seolite_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned DEFAULT NULL,
  `template` text,
  `default_keywords` varchar(1024) NOT NULL,
  `default_description` varchar(1024) NOT NULL,
  `default_title_postfix` varchar(60) NOT NULL,
  PRIMARY KEY (`seolite_config_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_seolite_config`
-- ----------------------------
BEGIN;
INSERT INTO `exp_seolite_config` VALUES ('1', '1', '<title>{title}{site_name}</title>\n<meta name=\'keywords\' content=\'{meta_keywords}\' />\n<meta name=\'description\' content=\'{meta_description}\' />\n<link rel=\'canonical\' href=\'{canonical_url}\' />\n<!-- generated by seo_lite -->', 'your, default, keywords, here', 'Your default description here', ' |&nbsp;');
COMMIT;

-- ----------------------------
--  Table structure for `exp_seolite_content`
-- ----------------------------
DROP TABLE IF EXISTS `exp_seolite_content`;
CREATE TABLE `exp_seolite_content` (
  `seolite_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) NOT NULL,
  `entry_id` int(10) NOT NULL,
  `title` varchar(1024) DEFAULT NULL,
  `keywords` varchar(1024) NOT NULL,
  `description` text,
  PRIMARY KEY (`seolite_content_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_seolite_content`
-- ----------------------------
BEGIN;
INSERT INTO `exp_seolite_content` VALUES ('1', '1', '1', '', '', ''), ('2', '1', '2', '', '', ''), ('3', '1', '3', '', '', ''), ('4', '1', '4', '', '', ''), ('5', '1', '5', '', '', '');
COMMIT;

-- ----------------------------
--  Table structure for `exp_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `exp_sessions`;
CREATE TABLE `exp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `fingerprint` varchar(40) NOT NULL,
  `sess_start` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_sessions`
-- ----------------------------
BEGIN;
INSERT INTO `exp_sessions` VALUES ('42f0ee8138c1eb0fbbec73310d583c5a633f852c', '1', '1', '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36', 'dee9c18f2202b6da11ef84acf9ad7fda', '1383071627', '1383076573');
COMMIT;

-- ----------------------------
--  Table structure for `exp_sites`
-- ----------------------------
DROP TABLE IF EXISTS `exp_sites`;
CREATE TABLE `exp_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` mediumtext NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  `site_pages` longtext,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_sites`
-- ----------------------------
BEGIN;
INSERT INTO `exp_sites` VALUES ('1', 'ideasFoundation', 'default_site', null, 'YTo4ODp7czoxMDoic2l0ZV9pbmRleCI7czo5OiJpbmRleC5waHAiO3M6ODoic2l0ZV91cmwiO3M6Mjg6Imh0dHA6Ly9pZGVhc2ZvdW5kYXRpb25lZS5jbi8iO3M6MTY6InRoZW1lX2ZvbGRlcl91cmwiO3M6MzU6Imh0dHA6Ly9pZGVhc2ZvdW5kYXRpb25lZS5jbi90aGVtZXMvIjtzOjE1OiJ3ZWJtYXN0ZXJfZW1haWwiO3M6MjE6InNhbUBjcmVhdGl2ZW5lcmRzLm9yZyI7czoxNDoid2VibWFzdGVyX25hbWUiO3M6MDoiIjtzOjIwOiJjaGFubmVsX25vbWVuY2xhdHVyZSI7czo3OiJjaGFubmVsIjtzOjEwOiJtYXhfY2FjaGVzIjtzOjM6IjE1MCI7czoxMToiY2FwdGNoYV91cmwiO3M6NDQ6Imh0dHA6Ly9pZGVhc2ZvdW5kYXRpb25lZS5jbi9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX3BhdGgiO3M6ODM6Ii9Vc2Vycy9tYWNib29rL0RldmVsb3BtZW50L0NyZWF0aXZlTmVyZHMvaWRlYXNGb3VuZGF0aW9uL3NpdGUvaHRtbC9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX2ZvbnQiO3M6MToieSI7czoxMjoiY2FwdGNoYV9yYW5kIjtzOjE6InkiO3M6MjM6ImNhcHRjaGFfcmVxdWlyZV9tZW1iZXJzIjtzOjE6Im4iO3M6MTc6ImVuYWJsZV9kYl9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImVuYWJsZV9zcWxfY2FjaGluZyI7czoxOiJuIjtzOjE4OiJmb3JjZV9xdWVyeV9zdHJpbmciO3M6MToibiI7czoxMzoic2hvd19wcm9maWxlciI7czoxOiJuIjtzOjE4OiJ0ZW1wbGF0ZV9kZWJ1Z2dpbmciO3M6MToibiI7czoxNToiaW5jbHVkZV9zZWNvbmRzIjtzOjE6Im4iO3M6MTM6ImNvb2tpZV9kb21haW4iO3M6MDoiIjtzOjExOiJjb29raWVfcGF0aCI7czowOiIiO3M6MTc6InVzZXJfc2Vzc2lvbl90eXBlIjtzOjE6ImMiO3M6MTg6ImFkbWluX3Nlc3Npb25fdHlwZSI7czoyOiJjcyI7czoyMToiYWxsb3dfdXNlcm5hbWVfY2hhbmdlIjtzOjE6InkiO3M6MTg6ImFsbG93X211bHRpX2xvZ2lucyI7czoxOiJ5IjtzOjE2OiJwYXNzd29yZF9sb2Nrb3V0IjtzOjE6InkiO3M6MjU6InBhc3N3b3JkX2xvY2tvdXRfaW50ZXJ2YWwiO3M6MToiMSI7czoyMDoicmVxdWlyZV9pcF9mb3JfbG9naW4iO3M6MToieSI7czoyMjoicmVxdWlyZV9pcF9mb3JfcG9zdGluZyI7czoxOiJ5IjtzOjI0OiJyZXF1aXJlX3NlY3VyZV9wYXNzd29yZHMiO3M6MToibiI7czoxOToiYWxsb3dfZGljdGlvbmFyeV9wdyI7czoxOiJ5IjtzOjIzOiJuYW1lX29mX2RpY3Rpb25hcnlfZmlsZSI7czowOiIiO3M6MTc6Inhzc19jbGVhbl91cGxvYWRzIjtzOjE6InkiO3M6MTU6InJlZGlyZWN0X21ldGhvZCI7czo4OiJyZWRpcmVjdCI7czo5OiJkZWZ0X2xhbmciO3M6NzoiZW5nbGlzaCI7czo4OiJ4bWxfbGFuZyI7czoyOiJlbiI7czoxMjoic2VuZF9oZWFkZXJzIjtzOjE6InkiO3M6MTE6Imd6aXBfb3V0cHV0IjtzOjE6InkiO3M6MTM6ImxvZ19yZWZlcnJlcnMiO3M6MToibiI7czoxMzoibWF4X3JlZmVycmVycyI7czozOiI1MDAiO3M6MTE6InRpbWVfZm9ybWF0IjtzOjI6InVzIjtzOjEzOiJzZXJ2ZXJfb2Zmc2V0IjtzOjA6IiI7czoyMToiZGVmYXVsdF9zaXRlX3RpbWV6b25lIjtzOjEzOiJFdXJvcGUvTG9uZG9uIjtzOjEzOiJtYWlsX3Byb3RvY29sIjtzOjQ6Im1haWwiO3M6MTE6InNtdHBfc2VydmVyIjtzOjA6IiI7czoxMzoic210cF91c2VybmFtZSI7czowOiIiO3M6MTM6InNtdHBfcGFzc3dvcmQiO3M6MDoiIjtzOjExOiJlbWFpbF9kZWJ1ZyI7czoxOiJuIjtzOjEzOiJlbWFpbF9jaGFyc2V0IjtzOjU6InV0Zi04IjtzOjE1OiJlbWFpbF9iYXRjaG1vZGUiO3M6MToibiI7czoxNjoiZW1haWxfYmF0Y2hfc2l6ZSI7czowOiIiO3M6MTE6Im1haWxfZm9ybWF0IjtzOjU6InBsYWluIjtzOjk6IndvcmRfd3JhcCI7czoxOiJ5IjtzOjIyOiJlbWFpbF9jb25zb2xlX3RpbWVsb2NrIjtzOjE6IjUiO3M6MjI6ImxvZ19lbWFpbF9jb25zb2xlX21zZ3MiO3M6MToieSI7czo4OiJjcF90aGVtZSI7czo3OiJkZWZhdWx0IjtzOjIxOiJlbWFpbF9tb2R1bGVfY2FwdGNoYXMiO3M6MToibiI7czoxNjoibG9nX3NlYXJjaF90ZXJtcyI7czoxOiJ5IjtzOjEyOiJzZWN1cmVfZm9ybXMiO3M6MToieSI7czoxOToiZGVueV9kdXBsaWNhdGVfZGF0YSI7czoxOiJ5IjtzOjI0OiJyZWRpcmVjdF9zdWJtaXR0ZWRfbGlua3MiO3M6MToibiI7czoxNjoiZW5hYmxlX2NlbnNvcmluZyI7czoxOiJuIjtzOjE0OiJjZW5zb3JlZF93b3JkcyI7czowOiIiO3M6MTg6ImNlbnNvcl9yZXBsYWNlbWVudCI7czowOiIiO3M6MTA6ImJhbm5lZF9pcHMiO3M6MDoiIjtzOjEzOiJiYW5uZWRfZW1haWxzIjtzOjA6IiI7czoxNjoiYmFubmVkX3VzZXJuYW1lcyI7czowOiIiO3M6MTk6ImJhbm5lZF9zY3JlZW5fbmFtZXMiO3M6MDoiIjtzOjEwOiJiYW5fYWN0aW9uIjtzOjg6InJlc3RyaWN0IjtzOjExOiJiYW5fbWVzc2FnZSI7czozNDoiVGhpcyBzaXRlIGlzIGN1cnJlbnRseSB1bmF2YWlsYWJsZSI7czoxNToiYmFuX2Rlc3RpbmF0aW9uIjtzOjIxOiJodHRwOi8vd3d3LnlhaG9vLmNvbS8iO3M6MTY6ImVuYWJsZV9lbW90aWNvbnMiO3M6MToieSI7czoxMjoiZW1vdGljb25fdXJsIjtzOjQzOiJodHRwOi8vaWRlYXNmb3VuZGF0aW9uZWUuY24vaW1hZ2VzL3NtaWxleXMvIjtzOjE5OiJyZWNvdW50X2JhdGNoX3RvdGFsIjtzOjQ6IjEwMDAiO3M6MTc6Im5ld192ZXJzaW9uX2NoZWNrIjtzOjE6InkiO3M6MTc6ImVuYWJsZV90aHJvdHRsaW5nIjtzOjE6Im4iO3M6MTc6ImJhbmlzaF9tYXNrZWRfaXBzIjtzOjE6InkiO3M6MTQ6Im1heF9wYWdlX2xvYWRzIjtzOjI6IjEwIjtzOjEzOiJ0aW1lX2ludGVydmFsIjtzOjE6IjgiO3M6MTI6ImxvY2tvdXRfdGltZSI7czoyOiIzMCI7czoxNToiYmFuaXNobWVudF90eXBlIjtzOjc6Im1lc3NhZ2UiO3M6MTQ6ImJhbmlzaG1lbnRfdXJsIjtzOjA6IiI7czoxODoiYmFuaXNobWVudF9tZXNzYWdlIjtzOjUwOiJZb3UgaGF2ZSBleGNlZWRlZCB0aGUgYWxsb3dlZCBwYWdlIGxvYWQgZnJlcXVlbmN5LiI7czoxNzoiZW5hYmxlX3NlYXJjaF9sb2ciO3M6MToieSI7czoxOToibWF4X2xvZ2dlZF9zZWFyY2hlcyI7czozOiI1MDAiO3M6MTc6InRoZW1lX2ZvbGRlcl9wYXRoIjtzOjc0OiIvVXNlcnMvbWFjYm9vay9EZXZlbG9wbWVudC9DcmVhdGl2ZU5lcmRzL2lkZWFzRm91bmRhdGlvbi9zaXRlL2h0bWwvdGhlbWVzLyI7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjExOiJydGVfZW5hYmxlZCI7czoxOiJ5IjtzOjIyOiJydGVfZGVmYXVsdF90b29sc2V0X2lkIjtzOjE6IjEiO30=', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6NDM6Imh0dHA6Ly9pZGVhc2ZvdW5kYXRpb25lZS5jbi9pbWFnZXMvYXZhdGFycy8iO3M6MTE6ImF2YXRhcl9wYXRoIjtzOjgyOiIvVXNlcnMvbWFjYm9vay9EZXZlbG9wbWVudC9DcmVhdGl2ZU5lcmRzL2lkZWFzRm91bmRhdGlvbi9zaXRlL2h0bWwvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjQ5OiJodHRwOi8vaWRlYXNmb3VuZGF0aW9uZWUuY24vaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjEwOiJwaG90b19wYXRoIjtzOjg4OiIvVXNlcnMvbWFjYm9vay9EZXZlbG9wbWVudC9DcmVhdGl2ZU5lcmRzL2lkZWFzRm91bmRhdGlvbi9zaXRlL2h0bWwvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NTc6Imh0dHA6Ly9pZGVhc2ZvdW5kYXRpb25lZS5jbi9pbWFnZXMvc2lnbmF0dXJlX2F0dGFjaG1lbnRzLyI7czoxMjoic2lnX2ltZ19wYXRoIjtzOjk2OiIvVXNlcnMvbWFjYm9vay9EZXZlbG9wbWVudC9DcmVhdGl2ZU5lcmRzL2lkZWFzRm91bmRhdGlvbi9zaXRlL2h0bWwvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo4OToiL1VzZXJzL21hY2Jvb2svRGV2ZWxvcG1lbnQvQ3JlYXRpdmVOZXJkcy9pZGVhc0ZvdW5kYXRpb24vc2l0ZS9odG1sL2ltYWdlcy9wbV9hdHRhY2htZW50cy8iO3M6MjM6InBydl9tc2dfbWF4X2F0dGFjaG1lbnRzIjtzOjE6IjMiO3M6MjI6InBydl9tc2dfYXR0YWNoX21heHNpemUiO3M6MzoiMjUwIjtzOjIwOiJwcnZfbXNnX2F0dGFjaF90b3RhbCI7czozOiIxMDAiO3M6MTk6InBydl9tc2dfaHRtbF9mb3JtYXQiO3M6NDoic2FmZSI7czoxODoicHJ2X21zZ19hdXRvX2xpbmtzIjtzOjE6InkiO3M6MTc6InBydl9tc2dfbWF4X2NoYXJzIjtzOjQ6IjYwMDAiO3M6MTk6Im1lbWJlcmxpc3Rfb3JkZXJfYnkiO3M6MTE6InRvdGFsX3Bvc3RzIjtzOjIxOiJtZW1iZXJsaXN0X3NvcnRfb3JkZXIiO3M6NDoiZGVzYyI7czoyMDoibWVtYmVybGlzdF9yb3dfbGltaXQiO3M6MjoiMjAiO30=', 'YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJ5IjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJuIjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czo3MToiL1VzZXJzL21hY2Jvb2svRGV2ZWxvcG1lbnQvQ3JlYXRpdmVOZXJkcy9pZGVhc0ZvdW5kYXRpb24vc2l0ZS90ZW1wbGF0ZXMiO30=', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjc2OiIvVXNlcnMvbWFjYm9vay9EZXZlbG9wbWVudC9DcmVhdGl2ZU5lcmRzL2lkZWFzRm91bmRhdGlvbi9zaXRlL2h0bWwvaW5kZXgucGhwIjtzOjMyOiIwYmYxNmNkODJmMGVjZjA4MGQzNjdiYjUzZmMyMzllYyI7czo3OiJlbWFpbGVkIjthOjA6e319', 'YToxOntpOjE7YTozOntzOjM6InVybCI7czozODoiaHR0cDovL2lkZWFzZm91bmRhdGlvbmVlLmNuL2luZGV4LnBocC8iO3M6NDoidXJpcyI7YTozOntpOjE7czo2OiIvaG9tZS8iO2k6NDtzOjY6Ii9ibG9nLyI7aTo1O3M6MTg6Ii9ibG9nL2EtdGVzdC1ibG9nLyI7fXM6OToidGVtcGxhdGVzIjthOjM6e2k6MTtzOjE6IjEiO2k6NDtzOjE6IjEiO2k6NTtzOjI6IjE0Ijt9fX0=');
COMMIT;

-- ----------------------------
--  Table structure for `exp_snippets`
-- ----------------------------
DROP TABLE IF EXISTS `exp_snippets`;
CREATE TABLE `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_specialty_templates`
-- ----------------------------
DROP TABLE IF EXISTS `exp_specialty_templates`;
CREATE TABLE `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_specialty_templates`
-- ----------------------------
BEGIN;
INSERT INTO `exp_specialty_templates` VALUES ('1', '1', 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>'), ('2', '1', 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>'), ('3', '1', 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}'), ('4', '1', 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n'), ('5', '1', 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}'), ('6', '1', 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}'), ('7', '1', 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}'), ('8', '1', 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}'), ('9', '1', 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}'), ('10', '1', 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}'), ('11', '1', 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}'), ('12', '1', 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}'), ('13', '1', 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}'), ('14', '1', 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}'), ('15', '1', 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');
COMMIT;

-- ----------------------------
--  Table structure for `exp_stats`
-- ----------------------------
DROP TABLE IF EXISTS `exp_stats`;
CREATE TABLE `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_stats`
-- ----------------------------
BEGIN;
INSERT INTO `exp_stats` VALUES ('1', '1', '1', '1', 'CreativeNerds', '5', '0', '0', '0', '1383071700', '0', '0', '1383076573', '9', '1383071963', '1383158641');
COMMIT;

-- ----------------------------
--  Table structure for `exp_status_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_status_groups`;
CREATE TABLE `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_status_groups`
-- ----------------------------
BEGIN;
INSERT INTO `exp_status_groups` VALUES ('1', '1', 'Statuses');
COMMIT;

-- ----------------------------
--  Table structure for `exp_status_no_access`
-- ----------------------------
DROP TABLE IF EXISTS `exp_status_no_access`;
CREATE TABLE `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_statuses`
-- ----------------------------
DROP TABLE IF EXISTS `exp_statuses`;
CREATE TABLE `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_statuses`
-- ----------------------------
BEGIN;
INSERT INTO `exp_statuses` VALUES ('1', '1', '1', 'open', '1', '009933'), ('2', '1', '1', 'closed', '2', '990000');
COMMIT;

-- ----------------------------
--  Table structure for `exp_structure`
-- ----------------------------
DROP TABLE IF EXISTS `exp_structure`;
CREATE TABLE `exp_structure` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `listing_cid` int(6) unsigned NOT NULL DEFAULT '0',
  `lft` smallint(5) unsigned NOT NULL DEFAULT '0',
  `rgt` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dead` varchar(100) NOT NULL,
  `hidden` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_structure`
-- ----------------------------
BEGIN;
INSERT INTO `exp_structure` VALUES ('0', '0', '0', '0', '0', '1', '6', 'root', 'n'), ('1', '1', '0', '1', '0', '2', '3', '', 'n'), ('1', '4', '0', '1', '4', '4', '5', '', 'n');
COMMIT;

-- ----------------------------
--  Table structure for `exp_structure_channels`
-- ----------------------------
DROP TABLE IF EXISTS `exp_structure_channels`;
CREATE TABLE `exp_structure_channels` (
  `site_id` smallint(5) unsigned NOT NULL,
  `channel_id` mediumint(8) unsigned NOT NULL,
  `template_id` int(10) unsigned NOT NULL,
  `type` enum('page','listing','asset','unmanaged') NOT NULL DEFAULT 'unmanaged',
  `split_assets` enum('y','n') NOT NULL DEFAULT 'n',
  `show_in_page_selector` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`site_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_structure_channels`
-- ----------------------------
BEGIN;
INSERT INTO `exp_structure_channels` VALUES ('1', '1', '1', 'page', 'n', 'y'), ('1', '2', '0', 'asset', 'n', 'y'), ('1', '3', '0', 'asset', 'n', 'y'), ('1', '4', '14', 'listing', 'n', 'y');
COMMIT;

-- ----------------------------
--  Table structure for `exp_structure_listings`
-- ----------------------------
DROP TABLE IF EXISTS `exp_structure_listings`;
CREATE TABLE `exp_structure_listings` (
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(6) unsigned NOT NULL DEFAULT '0',
  `template_id` int(6) unsigned NOT NULL DEFAULT '0',
  `uri` varchar(75) NOT NULL,
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_structure_listings`
-- ----------------------------
BEGIN;
INSERT INTO `exp_structure_listings` VALUES ('1', '5', '4', '4', '14', 'a-test-blog');
COMMIT;

-- ----------------------------
--  Table structure for `exp_structure_members`
-- ----------------------------
DROP TABLE IF EXISTS `exp_structure_members`;
CREATE TABLE `exp_structure_members` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `nav_state` text,
  PRIMARY KEY (`site_id`,`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_structure_settings`
-- ----------------------------
DROP TABLE IF EXISTS `exp_structure_settings`;
CREATE TABLE `exp_structure_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(8) unsigned NOT NULL DEFAULT '1',
  `var` varchar(60) NOT NULL,
  `var_value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_structure_settings`
-- ----------------------------
BEGIN;
INSERT INTO `exp_structure_settings` VALUES ('1', '0', 'action_ajax_move', '44'), ('2', '0', 'module_id', '11'), ('13', '1', 'add_trailing_slash', 'y'), ('12', '1', 'hide_hidden_templates', 'y'), ('11', '1', 'redirect_on_publish', 'n'), ('10', '1', 'redirect_on_login', 'y');
COMMIT;

-- ----------------------------
--  Table structure for `exp_template_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_template_groups`;
CREATE TABLE `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`),
  KEY `group_name_idx` (`group_name`),
  KEY `group_order_idx` (`group_order`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_template_groups`
-- ----------------------------
BEGIN;
INSERT INTO `exp_template_groups` VALUES ('1', '1', 'site', '1', 'y'), ('3', '1', 'embeds', '2', 'n');
COMMIT;

-- ----------------------------
--  Table structure for `exp_template_member_groups`
-- ----------------------------
DROP TABLE IF EXISTS `exp_template_member_groups`;
CREATE TABLE `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_template_no_access`
-- ----------------------------
DROP TABLE IF EXISTS `exp_template_no_access`;
CREATE TABLE `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_templates`
-- ----------------------------
DROP TABLE IF EXISTS `exp_templates`;
CREATE TABLE `exp_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_templates`
-- ----------------------------
BEGIN;
INSERT INTO `exp_templates` VALUES ('1', '1', '1', 'index', 'y', 'webpage', '', '', '1382723476', '1', 'n', '0', '', 'n', 'n', 'o', '212'), ('10', '1', '3', '_header', 'y', 'webpage', '			<header>\n				<div class=\"inner\">\n\n					<div class=\"logo\">\n						<a href=\"/\"></a>\n					</div>\n\n					<ul class=\"nav\">\n						<li><a class=\"active\" href=\"#what-we-do\">WHAT</a></li>\n						<li><a href=\"#who-we-work\">WITH</a></li>\n						<li><a href=\"#us\">WHO</a></li>\n						<li><a href=\"#blog\">BLOG</a></li>\n						<li class=\"seperate\">/</li>\n					</ul>\n					<ul class=\"social-section\">\n						<li><a href=\"#\" class=\"twitter\"></a></li>\n						<li><a href=\"#\" class=\"facebook\"></a></li>\n						<li><a href=\"#\" class=\"youtube\"></a></li>\n						<li><a href=\"#\" class=\"vimeo\"></a></li>\n					</ul>\n				</div>\n			</header>', null, '1382723192', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('9', '1', '3', '_header-meta', 'y', 'webpage', '<!DOCTYPE html>\n<!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->\n<!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->\n<!--[if IE 8]>         <html class=\"no-js lt-ie9\"> <![endif]-->\n<!--[if gt IE 8]><!--> <html class=\"no-js\"> <!--<![endif]-->\n<head>\n	<meta charset=\"utf-8\">\n	<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">\n	<title></title>\n	<meta name=\"description\" content=\"\">\n	<meta name=\"viewport\" content=\"width=device-width\">\n\n	<link href=\'http://fonts.googleapis.com/css?family=Just+Another+Hand\' rel=\'stylesheet\' type=\'text/css\'>\n	<link rel=\"stylesheet\" href=\"css/screen.css\">\n\n	<script src=\"js/vendor/modernizr-2.6.2.min.js\"></script>\n</head>\n<body>\n		<!--[if lt IE 7]>\n			<p class=\"chromeframe\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> or <a href=\"http://www.google.com/chromeframe/?redirect=true\">activate Google Chrome Frame</a> to improve your experience.</p>\n			<![endif]-->', null, '1382723192', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('8', '1', '3', '_footer', 'y', 'webpage', '<footer>\n	<div class=\"inner\">\n		<div class=\"col-1\">\n			<h3>Our Network</h3>\n			<ul>\n				<li><a href=\"#\">link</a></li>\n				<li><a href=\"#\">link</a></li>\n				<li><a href=\"#\">link</a></li>\n				<li><a href=\"#\">link</a></li>\n			</ul>\n		</div>\n\n		<div class=\"col-2\">\n			<h3>Chat To Us</h3>\n			<ul>\n				<li><a href=\"#\">Twitter</a></li>\n				<li><a href=\"#\">Facebook</a></li>\n				<li><a href=\"#\">Youtube</a></li>\n				<li><a href=\"#\">Instagram</a></li>\n			</ul>\n		</div>\n\n		<div class=\"col-3\">\n			<h3>Find Us</h3>\n			<div class=\"address-section\">\n				<span>The ideas Foundation <br />\n					c/o Oglivy & Mather <br />\n					10 Cabot Square<br />\n					London, E14 4QB\n				</span>\n			</div>\n		</div>\n\n		<div class=\"col-4\">\n			<h3>Newsletter</h3>\n			<span>\n				Subscribe below to kept in the loops\n				with all happenings at Ideas Foundation.\n			</span>\n		</div>\n	</div>\n</footer>', null, '1382723192', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('7', '1', '3', '_footer-meta', 'y', 'webpage', '<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js\"></script>\n				<script>window.jQuery || document.write(\'<script src=\"js/vendor/jquery-1.10.1.min.js\"><\\/script>\')</script>\n\n				<script src=\"js/vendor/stella.js\"></script>\n				<script src=\"js/vendor/owl.carousel.min.js\"></script>\n				<script src=\"js/vendor/jquery.jcarousel.min.js\"></script>\n				<script src=\"js/main.js\"></script>\n			</body>\n			</html>', null, '1382723192', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('11', '1', '3', 'index', 'y', 'webpage', '', null, '1382723449', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('12', '1', '3', '_logos', 'y', 'webpage', '{exp:channel:entries channel=\"logos\"}\n<a href=\"{logos_link}\">\n	{logos_image}\n		{exp:imgsizer src=\"{url}\" width=\"67\"}\n			<img src=\"{sized}\" alt=\"\">\n		{/exp:imgsizer}\n	{/logos_image}\n</a>\n{/exp:channel:entries}', null, '1382885343', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('13', '1', '3', '_carousel', 'y', 'webpage', '{exp:channel:categories channel=\"carousel_item\" show_empty=\"no\"}\n    {category_name}\n{/exp:channel:categories}', null, '1382985717', '1', 'n', '0', '', 'n', 'n', 'o', '0'), ('14', '1', '3', 'blogajax', 'y', 'webpage', '<article data-id=\"first-blog\">\n	<figure class=\"polaroid\">\n		<img src=\"http://placehold.it/370x370\" alt=\"\">\n	</figure>\n\n	<figure class=\"polaroid secondary\">\n		<img src=\"http://placehold.it/370x370\" alt=\"\">\n	</figure>\n\n	<div class=\"tab-section\">\n		<div class=\"line-span\"></div>\n		<h3>Lorem ipsum dolar...</h3>\n\n		<div class=\"tab-content\">\n			<p>\n				SCHOOLS - Lorem ipsum dolor sit amet, consectetur \n				adipiscing elit. Quisque id metus libero. Pellentesque \n				habitant morbi tristique senectus et netus et malesuada \n				fames ac turpis egestas.  Lorem ipsum dolor sit amet, \n				consectetur  adipiscing elit. Quisque id metus libero. \n				Pellentesque habitant morbi tristique senectus et netus\n				fames ac turpis egestas.  senectus et netus et malesuada \n				fames ac turpis egestas.  Lorem ipsum dolor sit amet, consect\n				adipiscing elit. Quisque id .\n			</p>\n\n			<p>\n				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae, iure, sequi esse vitae nulla eum facere explicabo! Inventore, error totam at quam sed saepe enim! Dolore alias veniam nostrum odio.\n			</p>\n\n			<p>\n				<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi, enim possimus ut quae minus non omnis asperiores iste assumenda pariatur modi expedita necessitatibus blanditiis perspiciatis nemo animi quod doloremque ipsa.</span>\n				<span>Debitis, impedit, ullam, neque, dolorum eos fuga consectetur quisquam asperiores voluptatum aspernatur delectus quis cum odio aperiam voluptas commodi numquam voluptate voluptatibus officiis deserunt porro minima eaque iste nihil nemo!</span>\n				<span>Ea, in error vitae neque modi animi dolores suscipit qui tenetur quisquam. Earum, repellat, voluptatum itaque neque officiis alias veritatis aspernatur eos obcaecati eius iusto vel! Corrupti laborum tempora laudantium.</span>\n			</p>\n		</div>\n	</div>\n	<div class=\"testimonial clearfix\">\n		<div class=\"line-break\"></div>\n		<a href=\"\" class=\"close-blog\">\n			CLOSE\n		</a>\n	</div>\n</article>', null, '1383071417', '1', 'n', '0', '', 'n', 'n', 'o', '48'), ('15', '1', '3', '_blog', 'y', 'webpage', '{exp:channel:entries}\n<article class=\"{switch=\"one|two|three|four\"}\">\n	<figure >\n		<img src=\"http://placehold.it/155x155\" alt=\"\">\n	</figure>\n	<div class=\"blog-content\">\n		<h3>{title}</h3>\n		<p>{excerpt}</p>\n		<a href=\"#\" class=\"read-more\" data-id=\"{entry_id}\">...Read More</a>\n	</div>\n</article>\n{/exp:channel:entries}', null, '1383071949', '1', 'n', '0', '', 'n', 'n', 'o', '0');
COMMIT;

-- ----------------------------
--  Table structure for `exp_throttle`
-- ----------------------------
DROP TABLE IF EXISTS `exp_throttle`;
CREATE TABLE `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_upload_no_access`
-- ----------------------------
DROP TABLE IF EXISTS `exp_upload_no_access`;
CREATE TABLE `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `exp_upload_prefs`
-- ----------------------------
DROP TABLE IF EXISTS `exp_upload_prefs`;
CREATE TABLE `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_upload_prefs`
-- ----------------------------
BEGIN;
INSERT INTO `exp_upload_prefs` VALUES ('1', '1', 'Partners', '/Users/macbook/Development/CreativeNerds/ideasFoundation/site/html/images/uploads/partners/', 'http://ideasfoundationee.cn/images/uploads/partners/', 'img', '', '', '', '', '', '', '', '', '', '', null);
COMMIT;

-- ----------------------------
--  Table structure for `exp_wygwam_configs`
-- ----------------------------
DROP TABLE IF EXISTS `exp_wygwam_configs`;
CREATE TABLE `exp_wygwam_configs` (
  `config_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `config_name` varchar(32) DEFAULT NULL,
  `settings` text,
  PRIMARY KEY (`config_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `exp_wygwam_configs`
-- ----------------------------
BEGIN;
INSERT INTO `exp_wygwam_configs` VALUES ('1', 'Basic', 'YTo3OntzOjc6InRvb2xiYXIiO2E6ODp7aTowO3M6NDoiQm9sZCI7aToxO3M6NjoiSXRhbGljIjtpOjI7czo5OiJVbmRlcmxpbmUiO2k6MztzOjEyOiJOdW1iZXJlZExpc3QiO2k6NDtzOjEyOiJCdWxsZXRlZExpc3QiO2k6NTtzOjQ6IkxpbmsiO2k6NjtzOjY6IlVubGluayI7aTo3O3M6NjoiQW5jaG9yIjt9czo2OiJoZWlnaHQiO3M6MzoiMjAwIjtzOjE0OiJyZXNpemVfZW5hYmxlZCI7czoxOiJ5IjtzOjExOiJjb250ZW50c0NzcyI7YTowOnt9czo5OiJwYXJzZV9jc3MiO2I6MDtzOjEzOiJyZXN0cmljdF9odG1sIjtzOjE6InkiO3M6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9'), ('2', 'Full', 'YTo3OntzOjc6InRvb2xiYXIiO2E6Mjk6e2k6MDtzOjY6IlNvdXJjZSI7aToxO3M6MzoiQ3V0IjtpOjI7czo0OiJDb3B5IjtpOjM7czo1OiJQYXN0ZSI7aTo0O3M6OToiUGFzdGVUZXh0IjtpOjU7czoxMzoiUGFzdGVGcm9tV29yZCI7aTo2O3M6NDoiVW5kbyI7aTo3O3M6NDoiUmVkbyI7aTo4O3M6NToiU2NheXQiO2k6OTtzOjQ6IkJvbGQiO2k6MTA7czo2OiJJdGFsaWMiO2k6MTE7czo2OiJTdHJpa2UiO2k6MTI7czoxMjoiUmVtb3ZlRm9ybWF0IjtpOjEzO3M6MTI6Ik51bWJlcmVkTGlzdCI7aToxNDtzOjEyOiJCdWxsZXRlZExpc3QiO2k6MTU7czo3OiJPdXRkZW50IjtpOjE2O3M6NjoiSW5kZW50IjtpOjE3O3M6MTA6IkJsb2NrcXVvdGUiO2k6MTg7czo0OiJMaW5rIjtpOjE5O3M6NjoiVW5saW5rIjtpOjIwO3M6NjoiQW5jaG9yIjtpOjIxO3M6NToiSW1hZ2UiO2k6MjI7czo1OiJUYWJsZSI7aToyMztzOjE0OiJIb3Jpem9udGFsUnVsZSI7aToyNDtzOjExOiJTcGVjaWFsQ2hhciI7aToyNTtzOjg6IlJlYWRNb3JlIjtpOjI2O3M6NjoiU3R5bGVzIjtpOjI3O3M6NjoiRm9ybWF0IjtpOjI4O3M6ODoiTWF4aW1pemUiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjk6InBhcnNlX2NzcyI7YjowO3M6MTM6InJlc3RyaWN0X2h0bWwiO3M6MToieSI7czoxMDoidXBsb2FkX2RpciI7czowOiIiO30=');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
