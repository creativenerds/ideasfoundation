<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$active_group = 'expressionengine';
$active_record = TRUE;

//////////////////////////////////////////////////////////////////////////////
// Dynamically set the environment via environment.txt
// this file can be generated on deployment
// otherwise it sets it to the default below
//////////////////////////////////////////////////////////////////////////////

if (file_exists(dirname(__FILE__) . "/environment.txt")) {
    $environment = trim(file_get_contents(dirname(__FILE__) . "/environment.txt"));
} else {
    $environment = 'live';
}



if ($environment == 'local') {
    $db['expressionengine']['hostname'] = 'localhost';
	$db['expressionengine']['username'] = 'root';
	$db['expressionengine']['password'] = 'root';
	$db['expressionengine']['database'] = 'ideas_ee';
} else {
    $db['expressionengine']['hostname'] = "localhost";
    $db['expressionengine']['username'] = "ideas_ee";
    $db['expressionengine']['password'] = "u0&xbwDds&?3";
    $db['expressionengine']['database'] = "ideas_ee";
}


$db['expressionengine']['cachedir'] = dirname(__FILE__) . '/../cache/db_cache/';

$db['expressionengine']['dbdriver'] = 'mysql';
$db['expressionengine']['pconnect'] = FALSE;
$db['expressionengine']['dbprefix'] = 'exp_';
$db['expressionengine']['swap_pre'] = 'exp_';
$db['expressionengine']['db_debug'] = TRUE;
$db['expressionengine']['cache_on'] = FALSE;
$db['expressionengine']['autoinit'] = FALSE;
$db['expressionengine']['char_set'] = 'utf8';
$db['expressionengine']['dbcollat'] = 'utf8_general_ci';

/* End of file database.php */
/* Location: ./system/expressionengine/config/database.php */